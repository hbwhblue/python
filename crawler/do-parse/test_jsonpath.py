# --*-- conding:utf-8 --*--
# todo：jsonpath 模块测试
#
# @Time : 2024/3/14 17:03
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import unittest
from pprint import pprint

from jsonpath import jsonpath

from db.mongo_pool import MongoPool


class TestJsonpathCase(unittest.TestCase):
    def setUp(self):
        """
        前置设置
        @return:
        """
        self.book_dict = {
            "store": {
                "book": [
                    {
                        "name": "java 核心编程1",
                        "price": 65,
                        "isbn": "IS002598934",
                        "author": "周立新"
                    },
                    {
                        "name": "java 核心编程2",
                        "price": 64,
                        "isbn": "IS876430456",
                        "author": "周立新"
                    },
                    {
                        "name": "java 编程思想",
                        "price": 120,
                        "isbn": "IS256709873",
                        "author": "Bruce Eckel"
                    },
                    {
                        "name": "RabbitMq 实战指南",
                        "price": 79,
                        "isbn": "IS987623450",
                        "author": "朱忠华"
                    },
                    {
                        "name": "图解 TCP/IP",
                        "price": 69,
                        "isbn": "IS9787354679",
                        "author": "竹下隆史"
                    }
                ]
            }
        }
        pass

    def test_all(self):
        """
        获取所有的结构
        @return:
        """
        dict_data = jsonpath(self.book_dict, '$..*')
        print("查看dict_data支持的属性和方法：", dir(dict_data))

        try:
            for item in dict_data[0]['book']:
                # 美化打印
                pprint(item)
        except Exception as e:
            print(e)
            pass

    def test_sub_author(self):
        """
        获取所有子节点的作者
        @return:
        """
        all_author = jsonpath(self.book_dict, '$.store.book[*].author')
        print(all_author)
        pass

    def test_sub_all(self):
        """
        获取所有子孙节点
        @return:
        """
        sub_all = jsonpath(self.book_dict, '$..store.*')
        print(sub_all)
        pass

    def test_price_all(self):
        """
        获取子节点的所有价格
        @return:
        """
        price_all = jsonpath(self.book_dict, '$..price')
        print("所有的价格：", price_all)

        price_sum = sum([int(price) for price in price_all])
        print(f"价格之和：{price_sum}")
        pass

    def test_book_item(self):
        """
        取出第三本书的所有信息
        @return:
        """
        book_item = jsonpath(self.book_dict, '$..book[2]')
        print(book_item)
        pass

    def test_book_limit(self):
        """
        取出前两本书
        @return:
        """
        book_limit = jsonpath(self.book_dict, '$..book[0:2]')
        print(book_limit)

    def test_book_filter(self):
        book_count = jsonpath(self.book_dict, '$..book[?(@.price>70)]')
        print(book_count)

    def test_mongo_data(self):
        """
        获取mongo中的复杂结构的数据
        @return:
        """
        dict_data = MongoPool().project_8.coffer_check_data.find_one({"_id": "629dbfe615e74466831b5ce2"})
        # 美化打印
        pprint(dict_data)
        change_list = jsonpath(dict_data, '$..change')
        print("获取change的字段信息：", change_list)
        pass


if __name__ == '__main__':
    unittest.main()
