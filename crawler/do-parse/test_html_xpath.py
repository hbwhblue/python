# --*-- conding:utf-8 --*--
# todo：使用 lxml 解析 html 文档
#
# @Time : 2024/3/19 16:06
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import unittest
from pprint import pprint

from lxml import etree

from db.mongo_pool import MongoPool
from tools import Tools


class TestHtmlXpathCase(unittest.TestCase):
    def setUp(self):
        """
        前置操作
        @todo 1.将一个html文件转化成html对象，etree.parse()默认只能读取一个标准的html文档，对于标签不全的html,需要加上etree.HTMLParser(),否则就会报错,
            而使用HTML()会修复html的标签
        @todo 2.将html字符串转化成html对象，并使用etree.HTML()读取
        @return:
        """
        # 从本地文件中读取 book.html 文档,并使用标准的html解析器
        self.html_load = etree.parse("book.html", etree.HTMLParser())
        # 这里在末尾特意少了一个</li>，用来测试，最后是否自动补全
        self.html_str = """
        <div class="fruits-container">
            <ul>
                 <li class="item-0"><a href="apple.html">苹果</a></li>
                 <li class="item-1"><a href="orange.html">"橘子"</a></li>
                 <li class="item-inactive"><a href="banana.html"><span class="bold">香蕉</span></a></li>
                 <li class="item-1"><a href="pear.html">香梨</a></li>
                 <li class="item-0"><a href="strawberries.html">草莓</a></li>
                 <li class="item-0"><a href="pineapple.html">菠萝</a>
             </ul>
         </div>
        """
        pass

    def test_tostring(self):
        """
        获取 html中的最外层的div标签
        @return:
        """
        html_div = self.html_load.xpath('//div[@class="bookstore-container"]')
        print(html_div)
        # 将 html对象转换成字符串是 bytes 类型，并且格式化输出，并进行解码
        print(etree.tostring(html_div[0], pretty_print=True, encoding="utf-8").decode())
        pass

    def test_load_file(self):
        """
        获取 html中所有的li标签
        @return:
        """
        html_li = self.html_load.xpath('//li[@class="bookstore-item"]')
        # 遍历 class="bookstore-item" 的所有li标签
        book_list = []
        for key, li in enumerate(html_li):
            # 获取当前li标签下的a标签的href属性
            url = li.xpath("./a/@href")
            # 获取当前li 标签下的 div=title的文本内容
            title = li.xpath(".//div[@class='title']/text()")
            # 获取当前 li 标签下的 div=author的文本内容
            author = li.xpath(".//div[@class='author']/text()")
            # 获取当前li 标签下的 div=price的文本内容
            price = li.xpath(".//div[@class='price']/text()")
            # 获取当前li 标签下的 div=score的文本内容
            score = li.xpath(".//div[@class='score']/text()")
            # 获取当前li 标签下的 div=publisher的文本内容
            publisher = li.xpath(".//div[@class='publisher']/text()")
            book_dict = {
                # "_id": key,  # 主键
                "url": Tools.get_list_element(url, 0),
                "title": Tools.get_list_element(title, 0),
                "author": Tools.get_list_element(author, 0),
                "price": Tools.get_list_element(price, 0),
                "score": Tools.get_list_element(score, 0),
                "publisher": Tools.get_list_element(publisher, 0)
            }
            book_list.append(book_dict)

        # 格式化打印数据
        pprint(book_list)

        # 将数据存入到 mongodb中
        res = MongoPool().test.bookstore.insert_many(book_list)
        print(res.inserted_ids)
        pass

    def test_parse_html(self):
        """
        使用 etree.HTML() 解析 html 文档
        etree.HTML() 会修复 html 标签，并且将 html 转化成 html 对象
        @return:
        """
        html = etree.HTML(self.html_str)
        print(etree.tostring(html, pretty_print=True, encoding="utf-8").decode())

        # 获取class=item-inactive的 标签最终的 text 内容
        text_list = html.xpath('//li[@class="item-inactive"]//text()')
        print("class=item-inactive的最终文本:", text_list)

        # 获取class=item-0 的最后一个标签的 text 内容
        itme0_last_text = html.xpath('//li[@class="item-0"][last()]//text()')
        print("获取class=item-0的最后一个标签的text内容:", itme0_last_text)

        # 获取class=item-1 的所有链接地址
        item1_href = html.xpath('//li[@class="item-1"]//a/@href')
        print("获取class=item-1 的所有链接地址:", item1_href)

        # 查找所有class=bold 的标签,*所有标签
        bold_info = html.xpath('//*[@class="bold"]')
        # 将 bold_tag 转化成字符串
        # print(etree.tostring(bold_info[0], pretty_print=True, encoding="utf-8").decode())
        print("查找所有class=bold 的标签,*所有标签:", bold_info[0].tag)
        pass


pass
