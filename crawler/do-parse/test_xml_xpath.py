# --*-- conding:utf-8 --*--
# todo：
#
# @Time : 2024/3/12 00:03
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import unittest

from lxml import etree


class TestXmlXpathCase(unittest.TestCase):
    def test_xml_loadfile(self):
        """
        加载 xml文件，测试 xpath下节点的 text()内容
        @return:
        """
        tree = etree.parse('book.xml')
        # 获取 head 节点下的 title 节点的内容
        print(tree.xpath('head/title/text()'))

        # 获取 bookstore 节点下的 book 的内容
        for element in tree.xpath('//bookstore'):
            # 当前节点下的 book 节点的 title 节点的内容
            print(element.xpath('book/title/text()'))
            # 当前节点下的 book 节点的 price 节点的内容
            print(element.xpath('book/price/text()'))
        pass

    def test_xml_fromstring(self):
        """
        加载 xml 字符串，筛选属性的所有 title 的节点内容
        @return:
        """
        xml_str = """
        <root>
            <head>
                <title>xml的 xpath 测试</title>
            </head>
            <bookstore>
                <book>
                    <title lang="zh">图解 HTTP 协议</title>
                    <price>59</price>
                </book>
                <book>
                    <title lang="zh">网络爬虫开发实战</title>
                    <price>139</price>
                </book>
            </bookstore>
        </root>
        """
        tree = etree.fromstring(xml_str)
        title_nodes = tree.xpath("//title[@lang='zh']")
        for node in title_nodes:
            print(node.text)

        price_nodes = tree.xpath("//price")
        for node in price_nodes:
            print(node.text)
        pass


if __name__ == '__main__':
    unittest.main()
