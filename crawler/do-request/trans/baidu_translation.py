# --*-- conding:utf-8 --*--
# todo：baidu 翻译
#
# @Time : 2024/3/3 15:50
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import json

import requests

from .base_translation import BaseTranslation


class BaiduTranslation(BaseTranslation):
    def __init__(self, word, from_lang="zh", to_lang="en"):
        """
        初始化类成员
        @param word: 关键词
        @param from_lang:
        @param to_lang:
        """
        super().__init__(word, from_lang, to_lang)
        self.url = "https://fanyi.baidu.com/v2transapi"

    def run(self) -> requests.Response:
        post_data = {
            "form": self.from_lang,
            "to": self.to_lang,
            "query": self.word,
        }
        resp = requests.post(self.url, data=post_data, headers=self.headers, proxies=self.proxies)
        return resp

    def parse_result(self):
        """
        获取翻译结果
        @return:
        """
        res = self.run()
        if res.status_code != 200:
            return "请求失败！"

        # 将 json串转换成字典
        data_dict = json.loads(res.content.decode());
        if data_dict["errno"] != 0:
            return data_dict["errmsg"]
        else:
            return "查询成功"
