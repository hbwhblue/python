# --*-- conding:utf-8 --*--
# todo：金山翻译
#
# @Time : 2024/3/12 15:13
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import json

import requests

from .base_translation import BaseTranslation


class KingTranslation(BaseTranslation):
    # 1.先在chrom浏览器中打开一个无痕窗口进行抓包，得到请求的url，请求头 header,cookie信息
    # 2.使用request.post()发起请求，获取响应体内容
    # 3.解析响应体内容，获取翻译的结果

    def __init__(self, word, from_lang="auto", to_lang="auto"):
        super().__init__(word, from_lang, to_lang)
        # self.url = "https://ifanyi.iciba.com/index.php?c=trans&m=fly&client=6&auth_user=key_web_new_fanyi"
        self.url = "https://ifanyi.iciba.com/index.php?c=trans&m=fly&client=6&auth_user=key_web_new_fanyi"
        pass

    def run(self) -> requests.Response:
        """
        请求接口，获取翻译结果
        @return:
        """
        """
        处理翻译请求的抽象方法
        @return:
        """
        post_data = {
            "from": self.from_lang,
            "to": self.to_lang,
            "q": self.word
        }
        self.headers = {
            "Referer": "https://www.iciba.com/",
            "Cookie": "_last_active=a%3A3%3A%7Bs%3A1%3A%22i%22%3Bs%3A8%3A%2237473511%22%3Bi%3A0%3Bi%3A1710247096%3Bs%3A1%3A%22u%22%3Bs%3A12%3A%22my.iciba.com%22%3B%7D; _ustat=%7B%22i%22%3A%2237473511%22%2C%22n%22%3A%22hjc_042043%40sina.cn%22%2C%22e%22%3A%22hjc_042043%40sina.cn%22%2C%22s%22%3A%7B%22u%22%3Atrue%2C%22m%22%3Afalse%2C%22e%22%3Atrue%7D%2C%22t%22%3A1710247097%2C%22sid%22%3A%22bebd3ed986f919b5b23e4ce362c16000%22%7D; Authorization=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXlsb2FkIjoie1widWlkXCI6XCIzNzQ3MzUxMVwiLFwiZ2VuZXJhdGVUaW1lXCI6MTcxMDI0NzA5NyxcImV4cGlyZVRpbWVcIjoxNzExNDU2Njk3LFwidHRsXCI6MTIwOTYwMCxcImxvZ2luU291cmNlXCI6XCJkZWZhdWx0XCJ9In0.tsnBl2HzcS4L-ZWhDftJBg6k-1klAHUqxrJPh8KV2c4",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36",
        }

        resp = requests.post(self.url, data=post_data, headers=self.headers, proxies=self.proxies)
        return resp

    def parse_result(self) -> str:
        resp = self.run()
        if resp.status_code != 200:
            return "请求失败！"

        res_dict = json.loads(resp.content.decode())
        # 如果成功，则返回翻译结果，失败就返回错误信息
        try:
            return res_dict["content"]
        except:
            return f"错误代码：{res_dict['error_code']}，错误信息：{res_dict['message']}"

    pass
