# --*-- conding:utf-8 --*--
# todo：翻译模块
#
# @Time : 2024/3/3 15:49
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
from . import baidu_translation as baiduTrans
from . import king_translation as kingTrans

__all__ = ['baiduTrans', 'kingTrans']
