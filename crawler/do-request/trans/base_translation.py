# --*-- conding:utf-8 --*--
# todo：翻译基类
#
# @Time : 2024/3/12 15:31
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

"""
todo abc.ABCMeta 是一个metaclass(元类)，用于在Python程序中创建抽象基类
"""
from abc import ABCMeta, abstractmethod

import requests


class BaseTranslation(metaclass=ABCMeta):
    def __init__(self, word, from_lang="auto", to_lang="auto"):
        # 翻译的内容
        self.word = word
        # 原始语言
        self.from_lang = from_lang
        # 目标语言
        self.to_lang = to_lang

        # 请求头
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36"
        }

        # 代理
        self.proxies = {}
        pass

    @abstractmethod
    def run(self) -> requests.Response:
        return None
        pass

    @abstractmethod
    def parse_result(self) -> str:
        """
        解析翻译结果的抽象方法
        @return:
        """
        return ""
        pass
