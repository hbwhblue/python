# --*-- conding:utf-8 --*--
# todo：登录github，为了保持登录状态，需要使用requests.session
#
# @Time : 2024/3/13 18:14
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import re

import requests
from requests import Session


def get_auth_token(session: Session) -> str:
    """
    todo 先从github.com/login获取登录页面的html内容，然后通过正则表达式获取auth_token
    @param session:
    @return:
    """
    resp = session.get('https://github.com/login')
    if resp.status_code != 200:
        print("请求失败，请稍后再试！")
        exit(0)
    login_html = resp.content.decode()
    auth_token = re.findall(r'name="authenticity_token" value="(.*?)"', login_html)[0]
    return auth_token


def do_login(session: Session):
    """
    todo 组装表单数据，使用 session.post(),请求/session,用来记录登录状态
    @param session: session对象
    @return:
    """
    global resp
    post_data = {
        "commit": "Sign in",
        "authenticity_token": auth_token,
        "login": "hjc1985",
        "password": "123456",  # 登录密码，为了个人账号安全我这里不是真实密码
        "webauthn-conditional": "undefined",
        "javascript-support": "true",
        "webauthn-support": "supported",
        "webauthn-iuvpaa-support": "supported",
        "return_to": "https://github.com/login"
    }
    resp = session.post(url='https://github.com/session', data=post_data)
    if resp.status_code != 200:
        print("请求失败，请检查参数！")
    else:
        print("请求/session 成功！")


def chk_login_status(session: Session):
    """
    todo: 通过请求github.com/用户名，验证是否登录成功
    @param session:
    @return:
    """
    resp = session.get('https://github.com/hjc1985')
    html_content = resp.content
    res = re.findall(r'<title>(.+?)(GitHub)?</title>', html_content.decode('utf-8'))
    """
    todo 使用 try...except...来判断索引对应的值是否存在
    """
    try:
        end_str = res[0][1]
    except IndexError:
        end_str = ""

    if end_str == "":
        # 个人主页的title内容如果结尾没有GitHub，说明登录成功
        print("登录成功！")
    else:
        print("登录失败！")

    with open("github-profile.html", "wb") as f:
        f.write(html_content)


if __name__ == '__main__':
    # todo 声明一个session对象,并设置请求头
    session = requests.session()
    session.headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36'
    }
    # todo: 1.获取登录页面的auth_token
    auth_token = get_auth_token(session)

    # todo：2.组装表单数据，使用 session.post(),请求/session,用来记录登录状态
    do_login(session)

    # todo: 3.通过请求github.com/用户名，验证是否登录成功
    chk_login_status(session)

    # insert_data = {
    #     "username": "hjc1985",
    #     "req_headers": resp.request.headers,
    #     "resp_headers": resp.request.headers,
    #     "resp_status": resp.status_code,
    #     "resp_body": resp.content.decode()
    # }
    # result = MongoPool().test.login_github.insert_one(insert_data)
    # print(result.inserted_id)
