# --*-- conding:utf-8 --*--
# todo：
#
# @Time : 2024/2/29 16:35
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import json
import logging
import unittest

import requests

from trans import baiduTrans, kingTrans


class TestRequestsCase(unittest.TestCase):
    def test_requests_text(self):
        """
        测试get请求，获取响应体
        @return:
        """
        url = "https://www.baidu.com"
        # 中文站默认是乱码的
        resp = requests.get(url);

        # 推测的编码格式
        print("原来的编码：" + resp.encoding + "\n")
        # 将编码设置为 utf-8
        resp.encoding = "utf-8";

        # 打印响应内容
        print(resp.text)

        # 获取内容后的编码格式
        print("修改后的编码：" + resp.encoding)

    def test_requests_content(self):
        """
        测试 get请求，获取响应体 bytes格式
        @return:
        """
        url = "https://www.baidu.com"
        resp = requests.get(url);

        # response.content是存储的bytes类型的响应码,可以进行 decode操作
        # 默认是utf-8编码
        print(resp.content.decode())

    def test_requests_others(self):
        """
        测试 get请求，获取响应头和cookie
        @return:
        """
        url = "https://gitee.com/login"
        # 使用get请求,5秒超时
        resp = requests.get(url, timeout=5)
        print("响应的url：", resp.url)
        print("响应的状态码：", resp.status_code)
        print("响应对应的请求头：", resp.request.headers)
        # print("响应对应请求的 cookie：", resp.request.cookies)
        print("响应头：", resp.headers)
        print("响应的 cookie：", resp.cookies)

    def test_header(self):
        """
        带上User-Agent，实现模拟浏览器请求
        @return:
        """
        url = "https://www.baidu.com"
        # 以字典形式
        headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36"
        }
        resp = requests.get(url, headers=headers)
        print(resp.content.decode())

    def test_login_without_cookie(self):
        """
        header中无cookie时，查看源码 title为"<title>hjc1985 (allen.huang) · GitHub</title>"
        @return:
        """
        url = "https://github.com/hjc1985"
        headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36"
        }
        resp = requests.get(url, headers=headers)
        with open("github-without-cookie.html", "wb") as f:
            f.write(resp.content)

    def test_login_by_cookie(self):
        """
        请求头中带上cookie，是否登录成功查看源码如果不带GitHub的，表示成功了<title>hjc1985 (allen.huang)</title>"
        @return:
        """
        url = "https://github.com/hjc1985"
        headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36",
            "Cookie": "_octo=GH1.1.2126496952.1702033946; tz=Asia%2FShanghai; _device_id=407e3186e5a122df77f6f75167f8fe42; saved_user_sessions=26477262%3AhgfJbqSr1x4eMidTz2HelCMGP8ytihkDoVaB0k10h5N1dp4w; user_session=hgfJbqSr1x4eMidTz2HelCMGP8ytihkDoVaB0k10h5N1dp4w; __Host-user_session_same_site=hgfJbqSr1x4eMidTz2HelCMGP8ytihkDoVaB0k10h5N1dp4w; tz=Asia%2FShanghai; color_mode=%7B%22color_mode%22%3A%22auto%22%2C%22light_theme%22%3A%7B%22name%22%3A%22light%22%2C%22color_mode%22%3A%22light%22%7D%2C%22dark_theme%22%3A%7B%22name%22%3A%22dark%22%2C%22color_mode%22%3A%22dark%22%7D%7D; logged_in=yes; dotcom_user=hjc1985; has_recent_activity=1; preferred_color_mode=light; _gh_sess=mTCBQNs1Q78qCwNHO5sPgcLS9Qi8IDsm6xTWx72y9sgoo4BMgFPI46ZxiYL7itKaif7hm7ntqycxhCw6XkA98kfJmfbBeMVcSpNe6ZK5YB%2BD6ZBz1FAtfSgg2ms59nH5R0Z0sVGCEedN%2F7%2Fy51bn7bHESBhJU1kEsyTGRspVXQH4oBBokaX0aVUO%2BNU7tVjVYvBhqX6i%2FAS5vvWzBClRIFPTPqZumIRFfXHxR68YY%2Fc2gzZ0xmONelyOTi1qx%2FlvUb5d90fFND9HLweiqWiw9BZBt5bQZaCElcimN7NVflbEb2ENZUy5vz%2BaUt8mzM5kAnWZ%2B2wSpmJQCvmsKdnb3NhH%2BTL4Fx%2BFKA3DttQQgx9ZuT86sZHr2bs%2BBpmSfV58u0%2F4mZPtKlg9cbnpg1kQkZWOarNkokyj4p7QnigtjzmWmdd5Rwlij2xTi%2Bv6tXLAXg8lRoFD3ZP1oxo%2B3KZYukWMObEMGVVyeJnLbayvzeVMt0f7E7aCTwqrg2ZfUeCgoqlc%2FvACCSzYl7t3HpuosDy5OPPgBXDpEud%2FhPWbbfckJoV%2FhhoFSg%3D%3D--1DMqUook2Gpxl6AW--DobPmL8rMOYwjFLGp7fQGQ%3D%3D"
        }
        resp = requests.get(url, headers=headers)
        with open("github-with-cookie.html", "wb") as f:
            f.write(resp.content)

    def test_url_get(self):
        """
        带上参数，实现模拟浏览器请求
        @return:
        """
        url = "https://www.baidu.com/s?wd=python"
        headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36"
        }

        # 将请求结果写入文件
        resp = requests.get(url, headers=headers)
        resp.close()
        with open("baidu.html", "wb") as f:
            f.write(resp.content)

    def test_params_get(self):
        """
        使用参数字典，实现模拟浏览器请求
        @return:
        """
        url = "https://www.baidu.com/s?"
        # header字典
        headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36"
        }
        # 参数字典
        pdata = {
            "wd": "python"
        }
        resp = requests.get(url, headers=headers, params=pdata)
        resp.close()
        print(resp.url)

        with open("baidu_python.html", "wb") as f:
            f.write(resp.content)

    def test_cookie_get(self):
        """
        将请求头的cookie设置成一个字典参数
        @return:
        """
        url = "https://github.com/hjc1985"
        headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36",
        }

        # 将cookie 数据分解
        cookies = {}
        cookie_str = "_octo=GH1.1.2126496j952.1702033946; tz=Asia%2FShanghai; _device_id=407e3186e5a122df77f6f75167f8fe42; saved_user_sessions=26477262%3AhgfJbqSr1x4eMidTz2HelCMGP8ytihkDoVaB0k10h5N1dp4w; user_session=hgfJbqSr1x4eMidTz2HelCMGP8ytihkDoVaB0k10h5N1dp4w; __Host-user_session_same_site=hgfJbqSr1x4eMidTz2HelCMGP8ytihkDoVaB0k10h5N1dp4w; tz=Asia%2FShanghai; color_mode=%7B%22color_mode%22%3A%22auto%22%2C%22light_theme%22%3A%7B%22name%22%3A%22light%22%2C%22color_mode%22%3A%22light%22%7D%2C%22dark_theme%22%3A%7B%22name%22%3A%22dark%22%2C%22color_mode%22%3A%22dark%22%7D%7D; logged_in=yes; dotcom_user=hjc1985; has_recent_activity=1; preferred_color_mode=light; _gh_sess=mTCBQNs1Q78qCwNHO5sPgcLS9Qi8IDsm6xTWx72y9sgoo4BMgFPI46ZxiYL7itKaif7hm7ntqycxhCw6XkA98kfJmfbBeMVcSpNe6ZK5YB%2BD6ZBz1FAtfSgg2ms59nH5R0Z0sVGCEedN%2F7%2Fy51bn7bHESBhJU1kEsyTGRspVXQH4oBBokaX0aVUO%2BNU7tVjVYvBhqX6i%2FAS5vvWzBClRIFPTPqZumIRFfXHxR68YY%2Fc2gzZ0xmONelyOTi1qx%2FlvUb5d90fFND9HLweiqWiw9BZBt5bQZaCElcimN7NVflbEb2ENZUy5vz%2BaUt8mzM5kAnWZ%2B2wSpmJQCvmsKdnb3NhH%2BTL4Fx%2BFKA3DttQQgx9ZuT86sZHr2bs%2BBpmSfV58u0%2F4mZPtKlg9cbnpg1kQkZWOarNkokyj4p7QnigtjzmWmdd5Rwlij2xTi%2Bv6tXLAXg8lRoFD3ZP1oxo%2B3KZYukWMObEMGVVyeJnLbayvzeVMt0f7E7aCTwqrg2ZfUeCgoqlc%2FvACCSzYl7t3HpuosDy5OPPgBXDpEud%2FhPWbbfckJoV%2FhhoFSg%3D%3D--1DMqUook2Gpxl6AW--DobPmL8rMOYwjFLGp7fQGQ%3D%3D"
        # 将cookie字符串先使用"; "进行分割
        cookie_list = cookie_str.split(sep="; ")
        # 常规写法
        # for data in cookie_list:
        #     # 再使用 "="进行分割
        #     cookie_data = data.split(sep="=")
        #     cookies[cookie_data[0]] = cookie_data[1]

        # 使用字典推导式简写
        cookies = {data.split("=")[0]: data.split("=")[1] for data in cookie_list}

        # 请求 github的个人页面
        resp = requests.get(url, headers=headers, cookies=cookies)
        with open("github-with-cookie2.html", "wb") as f:
            f.write(resp.content)

    def test_proxy_get(self):
        """
        todo 使用代理，实现模拟浏览器请求
        todo 当代理ip失效时，要么就是一直卡着，要么就是报错
        @return:
        """
        url = "https://www.baidu.com"
        headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36"
        }
        proxies = {
            # "http": "121.236.238.89:33502",
            "https": "183.145.112.98:64256"
            # "https": "49.79.50.248:64256"
            # "https": "113.133.13.173:64256"
        }
        resp = requests.get(url, headers=headers, proxies=proxies)
        print(resp.status_code)

    def test_no_verify_get(self):
        """
        todo 关闭verify,让我们的请求忽略CA证书(默认是开启的)，是在目标网站的证书不被信任，或证书到期时使用
        @return:
        """
        # 使用日志的方式来打印
        logging.basicConfig(level=logging.DEBUG)

        url = "https://sam.huat.edu.cn:8443/selfserver"
        headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36"
        }
        resp = requests.get(url, headers=headers, verify=False)
        logging.debug(resp.status_code)

    def test_stream_get(self):
        """
        todo 流式获取数据
        @return:
        """
        resp = requests.get('https://httpbin.org/stream/20', stream=True)
        for line in resp.iter_lines():
            if line:
                decoded_line = line.decode("utf-8")
                # json 转 python数据结构
                print(json.loads(decoded_line))

    def test_post(self):
        """
        post 请求
        @return:
        """
        headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36"
        }
        post_data = {'uid': '1', 'uname': 'hjc'}
        resp = requests.post("https://httpbin.org/post", headers=headers, data=post_data)
        print(resp.text)

    def test_upload_text_file(self):
        """
        todo txt文件上传
        @return:
        """
        url = 'https://httpbin.org/post'
        txt_file = {'file': open('1.txt', 'rb')}
        resp = requests.post(url, files=txt_file);
        print(resp.text)

    def test_upload_excel_file(self):
        """
        todo excel文件上传
        @return:
        """
        url = "https://httpbin.org/post"
        execl_file = files = {
            'file': (
                'report.xls', open('test1.xlsx', 'rb'),
                'application/vnd.ms-excel',
                {'Expires': '0'}
            )
        }
        resp = requests.post(url, files=execl_file)
        print(resp.text)

    def test_upload_multiples(self):
        """
        todo 上传多个文件,需要关闭文件，不然就会报错
        @return:
        """
        url = 'https://httpbin.org/post'
        try:
            img1_handle = open('1.jpg', 'rb')
            img2_handle = open('2.jpg', 'rb')
            multiple_files = [
                ('images', ('1.jpg', img1_handle, 'image/jpeg')),
                ('images', ('2.jpg', img2_handle, 'image/jpeg')),
            ]
            resp = requests.post(url, files=multiple_files)
        except Exception as e:
            print(e)
        finally:
            img1_handle.close()
            img2_handle.close()

        print(resp.status_code)

    def test_response_json(self):
        """
        todo 将响应内容转换成python 数据结构(list 或 dict)
        @return:
        """
        resp = requests.get("https://api.github.com/events")
        json_data = resp.json()
        print(type(json_data), json_data)

    def test_baidu_translation(self):
        word = input("请输入要翻译的词：")
        baidu_trans = baiduTrans.BaiduTranslation(word=word, from_lang="zh", to_lang="en")
        res = baidu_trans.parse_result()
        print(res)

    def test_king_translation(self):
        # word = input("请输入翻译的词：")
        king_trans = kingTrans.KingTranslation(word="进程", from_lang="zh", to_lang="en")
        res = king_trans.parse_result
        print(res)

    def test_session(self):
        """
        todo session是自动保持cookie，在多次请求时，需要用到 cookie,就是用session
        todo 比如在模拟登录后，再去访问个人页，这里是拿模拟登录github.com举例
        @return: 
        """


if __name__ == '__main__':
    unittest.main()
