# --*-- conding:utf-8 --*--
# todo：抓取虎牙直播的列表数据
#
# @Time : 2024/3/28 14:45
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import json
import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromService
from selenium.webdriver.common.by import By

from db.mongo_pool import MongoPool


class GetHuyaDatas(object):
    def __init__(self):
        self.url = 'https://www.huya.com/l'
        # 显示设置驱动的路径,这是 selenium4之后的新写法,主要是为了解决 selenium打开浏览器慢的问题
        service = ChromService(executable_path="/usr/local/bin/chromedriver")
        self.driver = webdriver.Chrome(service=service)
        pass

    def run(self):
        self.driver.get(self.url)
        # 关闭登录弹窗
        try:
            # 隐式等待 最大10秒
            self.driver.implicitly_wait(10)

            # 关闭登录弹窗,需要切换到弹窗的iframe中
            self.driver.switch_to.frame('UDBSdkLgn_iframe')
            self.driver.find_element(by=By.ID, value="close-udbLogin").click()
            # 从弹窗的iframe中切换回主页面
            self.driver.switch_to.default_content()
        except Exception as e:
            print(e)
            pass

        # 隐式等待 最大15秒,等待ajax请求完成
        self.driver.implicitly_wait(15)

        # 遍历10页
        for i in range(1, 11):
            print(f'正在抓取第{i}页')
            time.sleep(1)

            with open(f'huya{i}.html', 'w') as f:
                f.write(self.driver.page_source)

            # 解析
            data_list = self.parse(i)
            self.save_data(data_list)

            # 点击下一页,获取新的页面内容
            try:
                # 滚动到底部
                self.driver.execute_script("window.scrollTo(0, 100000);")
                self.driver.find_element(by=By.XPATH,
                                         value='//div[@id="js-list-page"]//a[@class="laypage_next"]').click()
            except:
                print('已经到最后一页了')
                pass

        time.sleep(5)
        self.driver.quit()
        pass

    def save_data(self, data_list):
        """
        保存数据到mongodb
        @param data_list:
        @return:
        """
        MongoPool().test.huya.insert_many(data_list)

    def parse(self, page):
        """
        解析虎牙直播的数据
        @param data_list:属于引用数据
        @return:
        """
        list_data = []
        room_list = self.driver.find_elements(by=By.XPATH, value='//div[@class="box-bd"]//ul[@id="js-live-list"]/li')
        for key, elem in enumerate(room_list):
            # 获取每个直播间的 链接，封面，直播间名字，主播名称，热度，游戏类名组成字典
            tmp_dic = {}
            # 直播间链接
            try:
                tmp_dic['link'] = elem.find_element(by=By.XPATH, value='./a[1]').get_attribute('href')
            except:
                tmp_dic['link'] = ''
                pass

            # 直播间封面
            try:
                tmp_dic['cover'] = elem.find_element(by=By.XPATH, value='./a[1]/img').get_attribute('src')
            except:
                tmp_dic['cover'] = ''
                pass

            # 直播间名字
            try:
                tmp_dic['name'] = elem.find_element(by=By.XPATH, value='./a[2]').text
            except:
                tmp_dic['name'] = ''
                pass

            # 主播头像
            try:
                tmp_dic['user-cover'] = elem.find_element(by=By.XPATH,
                                                          value='.//span[contains(@class,"avatar")]/img').get_attribute(
                    'src')
            except:
                tmp_dic['user-cover'] = ''
                pass

            # 主播昵称
            try:
                tmp_dic['user-name'] = elem.find_element(by=By.XPATH, value='.//span[contains(@class,"avatar")]/i').text
            except:
                tmp_dic['user-name'] = ''
                pass

            # 游戏名称
            try:
                tmp_dic['game-name'] = elem.find_element(by=By.XPATH,
                                                         value='.//span[contains(@class,"game-type")]/a').text
            except:
                tmp_dic['game-name'] = ''
                pass

            # 游戏链接
            try:
                tmp_dic['game-link'] = elem.find_element(by=By.XPATH,
                                                         value='.//span[contains(@class,"game-type")]/a').get_attribute(
                    'href')
            except:
                tmp_dic['game-link'] = ''
                pass

            # 热度
            try:
                tmp_dic['hot'] = elem.find_element(by=By.XPATH, value='.//span[@class="num"]/i[@class="js-num"]').text
            except:
                tmp_dic['hot'] = ''
                pass

            # 新增时间
            tmp_dic['insert_time'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
            # 将数据写入列表
            list_data.append(tmp_dic)
            pass

        # 将数据写入文件
        with open(f'huya{page}.json', 'a', encoding="utf-8") as f:
            json.dump(list_data, f, indent=4, sort_keys=False)
            pass
        return list_data

    pass


if __name__ == '__main__':
    huya = GetHuyaDatas()
    huya.run()
