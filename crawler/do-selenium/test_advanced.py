# --*-- conding:utf-8 --*--
# todo：selenium模拟浏览器高级操作
#
# @Time : 2024/3/25 03:23
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import time
import unittest
from pprint import pprint

from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromService
from selenium.webdriver.common.by import By

from tools import Tools


class TestAdvancedCase(unittest.TestCase):
    def setUp(self):
        """
        前置操作
        @return:
        """
        self.user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'
        # 验证 selenium网站是否有效
        self.url = 'https://bot.sannysoft.com/'
        self.service = ChromService(executable_path="/usr/local/bin/chromedriver")
        pass

    def test_selenium_valid(self):
        """
        验证selenium是否被反爬
        @return:
        """
        # 实例化配置项
        chrome_options = webdriver.ChromeOptions()
        # 设置无头模式
        chrome_options.add_argument("--headless")
        # 设置无显卡
        chrome_options.add_argument("--disable-gpu")
        # 实例化浏览器对象
        driver = webdriver.Chrome(service=self.service, options=chrome_options)

        # 发送请求,截图
        driver.get(self.url)
        driver.save_screenshot('sannysoft_valid.png')
        print("截屏成功！")
        # 关闭浏览器
        driver.quit()
        pass

    def test_stealth(self):
        """
        todo 主要为了隐藏 selenium 的指纹，不被网站给识别出来是机器人(即不被反爬)
        @return:
        """
        # 配置实例化配置项
        chrome_options = webdriver.ChromeOptions()
        # 设置无头模式
        chrome_options.add_argument('--headless')
        # 设置无显卡
        chrome_options.add_argument('--disable-gpu')
        # 实例化浏览器对象
        driver = webdriver.Chrome(service=self.service, options=chrome_options)

        # 执行`stealth.min.js`文件进行隐藏浏览器指纹
        with open("../stealth.min.js", "r") as f:
            js = f.read()
        # 执行js代码
        driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {"source": js})
        # 发送请求
        driver.get(self.url)
        # 截图
        driver.save_screenshot('sannysoft.png')
        print("消除指纹成功！")
        # 关闭浏览器
        driver.quit()
        pass

    def test_label_switch(self):
        """
        todo: 使用句柄切换标签页，同时隐藏指纹
        todo: 句柄相当于是智能指针，来标识资源；而隐藏指纹是为了防止被反爬
        @return:
        """
        # 声明浏览器驱动实例
        driver = webdriver.Chrome(service=self.service)
        # 执行反爬js代码，隐藏浏览器指纹
        driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {"source": Tools.open_stealth_js()})

        # 进入58同城首页
        driver.get("https://hz.58.com/")

        # todo 获取当前所有的标签页的句柄构成的列表，这不能提前申明为变量，随着新便签页的打开，这是动态变化的
        # current_windows = driver.window_handles

        # 获取当前页
        print(driver.current_url)
        print(driver.window_handles)

        # 点击整租的链接，打开租房页面
        driver.find_element(by=By.XPATH, value="/html/body/div[3]/div[1]/div[1]/div/div[1]/div[1]/span[2]/a").click()
        # todo 根据标签页句柄,将句柄切换到最新新打开的标签页，这一定要有，不然句柄不会变化，那么下一页内容取不到
        driver.switch_to.window(driver.window_handles[-1])
        print(driver.current_url)
        print(driver.window_handles)

        elem_list = driver.find_elements(by=By.XPATH, value="/html/body/div[6]/div[2]/ul/li/div[2]/h2/a")
        # 统计租房列表的个数
        print(len(elem_list))

        time.sleep(5)
        driver.quit()
        pass

    def test_no_switchto_frame(self):
        # 声明浏览器驱动实例
        driver = webdriver.Chrome(service=self.service)
        # 打开 qzone 登录页
        driver.get("https://qzone.qq.com/")

        # 默认进来是采用扫码，我们这里需要切换成账号密码登录
        driver.find_element(by=By.ID, value="switcher_plogin").click()
        # 填写账号
        driver.find_element(by=By.ID, value="u").send_keys("531881823.qq.com")
        # 并不是真实的密码
        driver.find_element(by=By.ID, value="p").send_keys("123456")
        # 点击登录按钮
        driver.find_element(by=By.XPATH, value='//*[@id="login_button"]').click()

        time.sleep(5)
        driver.quit()
        pass

    def test_switchto_frame(self):
        driver = webdriver.Chrome(service=self.service)
        driver.get("https://qzone.qq.com/")

        # 切入iframe
        driver.switch_to.frame("login_frame")

        driver.find_element(by=By.ID, value="switcher_plogin").click()
        driver.find_element(by=By.ID, value="u").send_keys("531881823.qq.com")
        driver.find_element(by=By.ID, value="p").send_keys("123456")
        driver.find_element(by=By.XPATH, value='//*[@id="login_button"]').click()

        time.sleep(5)
        driver.quit()
        pass

    def test_cookie(self):
        """
        获取cookie
        @return:
        """
        driver = webdriver.Chrome(service=self.service)
        driver.get("https://baidu.com/")

        # 获取cookie的值
        cookie_info = {data["name"]: data["value"] for data in driver.get_cookies()}
        pprint(cookie_info)

        time.sleep(2)
        driver.quit()
        pass

    def test_js(self):
        """
        执行js，打开一个新窗口，跳转到二手房页面
        @return:
        """
        # 打开浏览器，进到链家首页
        driver = webdriver.Chrome(service=self.service)
        driver.get("https://hz.lianjia.com/")

        # 我想重开一个窗口查看所有的二手房页面
        js = "window.open('https://hz.lianjia.com/ershoufang/');"
        # 执行js代码
        driver.execute_script(js)

        time.sleep(5)
        driver.quit()
        pass

    def test_js2(self):
        """
        执行js代码，滚动条往下拉
        @return:
        """
        driver = webdriver.Chrome(service=self.service)
        driver.get("https://hz.lianjia.com/")

        # 执行js代码，滚动条往下拉500像素
        js = "window.scrollTo(0,1000);"
        driver.execute_script(js)

        time.sleep(2)

        # 然后点击查看更多二手房的按钮
        driver.find_element(by=By.XPATH, value='//div[@id="ershoufanglist"]/div/div[1]/p/a').click()

        # 强制等待5秒
        time.sleep(5)
        driver.quit()
        pass

    def test_page_wait(self):
        """
        隐式等待
        @return:
        """
        # 打开浏览器，进到百度首页
        driver = webdriver.Chrome(service=self.service)
        driver.get("https://www.baidu.com/")

        # 隐式等待，最大等待10秒
        driver.implicitly_wait(10)

        # 点击百度的logo图标的src 属性
        elem = driver.find_element(by=By.XPATH, value='//img[@id="s_lg_img_new"]')
        print(elem.get_attribute("src"))

        # 强制等待2秒
        time.sleep(2)
        driver.quit()
        pass

    def test_proxy(self):
        """
        使用代理测试121.234.119.235
        @return:
        """
        # 声明浏览器配置对象
        opt = webdriver.ChromeOptions()
        opt.add_argument("--proxy-server=http://121.234.119.235:64256")

        # 打开浏览器，进到百度首页
        driver = webdriver.Chrome(service=self.service, options=opt)
        driver.get("https://www.baidu.com/")
        driver.save_screenshot('baidu.png')

        time.sleep(5)
        driver.quit()
        pass

    def test_user_agent(self):
        """
        selenium 可以修改请求头，user-agent，模拟成不同的浏览器
        @return:
        """
        user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.4 Safari/605.1.15"
        # 声明浏览器配置对象
        opt = webdriver.ChromeOptions()
        # 设置user-agent修改请求头
        opt.add_argument(f"--user-agent={user_agent}")
        # 打开浏览器，进到百度首页
        driver = webdriver.Chrome(service=self.service, options=opt)
        driver.get("https://www.baidu.com/")

        time.sleep(5)
        driver.quit()
        pass

    def test_selenium_headers(self):
        """
        selenium获取请求头和响应头
        @return:
        """
        user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.4 Safari/605.1.15"

        # 导入seleniumwire库
        from seleniumwire import webdriver
        # 声明浏览器配置对象
        opt = webdriver.ChromeOptions()
        # 设置user-agent修改请求头
        opt.add_argument(f"--user-agent={user_agent}")
        # 打开浏览器，进到百度首页
        driver = webdriver.Chrome(service=self.service, options=opt)
        driver.get("https://www.baidu.com/")

        print("请求头：")
        for request in driver.requests:
            pprint(request.headers)
            break

        print("响应头：")
        for request in driver.requests:
            pprint(request.response.headers)
            break

        time.sleep(5)
        driver.quit()
        pass


if __name__ == '__main__':
    unittest.main()
