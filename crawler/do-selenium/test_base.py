# --*-- conding:utf-8 --*--
# todo：selenium模拟浏览器的基本操作
#
# @Time : 2024/3/24 16:10
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import time
import unittest

from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromService
from selenium.webdriver.common.by import By


class TestBaseCase(unittest.TestCase):
    def setUp(self) -> None:
        """
        @todo: 设置驱动的路径,主要是为了加快selenium的启动速度，不用再去搜索驱动的路径
        @return:
        """
        self.service = ChromService(executable_path="/usr/local/bin/chromedriver")
        pass

    def test_chrome_driver(self):
        """
        @todo: selenium测试chrome的浏览器
        @return:
        """
        # 设置驱动的路径,这是 selenium4之后的新写法
        # 实例化浏览器对象
        browser = webdriver.Chrome(service=self.service)
        # 发送请求访问百度
        browser.get('https://www.baidu.com')
        # 获取页面标题
        print("当前页面标题：", browser.title)

        time.sleep(10)
        # 退出浏览器
        browser.quit()
        pass

    def test_attr_method(self):
        """
        @todo: 测试浏览器的属性和方法
        @return:
        """
        # 实例化浏览器对象
        driver = webdriver.Chrome(service=self.service)

        # 获取浏览器的名称
        print(driver.name)

        # 访问百度首页，这里先暂时不使用https
        driver.get('http://www.baidu.com')
        # 属性1：打印当前响应对应的URL,之前 http的转换成了 https
        print(driver.current_url)
        # 属性2：打印当前标签页的标题
        print(driver.title)
        # 属性3：打印当前网页的源码长度
        print(len(driver.page_source))

        # 休息2秒，跳转到豆瓣首页,这里的两秒是等当前页面加载完毕，也就是浏览器转完圈的两秒
        time.sleep(2)
        driver.get('https://www.douban.com')

        # 休息2秒，再返回百度
        time.sleep(2)
        driver.back()

        # 休息2秒，再前进到豆瓣
        time.sleep(2)
        driver.forward()

        # 休息2秒，再刷新页面
        time.sleep(2)
        driver.refresh()

        # 保存当前页面的截屏快照
        driver.save_screenshot("./screenshot.png")

        # 关闭当前标签页
        driver.close()

        # 关闭浏览器,释放进程
        driver.quit()
        pass

    def test_headless_driver(self):
        """
        @todo: 测试chrome无头模式浏览器
        @return:
        """
        # 1.实例化配置对象
        chrome_options = webdriver.ChromeOptions()
        # 2.配置对象开启无头模式
        chrome_options.add_argument('--headless')
        # 3.配置对象添加无显卡模式，即无图形界面
        chrome_options.add_argument('--disable-gpu')
        # 4.实例化浏览器对象
        browser = webdriver.Chrome(service=self.service, options=chrome_options)
        browser.get('https://www.baidu.com')

        # 查看当前页面url
        print(browser.current_url)
        # 获取页面标题
        print("页面标题：", browser.title)
        # 获取渲染后的页面源码
        print("页面源码-长度：", len(browser.page_source))
        # 获取页面cookie
        print("cookie-data", browser.get_cookies())

        # 关闭页面的标签页
        browser.close()

        time.sleep(5)

        # 关闭浏览器
        browser.quit()
        pass

    def test_save_screen_shot(self):
        """
        todo 测试无头浏览器，保存页面截图
        @return:
        """
        # 实例化配置对象
        chrome_options = webdriver.ChromeOptions()
        # 配置对象开启无头模式
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-gpu")
        # 实例化Chrome浏览器对象
        driver = webdriver.Chrome(service=self.service, options=chrome_options)

        # 访问百度首页
        driver.get("https://www.baidu.com")
        # 截图百度首页保存
        driver.save_screenshot("./screenshot.png")
        pass

    def test_phantjs_driver(self):
        """"
        测试phantomjs的浏览器,在 selenium4之后，phantomjs已经不再支持了
        @return:
        """
        driver = webdriver.PhantomJS()
        driver.get('https://www.baidu.com')
        print(driver.title)
        driver.quit()
        pass

    def test_element_id(self):
        """
        @todo: 通过id定位元素，测试输入框的输入和点击
        @return:
        """
        # 实例化浏览器对象
        browser = webdriver.Chrome(service=self.service)
        # 打开百度首页
        browser.get('https://www.baidu.com')
        # 输入关键字内容sora,并赋值给 标签的id属性名来定位；这是新的写法,
        browser.find_element(by=By.ID, value='kw').send_keys('sora')
        # 点击搜索按钮
        browser.find_element(by=By.ID, value='su').click()
        # 获取搜索结果
        # 关闭浏览器
        time.sleep(5)
        browser.quit()
        pass

    def test_element_class_name(self):
        """
        @todo: 通过class_name定位元素，测试输入框的输入和点击
        @return:
        """
        browser = webdriver.Chrome(service=self.service)
        browser.get('https://www.baidu.com')
        # 使用class_name定位输入框,并把输入框的内容设置为sora
        browser.find_element(by=By.CLASS_NAME, value='s_ipt').send_keys('sora')
        browser.find_element(by=By.CLASS_NAME, value='s_btn').click()
        time.sleep(2)
        browser.quit()
        pass

    def test_element_xpath(self):
        """
        @todo: 通过xpath定位元素，测试输入框的输入和点击
        @return:
        """
        # 实例化浏览器对象
        browser = webdriver.Chrome(service=self.service)
        browser.get('https://www.baidu.com')
        # 使用xpath定位输入框,并把输入框的内容设置为sora
        browser.find_element(by=By.XPATH, value='//*[@id="kw"]').send_keys('sora')
        # 点击搜索按钮
        browser.find_element(by=By.XPATH, value='//*[@id="su"]').click()
        # 关闭浏览器
        time.sleep(5)
        browser.quit()
        pass

    def test_element_css_selector(self):
        """
        @todo: 通过css选择器定位元素，测试输入框的输入和点击
        @return:
        """
        # 实例化浏览器对象,打开百度首页
        browser = webdriver.Chrome(service=self.service)
        browser.get('https://www.baidu.com')
        # 使用css选择器定位输入框,先进行清空文本框，并把输入框的内容设置为sora
        browser.find_element(by=By.CSS_SELECTOR, value="#kw").clear()
        browser.find_element(by=By.CSS_SELECTOR, value="#kw").send_keys('sora')
        browser.find_element(by=By.CSS_SELECTOR, value="#su").click()
        # 关闭浏览器
        time.sleep(2)
        browser.quit()
        pass

    def test_elements_xpath(self):
        """
        @todo: 通过xpath定位元素列表，测试获取列表的数据
        @return:
        """
        browser = webdriver.Chrome(service=self.service)
        browser.get('https://fund.eastmoney.com/data/fundranking.html#tall')
        # 使用xpath定位列表
        elements = browser.find_elements(by=By.XPATH, value='//table[@id="dbtable"]/tbody/tr/td[3]/a')
        for elem in elements:
            print(elem.text)
        # 页面加载完后，再等待2秒，自动关闭浏览器，这里没有使用quit()方法，也能自动释放
        time.sleep(2)
        browser.quit()
        pass

    def test_diff_find_element(self):
        """
        测试 find_element 和 find_elements 的区别
        @return:
        """
        browser = webdriver.Chrome(service=self.service)
        browser.get('https://www.baidu.com')

        # 使用xpath定位列表
        elements = browser.find_elements(by=By.XPATH, value='//table[@id="dbtable"]/tbody/tr/td[3]/a')
        for elem in elements:
            print(elem.text)

        # 定位单个
        try:
            elem = browser.find_element(by=By.XPATH, value='//table[@id="dbtable"]/tbody/tr/td[3]/a')
            print(elem)
        except:
            print("没有找到响应的元素")
            pass

        # 页面加载完后，再等待2秒，自动关闭浏览器，这里没有使用quit()方法，也能自动释放
        time.sleep(2)
        browser.quit()
        pass

    def test_op_element(self):
        """
        测试获取元素的文本内容
        @return:
        """
        # 实例化浏览器对象
        browser = webdriver.Chrome(service=self.service)
        # 天天基金网，查看所有基金，这只是测试，不作为正式的爬虫
        browser.get('https://fund.eastmoney.com/data/fundranking.html#tall')
        # 使用xpath定位元素列表,基金代码
        elems = browser.find_elements(by=By.XPATH, value='//table[@id="dbtable"]/tbody/tr/td[3]/a')
        for elem in elems:
            # 代码的内容
            print(elem.text)
            # 获取基金的链接
            print(elem.get_attribute('href'))

        time.sleep(2)
        browser.quit()
        pass


if __name__ == '__main__':
    unittest.main()
