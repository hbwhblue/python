# --*-- conding:utf-8 --*--
# todo：socket服务端
#
# @Time : 2024/4/9 13:55
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm


import socket


def main():
    # 1. 创建socket,socket.AF_INET我们的网络协议是ipv4,socket.SOCK_STREAM就是tcp协议
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # 2. 绑定ip和端口,需要写成0.0.0.0,这样子局域网的客户端才能连接到我们的服务端,127.0.0.1只能本机连接
    s.bind(('0.0.0.0', 8888))
    # 3. 监听
    s.listen(5)
    # 4. 接收客户端连接
    while True:
        # 4.1 接收客户端连接信息，是一个元组，第一个元素是连接对象，第二个元素是客户端的ip和端口
        conn, addr = s.accept()
        print(f'accept from {addr}')
        # 5. 接收客户端发送的数据
        data = conn.recv(1024)
        print(data)
        # 6. 发送数据给客户端
        conn.send(b'local server')
        # 7. 关闭连接
        conn.close()


if __name__ == '__main__':
    main()
