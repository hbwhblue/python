# --*-- conding:utf-8 --*--
# todo：socket客户端
#
# @Time : 2024/4/9 13:55
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import socket

# 1. 创建socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# 2. 连接服务器
s.connect(('127.0.0.1', 8888))
# 3. 发送客户端数据
s.send(b'this is local client')
# 4. 接收服务端的响应数据,1KB的数据
data = s.recv(1024)
# 5. 打印数据，关闭连接
print(type(data), data)
s.close()
