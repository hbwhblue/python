# --*-- conding:utf-8 --*--
# todo：
#
# @Time : 2024/1/4 03:33
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import json
import unittest

import xmltodict


class MyTestCase(unittest.TestCase):
    def test_xml2json(self):
        """
        xml转json
        """

        # 读取xml文件
        xml_path = '1.xml'
        with open(xml_path, 'r', encoding='utf-8') as fp:
            xml_str = fp.read()
            # 截取xml文件名，不包含后缀xml
            with open(xml_path[:-3] + "json", 'w', encoding='utf-8') as json_file:
                json_file.write(xml_to_json(xml_str))

    def test_json2xml(self):
        """
        json转换为xml
        """
        # 读取json文件
        json_path = 'test.json'
        with open(json_path, 'r', encoding='utf-8') as fp:
            json_str = fp.read()
            with open(json_path[:-4] + "xml", "w", encoding="utf-8") as xml_file:
                xml_file.write(json_to_xml(json_str))


pass


def xml_to_json(xml_str):
    # 将xml转换成python字典
    xml_parse = xmltodict.parse(xml_str)
    # 将python字典转换为json字符串，并且进行格式化
    json_str = json.dumps(xml_parse, indent=4)
    return json_str


def json_to_xml(json_str):
    """
    将json字符串转换成xml
    """
    # 将json字符串转成字典
    python_dict = json.loads(json_str)
    # 将python字典转换成xml字符串
    xml_str = xmltodict.unparse(python_dict)
    return xml_str


if __name__ == '__main__':
    unittest.main()
