# --*-- conding:utf-8 --*--
# todo：dumps和loads来处理json字符串
# todo：load和dump来处理json文件
#
# @Time : 2024/1/3 02:14
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import json
import unittest


class TestJson(unittest.TestCase):
    def test_dumps(self):
        """
        将字典转换成json
        """
        person = {
            "name": "黄锦潮",
            "age": 39,
            "tel": ["15389237523", "13634199417"],
            "isonly": True
        }
        jsonStr = json.dumps(person)
        print("json：", jsonStr)

    def test_to_jsonfile(self):
        """
        将python数据类型转换成json，并写入到文件中去
        """
        person = {
            "name": "小明",
            "age": 28,
            "tel": ["0571-67239317", "13523459876"],
            "isonly": True,
            "email": "hjc_042043@sina.cn"
        }
        # with open("data.json", "w", encoding="utf-8") as fp:
        #     pass

        json.dump(person, open("data.json", "w", encoding="utf-8"),
                  sort_keys=True, indent=4)

    def test_loads(self):
        """
        将json字符串转换成python类型
        """
        json_str = '{"name": "\u9ec4\u9526\u6f6e", "age": 39, "tel": ["15389237523", "13634199417"], "isonly": true}'
        python_obj = json.loads(json_str)
        print(python_obj)
        print("类型：", type(python_obj))

    def test_load_jsonfile1(self):
        """
        通过打开文件的方式先从json文件中读取json字符串，再转换成python字典
        """
        with open("data.json", 'r', encoding='utf-8') as fp:
            content = fp.read()

        python_data = json.loads(content)
        print(type(python_data), python_data)

        print(python_data.keys())
        print(python_data.values())
        print(python_data['tel'][1])

    def test_load_jsonfile2(self):
        """
        直接读取json文件，转换成python字典
        @return:
        """
        # with open('data.json', 'r', encoding='utf-8') as fp:
        #     pass
        python_obj = json.load(open('data.json', 'r', encoding='utf-8'))
        print(type(python_obj), python_obj)


if __name__ == '__main__':
    unittest.main()
