# --*-- conding:utf-8 --*--
# todo：python中json的注意事项：
#     > json的键值对的键部分,必须用双引号"包裹，单引号不行(如果在键中出现了关键字，也被字符化了)，而js中对象没有强制要求(所以键中不允许出现关键字)
#     > json键值对中的值部分，不允许出现function,undefined,NaN,但是可以有null,js中对象的值中可以出现
#     > json数据结束后，不允许出现没有意义的的逗号，如{"name":"admin","age":18,}，注意看数据结尾部分18后面的逗号，不允许出现.
# <p>
# todo: json模块：
#     > 作用：
#       1.使用json字符串生成python对象
#       2.由python对象转换成json字符串
# <p>
# todo：数据类型转换
#  将数据从Python转换成json格式，在数据类型上会有变化，如下所示：
#  Python类型         Json类型
#  dict               object
#  list,tuple         array
#  str                string
#  int,float,enums    number
#  True               true
#  False              false
#  None               null
# <p>
# todo：使用方法：
#  json模块的使用其实很简单，对于绝大多数场合下，我们只需要使用下面的四个方法就可以了：
#  方法               功能
#  json.dumps(obj)     将python数据类型转换成json字符串
#  json.dump(obj,fp)  将python数据类型转换，并保存到json格式的文件内
#  json.loads(s)       将json格式的字符串转换成python的数据类型
#  json.load(fp)      将json格式的文件中读取数据并转换为python的类型
# <p>
# todo json.dumps()的语法格式
#  json.dumps(obj,*,skipKeys=False,indent=4,sort_keys=True,**kw)
#  解读：
#   obj：python对象
#   *: 参数标识符，*左边的是位置参数，不能改变位置，*右边的是关键词参数(参数名=值)，可以改变位置
#   skipKeys: 字典的key是否是字符串类型(默认是不允许的)
#   indent：定义缩进距离
#   sort_keys：是否通过字典键排序
# <p>
# todo json.dump()的语法格式：
#  json.dump(obj,fp,*,skipKeys=False,sort_keys=False,indent=None,**kw)
# <p>
# todo json.loads()的语法格式：
#  json.loads(json_str,*,cls=None,parse_float=None,parse_int=None,parse_constant=None,**kw)
# todo json.load()的语法格式：
#  json.load(fp,*,cls=None,parse_float=None,parse_int=None,parse_constant=None,**kw)
# <p>
# todo json.load()和json.loads()的区别：
#  > loads()传的json字符串，而load()传的是文件对象
#  > 使用loads()时需要先读取文件再使用,而load()则不用
#
# @Time : 2024/1/3 00:42
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
