# --*-- conding:utf-8 --*--
# todo：future对象的示例1
#
# @Time : 2024/4/8 14:38
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import asyncio


async def main():
    # 获取当前事件循环
    loop = asyncio.get_running_loop()

    # 创建一个任务(Future对象)，这个任务什么都不干
    fut = loop.create_future()

    # 等待任务最终结果（Future对象），没有结果则会一直等待下去
    await fut


if __name__ == '__main__':
    asyncio.run(main())
