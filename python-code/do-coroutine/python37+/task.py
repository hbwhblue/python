# --*-- conding:utf-8 --*--
# todo：事件循环中添加多个任务
#
# @Time : 2024/4/8 11:38
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import asyncio


async def t1():
    """
    异步任务1
    @return:
    """
    print(1)
    await asyncio.sleep(2)
    print(2)
    print('task1 end')
    return 't1'


async def t2():
    """
    异步任务2
    @return:
    """
    print(3)
    await asyncio.sleep(2)
    print(4)
    print('task2 end')
    return 't2'


async def main():
    """
    两个协程任务的执行，有点像 JAVA 中的线程通信机制notify...wait
    @todo 执行流程如下：
    1. 先执行t1协程，打印出1，接着sleep2秒,IO阻塞，t1协程挂起,Cpu执行权交个t2协程；
    2. 然后执行t2协程，打印3，接着sleep2秒,IO阻塞，t2协程挂起,Cpu执行权又交给t1协程；
    3. 再执行t1协程，接着上一次挂起位置继续往下执行，打印2，打印`task1 end`，返回`t1`，
    4. 最后执行t2协程，接着上一次挂起位置继续往下执行，打印4，打印`task2 end`，返回`t2`
    @return:
    """
    print('main开始')

    # 创建两个任务
    task1 = asyncio.create_task(t1())
    task2 = asyncio.create_task(t2())

    # 当执行task1时，遇到IO阻塞，就会挂起，然后去执行task2，同理,task2遇到IO阻塞，也会挂起，然后去执行task1
    # await是等待task1,task2 执行完毕获取结果
    res1 = await task1
    res2 = await task2
    print(res1, res2)

    print('main end')


if __name__ == '__main__':
    asyncio.run(main())
