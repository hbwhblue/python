# --*-- conding:utf-8 --*--
# todo：asyncio遇到不支持异步的模块，自动转换成异步的案例
#
# @Time : 2024/4/8 22:19
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import asyncio
import time

import requests

from tools import Tools


class DownloadImgs:
    """
    下载图片
    @param urls 图片链接:
    @return:
    """

    def __init__(self, urls):
        # 图片链接
        self.urls = urls
        # 请求头
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36'
        }
        # 图片下载路径
        self.img_path = Tools.create_dir(Tools.get_root_dir() + '/imgs/')

    def do_sync_download(self):
        """
        同步同步下载图片
        @return:
        """
        for url in self.urls:
            resp = requests.get(url, headers=self.headers)
            try:
                with open(self.img_path + url.split('/')[-1], 'wb') as f:
                    f.write(resp.content)
                    print(f'{url}下载完成')
                    time.sleep(1)
            except Exception as e:
                print(e)
        pass

    async def do_async_download(self, url, session):
        """
        异步下载图片
        @return:
        """
        print("开始下载图片：", url)
        # 获取事件驱动
        loop = asyncio.get_event_loop()
        # 因为 requests模块不支持异步，所以使用线程池来实现，调用 requests.sessions.Session.get()方法方法，参数是url
        fut = loop.run_in_executor(None, session.get, url)
        # 响应内容需要等待，使用await关键字
        response = await fut
        # 保存图片
        with open(self.img_path + url.split('/')[-1], 'wb') as f:
            f.write(response.content)
            print("下载完成：", url)
        pass


if __name__ == '__main__':
    urls = [
        'https://file.ncntv.com.cn/cms/2024-03-17/22036780/C2925EE8BB32F5A49AEEE3F364C407C8.jpeg',
        'https://img.pconline.com.cn/images/upload/upc/tx/itbbs/1403/30/c40/32630874_1396186802482.jpeg',
        'https://sghimages.shobserver.com/img/catch/2024/04/01/770f7787-012c-4a2b-88e1-29809e7812b7.jpg',
        'https://file.ncntv.com.cn/cms/2024-03-17/22036654/09760D27CF99CC1CC14178FE7713936C.jpeg',
    ]
    downloadImgs = DownloadImgs(urls)
    # downloadImgs.do_sync_download()

    # 将下载连接装载成任务
    session = requests.Session()
    session.headers = downloadImgs.headers
    tasks = [downloadImgs.do_async_download(url, session) for url in urls]

    # 执行任务
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait(tasks))
