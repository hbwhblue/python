# --*-- conding:utf-8 --*--
# todo：异步等待处理2
#
# @Time : 2024/4/8 01:04
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import asyncio


async def other1():
    print("other1-start")
    await asyncio.sleep(2)
    # 因为sleep是阻塞的，所以优先打印end
    print("other1-处理其他的任务")
    return 1


async def other2():
    print("other2-start")
    await asyncio.sleep(2)
    print("other2-处理其他的任务")
    return 2


async def main():
    print("执行协程函数的内部代码！")

    # todo 当遇到IO等待时，协程(任务)会挂起，那么就会执行其他协程(任务)，直到IO等待结束，程序再继续往下执行。
    # todo 这里的效果更像是同步等待，但是实际上是异步等待。
    response = await other1()
    print("IO等待结束,结果为：", response)

    print("----------------------------------")

    resp = await other2()
    print("IO等待结束,结果为：", resp)

    # todo 因为最终结果是需要等待两个任务完成，再进行计算，所以需要加await
    print('俩任务的处理结果是：', response + resp)


if __name__ == '__main__':
    asyncio.run(main())
