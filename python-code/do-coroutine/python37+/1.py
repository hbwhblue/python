# --*-- conding:utf-8 --*--
# todo：快速上手协程,协程的底层是采用事件循环来处理
#   注意：执行协程函数创建协程对象，函数内部代码不会执行
#   如果想要运行协程函数的内部代码，必须将协程对象交给事件循环来处理
#
# @Time : 2024/4/8 00:13
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import asyncio


async def test():
    print('test,hello,world')
    pass


if __name__ == '__main__':
    # 创建协程对象
    co = test()
    print("协程对象：", co)

    # 创建事件循环对象
    loop = asyncio.get_event_loop()
    # 将协程对象交给任务列表
    loop.run_until_complete(co)
    loop.close()

    # todo 使用asyncio.run()方法来处理协程对象,这等同于上面的事件循环代码
    # asyncio.run(co)
