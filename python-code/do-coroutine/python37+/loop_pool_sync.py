# --*-- conding:utf-8 --*--
# todo：在事件驱动线程池中将同步方法转换成异步方法
#
# @Time : 2024/4/8 18:11
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import asyncio
import time


def func1():
    """
    同步函数
    """
    time.sleep(2)
    return "hello"


async def main():
    """
    异步函数
    """
    # 获取当前执行的事件驱动，在事件循环中使用默认线程池
    loop = asyncio.get_running_loop()

    # todo 第一步：内部先调用ThreadPoolExecutor的submit方法，去线程池中申请一个线程去执行func1函数，并返回一个 concurrent.futures.Future对象
    # todo 第二步：调用 asyncio.wrap_future 将concurrent.future.Future 对象包装成 asyncio.Future 对象。
    # todo 因为concurrent.futures.Future对象不支持 await 语法，所以需要包装为asyncio.Future 对象才能使用。
    # todo 下面，就是将同步方法加入事件的线程池
    fut = loop.run_in_executor(None, func1)
    result = await fut
    print('在事件循环中使用默认线程池：', result)

    # 运行在自定义的线程池
    # with concurrent.futures.ThreadPoolExecutor() as pool:
    #     result = await loop.run_in_executor(pool, func1)
    #     print('自定义线程池：', result)

    # 运行在自定义的进程池
    # with concurrent.futures.ProcessPoolExecutor() as pool:
    #     result = await loop.run_in_executor(pool,func1)
    #     print('自定义进程池：',result)


asyncio.run(main())
