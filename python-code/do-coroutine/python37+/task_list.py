# --*-- conding:utf-8 --*--
# todo：任务列表来实现
#
# @Time : 2024/4/8 13:24
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import asyncio


async def t1():
    """
    异步任务1
    @return:
    """
    print(1)
    await asyncio.sleep(2)
    print(2)
    print('task1 end')
    return 't1'


async def t2():
    """
    异步任务2
    @return:
    """
    print(3)
    await asyncio.sleep(2)
    print(4)
    print('task2 end')
    return 't2'


async def main():
    """
    @todo 注意：这里在创键任务列表的时候，同时也创建了事件循环对象
    @return:
    """
    print('main start')

    # 将任务放入任务列表中,通过查看 create_task源码可以看到,它可以接收协程对象，协程名字，上下文对象
    task_list = [
        asyncio.create_task(t1(), name='t1'),
        asyncio.create_task(t2(), name='t2')
    ]
    # 等待任务任务全部执行完毕,done表示已经执行完毕的任务，pending表示未执行完毕的任务
    done, pending = await asyncio.wait(task_list)
    print("已经完成：", done)
    print("未完成：", pending)

    print('main end')


if __name__ == '__main__':
    # 启动事件循环，事件循环在协程main()中，已经创建了
    asyncio.run(main())
