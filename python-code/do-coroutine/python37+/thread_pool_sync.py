# --*-- conding:utf-8 --*--
# todo：使用concurrency.futures.ThreadPoolExecutor实现线程池
#
# @Time : 2024/4/8 17:27
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import time
from concurrent.futures.thread import ThreadPoolExecutor


def func(value):
    time.sleep(1)
    print(value)


if __name__ == '__main__':
    """
    todo 这里使用线程池+异步任务的方式，可以实现并发执行
    这里的线程池采用4个线程来处理10个任务，通过执行效果来看，任务是4个任务同时执行的，执行完再执行，后面4个任务
    """
    # 创建线程池，因为电脑是4核，启动4个线程
    pool = ThreadPoolExecutor(max_workers=4)

    # 创建进程池
    # pool = ProcessPoolExecutor(max_workers=4)

    # 循环10次，往线程池中提交10次
    for i in range(10):
        # 提交任务到线程池中，返回的是线程池中的Future对象,这和异步任务的Future的不一样，因为这里的func不是异步的
        fut = pool.submit(func, i)
        print(fut)
