# --*-- conding:utf-8 --*--
# todo：示例2
#
# @Time : 2024/4/8 14:46
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import asyncio


async def set_after(fut):
    await asyncio.sleep(2)
    fut.set_result(11111)


async def main():
    # 获取当前事件循环
    loop = asyncio.get_running_loop()

    # 创建一个任务（Future对象），如果没有绑定事件，则这个任务永远不知道什么时候结束
    fut = loop.create_future()

    # 创建一个任务(Task 对象)，绑定了set_after函数,在函数内部 sleep2秒后，给fut设置结果
    # 手动设置future结果, 那么future就可以结束了
    await loop.create_task(set_after(fut))

    # 等待 future 的最终结果,否则就一直等待
    data = await fut
    print(data)


if __name__ == '__main__':
    asyncio.run(main())
