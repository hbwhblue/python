# --*-- conding:utf-8 --*--
# todo：await测试，await 是当前的处理结果，需要依赖上一个任务的结果，所以需要上一个任务完成后才能执行
#
# @Time : 2024/4/8 00:44
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import asyncio


async def test2():
    print("test2")
    # io等待2秒,如果有其他任务，就会切换到其他任务
    res = await asyncio.sleep(2)
    print("结束", res)


if __name__ == '__main__':
    # 执行协程对象
    asyncio.run(test2())
