# --*-- conding:utf-8 --*--
# @Time : 2023/12/31 00:31
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 数学函数
import math
import unittest


class TestMath(unittest.TestCase):
    def test_ceil(self):
        """
        向上取整
        """
        res = math.ceil(13.14)
        print(res)

        res = math.ceil(9.9)
        print(res)

        res = math.ceil(19.2)
        print(res)

    def test_floor(self):
        """
        向下取整
        """
        res = math.floor(12.15)
        print(res)

        res = math.floor(13.39)
        print(res)

        res = math.floor(11.97)
        print(res)
        
    def test_abs(self):
        """
        取绝对值
        """
        res = math.fabs(-9)
        print(res)

        res = math.fabs(9.4)
        print(res)

        res = abs(-8)
        print(res)
        
    def test_fmod(self):
        """
        取模，返回浮点数
        """
        res = math.fmod(4,2)
        print(res)

        res = math.fmod(5,2)
        print(res)

        res = math.fmod(10,3)
        print(res)
        
    def test_sqrt(self):
        """
        开平方，返回浮点数
        """
        res = math.sqrt(9)
        print(res)

        res = math.sqrt(16)
        print(res)
        
    def test_fsum(self):
        """
        返回序列中所有元素的和;返回值：浮点数
        """
        total = math.fsum(range(1,11))
        print(total)

        total2 = math.fsum(range(1,5))
        print(total2)

    def test_sum(self):
        """
        将一个序列的数值进行相加求和；返回值:数值类型
        """
        res = sum((1,2,3,4))
        print(res)

        res = sum(range(1,11))
        print(res)

    def test_modf(self):
        """
        将一个浮点数拆成小数 和 整数部分组成的元祖；返回值：元祖
        """
        res = math.modf(10.2)
        print(res)

        res = math.modf(9.5)
        print(res)

        res = math.modf(8.3)
        print(res)
        
    def test_trunc(self):
        """
        返回浮点数的整数部分,返回值：整数
        """
        res = math.trunc(2.1)
        print(res)

        res = math.trunc(3.9)
        print(res)
        