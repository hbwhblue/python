# --*-- conding:utf-8 --*--
# @Time : 2023/12/31 01:44
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 精确的浮点数的精度
import decimal
import unittest


class TestDecimal(unittest.TestCase):
    def test_float(self):
        print(2.03 + 3.02)

    def test_decimal(self):
        res = decimal.Decimal(2.03) + decimal.Decimal(3.02)
        print(res)
