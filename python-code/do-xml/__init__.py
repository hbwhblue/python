# --*-- conding:utf-8 --*--
# todo：xml的两种方式
#  一、ElementTree方式
#    > ElementTree模块提供了一个轻量级、Pythonic的API，同时还有一个高效的C语言实现，即xml.tree.cElementTree
#    > 与DOM相比，ET的速度更快,API直接、方便。与SAX相比,ET.iterparse函数同样提供了按需解析的功能,不会一次性在内存中读入整个文档。
#    > ET的性能与SAX模块大致相仿，但它的API更加高层次,用户使用起来更佳便捷。
#  Element对象方法：
#  类方法                                              说明
#  Element.iter(tag=None)                            遍历该Element所有节点(递归式将所有子元素遍历)，也可以指定tag进行遍历寻找
#  Element.iterfind(path,namespaces=None)            根据tag或path查找所有的后代
#  Element.itertext()                                遍历所有后代并返回text值
#  Element.findall(path)                             查找当前元素下tag或path能够匹配的直系节点
#  Element.findtext(path,default=None,namespace=None)寻找第一个匹配子元素，返回其text值。匹配对象可以为tag或path
#  Element.find(path)                                查找当前元素下tag或path能够匹配的首个直系子节点。
#  Element.findtext                                  获取当前元素的text值
#  Element.get(key,default=None)                     获取元素指定key对应的属性值，如果没有该属性，则返回默认值
#  Element.keys()                                    获取元素属性名称列表
#  Element.items()                                   返回(name,value)列表
#  Element.getchildren()                             返回当前元素的子节点
#  Element.getiterator(tag=None)                     获得这个标签下的，所有迭代器数据
#  Element.getiterator(self,tag=None)                获得当前元素下的所有标签的所有迭代器数据
# <p>
# todo
#  Element属性方法
#  方法名                                              说明
#  Element.tag                                       节点名(tag) str类型
#  Element.attrib                                    属性(attributes) dict类型
#  Element.text                                      文本(text) str类型
#  Element.tail                                      附加文本(tail) str类型
#  Element[:]                                        子节点列表(list) list类型
#
# @Time : 2024/1/4 14:33
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm