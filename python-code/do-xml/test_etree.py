# --*-- conding:utf-8 --*--
# todo：ElementTree的测试
#
# @Time : 2024/1/4 15:19
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import unittest
import xml.etree.ElementTree as ET


class TestElementTree(unittest.TestCase):
    def test_elementTree(self):
        """
        加载文档并解析
        """
        # 1.加载文档
        tree = ET.ElementTree(file="test.xml")

        # 2.获取根元素
        rootObj = tree.getroot()
        print("根元素标签：", rootObj.tag)
        print("根元素属性值：", rootObj.attrib)
        print(rootObj)

        # 3.根元素遍历直接子元素接口
        for child_of_root in rootObj:
            print("直接子节点标签=", child_of_root.tag, "直接子节点属性=", child_of_root.attrib, "文本=",
                  child_of_root.text)

    def test_iter(self):
        """
        iter方法可以对某个元素对象之下所有的子元素进行深度优先遍历
        """
        # 1.加载文档
        tree = ET.ElementTree(file="collection.xml")
        # 2.获取根元素
        rootObj = tree.getroot()
        # 3.遍历所有子节点元素
        for elem in rootObj.iter():
            print("标签=", elem.tag, "属性=", elem.attrib)

    def test_iter_tag(self):
        """
        通过标签过滤文档
        """
        # 加载文档
        tree = ET.ElementTree(file="collection.xml")
        # 获取根元素
        rootObj = tree.getroot()
        # 查找需要的子元素
        for e in rootObj.iter(tag="teacher"):
            print(e.tag, e.text)

    def test_iter_xpath(self):
        """
        通过xpath过滤文档
        """
        # 加载文档
        tree = ET.ElementTree(file="collection.xml")
        # 获取根元素
        rootObj = tree.getroot()
        # 查找需要的子元素
        for e in rootObj.iterfind("class/teacher"):
            print(e.tag, e.text)

    def test_iter_selector(self):
        """
        通过标签下的属性过滤文档
        """
        # 加载文档
        tree = ET.ElementTree(file="test.xml")
        # 获取根元素
        rootObj = tree.getroot()
        # 查找需要的子元素
        for e in rootObj.iterfind("student[@stid='10214']"):
            print(e.tag, e.attrib)
            print(e.items())

    def test_iter_enum(self):
        """
        解析内容
        """
        # 加载文档
        tree = ET.ElementTree(file="collection.xml")
        # 获取根元素对象
        rootObj = tree.getroot()
        # 通过枚举遍历
        for idx, child in enumerate(rootObj):
            print("第%s个%s元素，属性：%s" % (idx, child.tag,child.attrib))
            for i,child_child in enumerate(child):
                print("标签：%s,内容：%s") % (child_child.tag,child_child.text)



if __name__ == '__main__':
    unittest.main()
