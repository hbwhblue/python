#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : TestDict.py
@Time       : 2023/6/20 22:49
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 字符串
"""


def no_parse():
    """
    字符串不转义转义
    :return:
    """
    string = "hello,world\n"
    print("原始输出：", string)
    print(r"hello,world\n")  # 不转义


# no_parse()


def str_case():
    """
    大小写相关操作
    :return:
    """
    s = "hello,world"
    print("字符串首字母大写：", s.capitalize())
    print("单词首字母大写：", s.title())
    print("所有字母转换成大写：", s.upper())

    s2 = "GOOD,BYE"
    print("字符串转换成小写", s2.lower())  # 使用lower方法获得字符串小写后的字符串

    s3 = "I am Huangjinchao"
    words = s3.split()  # 默认按空格进行拆分
    print("字符串按空格分割：", words)
    print("按,分割：", "1,2,3,4".split(","))

    # 将一个列表用join连接成字符串
    new_str = "-".join(s3)
    print("new_str:", new_str)


str_case()


def str_strip():
    s = " hello,world "
    print("#" + s.lstrip() + "#")  # 去掉左侧空格
    print("#" + s.strip() + "#")  # 去掉左右空格
    print("#" + s.rstrip() + "#")  # 去掉右侧空格


str_strip()
