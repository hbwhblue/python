# --*-- conding:utf-8 --*--
# @Time : 2023/12/21 03:22
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 测试lamda表达式
import unittest


class LambdaTest(unittest.TestCase):
    def test_lamda1(self):
        """
        使用map函数，将列表的每一个函数应用于lamda表达式中，lamda表达式类似于匿名函数
        并且map函数的结果，转换成list接口
        f(x):
            return x*x
        :return:
        """
        newList = list(map(lambda x: x * x, [1, 2, -3, 4, 5, -6, 7, 8, 9]))
        print(newList)
        pass

    def test_lamda2(self):
        """
        lambda表达式，相当于是匿名函数
        :return:
        """
        f = lambda x: x * x
        print(f)  # 打印类型
        print(f(6))  # 打印匿名函数的值
        pass

    def test_lamda3(self):
        res = build(2, 3)
        print(res())
        pass


def build(x, y):
    """
    匿名函数作为返回值返回
    """
    return lambda: x * x + y * y
    pass
