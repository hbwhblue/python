#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : application.py
@Time       : 2023/6/30 15:05
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 函数的应用
"""
import operator
import random
import string
from os.path import splitext

ALL_CHARS = string.digits + string.ascii_letters


def generate_code(code_len=4):
    """
    生成指定长度的验证码
    :param code_len:验证码长度,默认是4
    :return:有大小写字母，数字构成的随机验证码字符串
    """
    res = "".join(random.choices(ALL_CHARS, k=code_len))
    return res


code = generate_code()
print("code=", code)


def get_suffix(file_name, ignore_dot=True):
    """
    返回文件的后缀
    :param file_name: 文件名
    :param ignore_dot: 是否忽略后缀名前面的那个点
    :return: 文件的后缀名
    """

    # 从字符串中逆向查找.出现的位置
    pos = file_name.rfind(".")

    # 通过切片操作从文件名中取出后缀名
    if pos <= 0:
        return ""

    if ignore_dot:
        return file_name[pos + 1:]
    return file_name[pos:]


# suffix = get_suffix("readme.txt", False)
# print("suffix=", suffix)


def get_suffix2(file_name):
    return splitext(file_name)


# ext_info = get_suffix2("reademe.txt")
# print(ext_info[1][1:])


def is_prime(num: int) -> bool:
    """
    判断一个正整数是不是质数
    :param num: 正整数 int
    :return: True|False
    """

    for i in range(2, int(num**0.5) + 1):
        if num % i == 0:
            return False
    return num != 1


# num = int(input("请输入一个正整数："))
# res = is_prime(num)
# print(f"{num}是否是质数--->{res}")


def gcd(x, y: int) -> int:
    """
    求最大公约数
    :param x:
    :param y:
    :return:
    """

    while y % x != 0:
        x, y = y % x, x

    return x


# x = int(input("请输入x的值："))
# y = int(input("请输入y的值："))
# res = gcd(x, y)
# print("最大公约数res=", res)


def lcm(x: int, y: int) -> int:
    """
    求最小公倍数
    :param x:正整数1
    :param y:正整数2
    :return:
    """
    return x * y // gcd(x, y)  # 整除最大公约数


x = int(input("请输入x的值："))
y = int(input("请输入y的值："))
res = lcm(x, y)
print("最小公倍数：", res)
