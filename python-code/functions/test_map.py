# --*-- conding:utf-8 --*--
# @Time : 2023/12/20 22:41
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 测试高阶函数map
import unittest


class MapTest(unittest.TestCase):
    def test1(self):
        sum = add(5, -6, abs)
        print(sum)
        pass

    def test2(self):
        """
        todo 1.map(arg1,arg2)函数接收两个参数，arg1是回调函数名，arg2是函数参数，Iterable类型
        todo 2.map将传入的arg1函数依次作用到arg2序列的每个元素,并将结果作为新的迭代器(Iterator)返回。
        :return:
        """
        itera = map(abs, [-1, -2, -4, -3, -5, -6])
        print(itera)
        # 再将迭代器对象转换成列表
        lst = list(itera)
        print(lst)
        pass

    def test3(self):
        """
        将序列的每个元素进行平方计算
        :return:
        """
        itra = map(squNum, [-1, 2, 4, -5])
        print(itra)
        # 将迭代器对象转为列表
        lst = list(itra)
        print(lst)
        pass

    def test4(self):
        """
        将序列的每个元素转换成字符串
        :return:
        """
        itra = map(str, [1, 2, 3, 4, 5, 6, 7, 8, 9])
        lst = list(itra)
        print(lst)
        pass

    def test5(self):
        """
        回调函数的参数除了列表，也可以是多个参数
        @return:
        """
        itra = map(squNum, [-3, -4, -5])
        lst = list(itra)
        print(lst)
        pass


def add(x: int, y: int, fn: object) -> object:
    """
    简单的高阶函数
    :rtype: object
    :param x: int
    :param y: int
    :param fn: 函数类型
    :return:
    """
    return fn(x) + fn(y)


def squNum(x: int) -> int:
    return x * x
    pass
