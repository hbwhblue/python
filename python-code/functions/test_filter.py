# --*-- conding:utf-8 --*--
# @Time : 2023/12/20 23:37
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 过滤序列
import math
import unittest


class FilterTest(unittest.TestCase):
    def test1(self):
        """
        通过filter筛选出偶数列表
        :return:
        """
        evenList = list(filter(is_even, [1, 2, 5, 4, 7, 9, 16]))
        print("偶数序列：", evenList)
        pass

    def test2(self):
        sqrtList = list(map(math.sqrt, filter(is_even, [4, 16, 64, 256])))
        print("偶数求平方根：", sqrtList)
        pass

    def test3(self):
        for n in createPrime():
            if n < 1000:
                print(n)
            else:
                break
        pass


pass


def is_even(n):
    """
    是否是偶数
    :param n: 数值
    :return: 是否是偶数的判断
    """
    return n % 2 == 0


def odd_iter():
    """
    生成奇数的序列生成器
    :return:
    """
    n = 1
    while True:
        n = n + 2
        yield n


def not_divisible(n):
    """
    求余数大于0的数,也就是质数
    :param n:
    :return:
    """
    return lambda x: x % n > 0


def createPrime():
    """
    todo 创建质数的序列生成器
     每yield一次，就生成一个质数 return，程序就暂停，然后指针指向下一个元素的位置
    @return:
    """

    yield 2
    it = odd_iter()

    while True:
        n = next(it)  # 返回序列的第一个元素
        yield n
        it = filter(not_divisible(n), it)
