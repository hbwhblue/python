#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : higher_order_func.py
@Time       : 2023/7/4 01:16
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 高阶函数
"""
import operator


def ho_func(*args, init_value, op, **kwargs):
    """
    高阶函数
    :param args: 位置参数列表
    :param init_value: 运算结果初始值
    :param op: 运算符
    :param kwargs: 关键字参数
    :return: 计算结果
    """
    result = init_value
    for arg in args:
        if type(arg) in (int, float):
            result = op(result, arg)

    for val in kwargs.values():
        if type(val) in (int, float):
            result = op(result, val)
    return result


def add(x, y):
    return x + y


def mul(x, y):
    return x * y


# todo: 需要注意的是，将函数作为参数和调用函数是有显著的区别的，调用函数需要在函数名后面跟上圆括号，而把函数作为参数时只需要函数名即可。
res1 = ho_func(1, 2, 3, init_value=0, op=add, x=5, y=6)
print("res1=", res1)
res2 = ho_func(1, 2, x=3, y=5, z=6, init_value=1, op=mul)
print("res2", res2)

# todo：上面的代码也可以不用定义add和mul函数，因为Python标准库中的operator模块提供了代表加法运算的add和代表乘法运算的mul函数，我们直接使用即可
res1 = ho_func(1, 2, 3, x=5, y=6, init_value=0, op=operator.add)
print("res1=", res1)
res2 = ho_func(1, 2, x=3, y=5, z=6, init_value=1, op=operator.mul)
print("res2=", res2)


def is_enven(num):
    """
    验证是否是偶数
    :param num:正整数
    :return:
    """
    return num % 2 == 0


def square(num):
    """
    求平方
    :param num:
    :return:
    """
    return num**2


lst = [25, 12, 3, 8, 10, 9]
# 方法一
res = [item**2 for item in lst if item % 2 == 0]
print("res=", res)

# 方法二
# map和filter都是高阶函数，map是对序列中的元素进行映射，即让序列中的每个元素作为参数去调用自定义函数
# filter对将序列的元素进行过滤
res = list(map(square, filter(is_enven, lst)))
print("res=", res)


def format_str(strs: str) -> str:
    return strs.upper()


new_str = list(map(format_str, "abcdefghijk"))
print("new_str=", "".join(new_str))
