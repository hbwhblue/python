#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : test_generator.py
@Time       : 2023/6/28 10:53
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 函数的应用
"""
import random


def fac(num):
    """
    计算阶乘
    :param num: 正整数
    :return: 正整数
    """
    res = 1
    for item in range(1, num + 1):
        res = res * item
    return res


num = int(input("请输入一个正整数:"))
res = fac(num)
print("阶乘的结果：", res)


def default_param(n=2):
    """
    根据色子的个数计算出点数总和
    :param n: int 色子个数
    :return: int 总点数
    """

    # 返回摇色子返回的总点数
    total = 0
    for _ in range(n):
        total += random.randint(1, 6)
    return total


# print("3个色子的总点数：", default_param(3))


def changeable_param(*args):
    total = 0
    for item in args:
        if type(item) in (int, float):
            total += item
    return total


print("数字动态求和：", changeable_param(1, 2, 3, "a"))


def min_max(arr):
    """
    返回多个值，得到最大数和最小数
    :param arr:
    :return:
    """
    min_num = arr[0]
    max_num = arr[0]
    for item in arr:
        if item < min_num:
            min_num = item
        if item > max_num:
            max_num = item

    return min_num, max_num


print("做小数和最大数：", min_max([98, 321, 2, 34, 1, 56]))


# 斐波那契数列
def fibonacci(n):
    if n <= 1:
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


res = fibonacci(7)
print("斐波那契数列：", res)
