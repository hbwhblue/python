#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

"""
@File       : params.py
@Time       : 2023/6/30 15:05
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 函数参数
"""

i = 5


# 默认形参是函数定义时就设定好的，只在函数定义时计算一次，所以每次函数调用这个默认形参时，始终指向的都是初始化的对象
def f(arg=i):
    print(arg)


# i改变了，并不会改变函数的默认参数
i = 6
f()


# todo 如果默认参数是一个可变对象，也就是JAVA 中的引用类型，则每次函数调用时，如果对这个默认形参引用的这个对象进行修改，则修改的是同一个对象
def f2(var: object, arrList: object = None) -> object:
    # if arrList is None:
    #     arrList = []
    arrList.append(var)
    return arrList


# arr引用的是空的list对象，函数调用f2(1),f2(2)中的arr也始终引用的是同一个list对象
arrList = []
print(f2(1, arrList))
print(f2(2, arrList))


# exit(1)


def is_triangle(x, *, a, b, c):
    """
    :todo 利用关键字参数，判断是否能构成三角形,参数"*"表示分割符，
        "*"后面的使用关键词参数(参数名=值)，可以改变位置，不是固定，
        "*"前面是位置参数，不能改变位置。
    :param a:长
    :param b:宽
    :param c:高
    :return:True|False
    """

    print("x=", x)
    print(f"a={a},b={b},c={c}")
    return a + b > c and a + c > b and b + c > a


# print(is_triangle(0, a=3, c=4, b=5))
# exit(0)


def calc(*args):
    """
    可变参数，带一个*只能使用位置参数列表,不能用关键词参数
    :param args:
    :return:
    """
    res = 0
    for arg in args:
        if type(arg) in (int, float):
            res += arg
    return res


# res = calc(1, 2, 3.14)
# res = calc(a=1, b=2, c=3.14)  #错误
# print("res=%.2f" % res)
# exit(0)

def calc1(**kwargs):
    for i, item in enumerate(kwargs.items()):
        print(f"关键词参数: {i}={item}：")


dictParams = dict(uid=1, unme="hjc_042043", emal="hjc_042043")
calc1(**dictParams);


def calc2(*args, **kwargs):
    """
    可变参数和可变位置参数
    :param args: 列表或元祖，来设置位置参数
    :param kwargs: 字典,来设置关键词参数
    :return:
    """
    for arg in args:
        print("位置参数=", arg)

    res = 0
    for item in kwargs.values():
        res += item
    print("关键词参数求和：", res)


calc2("a", "b", "c", x=1, y=2, z=3)


# 解封位置参数列表
def add(x, y):
    return x + y


params = [1, 2]
print("利用list解封位置参数列表", add(*params))


# todo 利用字典解封关键词参数列表
def get_user_info(user_name, email, sex):
    print(f"user_name={user_name},email={email},sex={sex}")


user_dict = {"user_name": "hjc", "email": "hjc_042043@sina.cn", "sex": "1"}
get_user_info(**user_dict)
