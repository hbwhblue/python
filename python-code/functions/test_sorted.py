# --*-- conding:utf-8 --*--
# @Time : 2023/12/21 01:27
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 对迭代器进行排序测试
import unittest


class SortedTest(unittest.TestCase):
    """
    sorted和sort的区别：
    1.sort是列表的对象方法，只对列表内部进行排序，并不会产生新的列表
    2.sorted是python的内置函数，能够对任何迭代对象进行排序，并返回一个新的列表
    3.sort没有返回值，sorted是返回一个新的列表，可以将结果赋值给一个变量
    """

    def test_asc(self):
        """
        升序测试
        :return:
        """
        lst = [36, 5, -12, 0, -21]
        newList = sorted(lst)
        print(newList)
        pass

    def test_desc(self):
        """
        sorted()函数也是一个高阶函数，它还可以接收一个key函数来实现自定义的排序，
        key指定的函数将作用于list的每一个元素上，并根据key函数返回的结果进行排序,例如按绝对值大小排序：
        :return:
        """
        lst = [36, 5, -12, 0, -30]
        newList = sorted(lst, key=abs, reverse=True)
        print(newList)
        pass

    def test_str_asc(self):
        """
        字符串列表进行升序
        默认情况下,对字符串排序,是按照ASCII码的大小进行比较,由于'F' < 't',所以大写字母F会排在小写字母a的前面
        :return:
        """
        lst = sorted(["One", "two", "three", "Fore"])
        print(lst)

        pass

    def test_str_desc(self):
        """
        给sorted传入key函数，按字母忽略大小写，按倒序排
        :return:
        """
        lst = sorted(["One", "two", "three", "Fore"], key=str.lower, reverse=True)
        print(lst)
        pass

    def test_score_desc(self):
        """
        根据列表中元祖的第二个元素进行排序
        :return:
        """
        lst = [('Tom', 75), ('Jack', 90), ('Lisa', 85), ('Bart', 66)]

        newList = sorted(lst, key=lambda item: item[1], reverse=True)  # 根据lambda表达式的结果，元组中的第二个元素进行排序
        print(newList)
        pass

    def test_score_desc_dict(self):
        """
        根据字典的value进行排序
        @return:
        """
        dict_data = {
            'Tom': 75,
            'Jack': 90,
            'Lisa': 85,
            'Bart': 66
        }
        sort_list = sorted(dict_data.items(), key=lambda item: item[1], reverse=True)
        print("sorted 后的结果：", sort_list)

        # 对 sort_list 从新组合成一个字典
        dict_data = {item[0]: item[1] for item in sort_list}
        print("最终的dict_data:", dict_data)
        pass
