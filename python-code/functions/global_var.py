#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : global_var.py
@Time       : 2023/6/28 12:04
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 全局变量和局部变量在函数中的使用
"""
x = "global"


# 全局变量在函数中访问
# def test():
#     print("x inside", x)
#
#
# test()
# print("x ouotside", x)


def change_global():
    """
    在函数体内，如果要改变全局变量的值，就必须要加上最开始加上global关键字
    如下例，x=的语句相当于定义了局部变量，因此函数里的x就不是全局变量的那个x了
    :return:
    """

    # 直接赋值是错误的
    # x = x * 2

    # 正确的姿势
    global x
    x = x * 2
    print("change global x=", x)


change_global()
