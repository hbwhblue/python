# --*-- conding:utf-8 --*--
# @Time : 2023/12/21 03:00
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 将函数作为返回值
import unittest


class ReturnFunctionTest(unittest.TestCase):
    """
    将函数作为返回值
    """

    def test_sum(self):
        sum = lazy_sum(1,2,3,4,5,6)
        print(sum)  # 打印函数类型
        print(sum())


def lazy_sum(*args):
    def sum() -> object:
        total = 0
        for arg in args:
            total += arg
        return total

    return sum