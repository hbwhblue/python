#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@NAME      :Untitled-1
@TIME      :2022/05/21 14:22:58
@AUTHOR    :hjc_042043@sina.cn
@VERSION   :1.0
"""

"""
@description  : 测试匿名函数
---------
@param  : 数值
-------
@Returns  : 最终结果
-------
"""


# 对列表进行lambda测试
def testLambda(lst):
    # todo map函数是对列表的每个元素进行处理，返回一个迭代器对象，lambda是匿名函数,表达式是"函数参数:函数体"，lambda没有返回值
    # todo 最外层的list()表示类型转换,将迭代器对象转换成列表
    new_lst = list(map(lambda x: x * x, lst))
    return new_lst


# 对字典进行排序


def sortDic():
    """
    todo 对字典的键值进行排序
    @return:
    """
    dict = {"rb1901": 3100, "a1901": 3200, "m1901": 3450, "bu1901": 2700}
    # 对字典的键进行升序排序
    sortedDict = sorted(dict.items(), key=lambda x: x[0])
    # 输出结果
    print("升序排序：", sortedDict)

    # 字典的键进行降序排序
    sortedDict2 = sorted(dict.items(), key=lambda x: x[1], reverse=True)
    # 输出结果
    print("降序排序：", sortedDict2)


def module_test():
    print("lambda")


def f(x):
    """
    todo 这等同于匿名函数lambda x: x * x
        左边 x表示函数的参数，
        右边 x*x是return的值
    :param x:
    :return:
    """
    return x * x


def main():
    lst = [1, 2, 3, 4, 5]
    new_lst = testLambda(lst)
    print("testLambda", new_lst)

    # 通过字典的键进行排序
    sortDic()


if __name__ == "__main__":
    main()
