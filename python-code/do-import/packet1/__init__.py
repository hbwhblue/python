# --*-- conding:utf-8 --*--
# todo：包测试
#
# @Time : 2024/3/12 17:41
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

# 有可能和其他模块有重名，所以用命名成别名
# 这表示从当前包中导入模块，前面的 from .可以省略
from . import develop as dev
from . import training as train

__all__ = ["art", "dev", "manage", "train"]
