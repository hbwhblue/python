# --*-- conding:utf-8 --*--
# @Time : 2023/12/22 03:19
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 异常处理,文件命名需要命名成test_xxx
import unittest


class ExceptionTest(unittest.TestCase):
    def test_except1(self):
        """
        一个完整的异常捕获例子
        :return:
        """
        try:
            print("try...")
            r = 10 / 0
            print("result：", r)
        except FloatingPointError:
            print("浮点数不正确")
        except ValueError:
            print("请输入正确的整数！")
        except (ZeroDivisionError, OverflowError):
            # 针对被除数=0，超出最大整数范围
            print("整数范围不正确！")
        except Exception as e:
            # 未知的錯誤异常
            print("exception:", e)
        else:
            # 代码不发生异常处理...
            print("代码正常运行！")
        finally:
            print("是否有异常都会执行")

        print("END")
        pass

