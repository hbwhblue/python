# --*-- conding:utf-8 --*--
# @Time : 2023/12/23 23:46
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 抛出异常测试
import unittest


class RaiseTest(unittest.TestCase):
    def test1(self):
        try:
            user_pwd = self.input_password()
            print(user_pwd)
        except Exception as e:
            print(f"错误信息：{e}")
            pass
        pass

    def test2(self):
        try:
             res = self.division(5,0)
             print(res)
        except Exception as e:
            print("运行异常："+str(e))
            pass

    def test3(self):
        try:
            res = self.division2(12,0)
        except MyError as e:
            print("运行异常：" + str(e))
            pass

    def input_password(self):
        pwd = input("请输入密码：")
        if len(pwd) > 8:
            return pwd

        raise Exception("密码长度不能为空！")
    
    def division(self,a:int,b:int) -> float:
        if b == 0:
            raise Exception("参数错误，b应该是大于0") # 抛出异常
        return a/b

    def division2(self,a:int,b:int) -> float:
        if b == 0:
            raise MyError("参数错误，b应该是大于0") # 抛出异常
        return a/b


class MyError(Exception):
    """
    自定义错误，继承父类Exception
    """
    pass
