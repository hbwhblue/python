# --*-- conding:utf-8 --*--
# todo：
#
# @Time : 2024/3/9 14:47
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import unittest
from pprint import pprint

from db.mongo_pool import MongoPool


class TestCURDCase(unittest.TestCase):
    def test_insert_one(self):
        """
        单条插入数据
        @return:
        """
        insert_data = {
            "uid": 1,
            "name": "0001",
            "real_name": "黄飞鸿",
            "email": "000@sina.cn",
            "age": 30,
            "sex": 1,
        }
        res = MongoPool().test.user.insert_one(insert_data)
        print(res.inserted_id)
        pass

    def test_insert_many(self):
        """
        批量插入数据
        @return:
        """
        user_list = [
            {"uid": 2, "name": "0002", "real_name": "张三", "email": "001@sina.cn", "age": 29, "sex": 1},
            {"uid": 3, "name": "0003", "real_name": "魏延", "email": "002@sina.cn", "age": 28, "sex": 1},
            {"uid": 4, "name": "0004", "real_name": "刘备", "email": "003@sina.cn", "age": 38, "sex": 1},
            {"uid": 5, "name": "0005", "real_name": "关羽", "email": "004@sina.cn", "age": 32, "sex": 1},
            {"uid": 6, "name": "0006", "real_name": "张飞", "email": "005@sina.cn", "age": 33, "sex": 1},
            {"uid": 7, "name": "0007", "real_name": "赵云", "email": "006@sina.cn", "age": 21, "sex": 1},
            {"uid": 8, "name": "0008", "real_name": "马超", "email": "007@sina.cn", "age": 22, "sex": 1},
            {"uid": 9, "name": "0009", "real_name": "黄忠", "email": "008@sina.cn", "age": 24, "sex": 1},
            {"uid": 10, "name": "0010", "real_name": "孙尚香", "email": "009@sina.cn", "age": 28, "sex": 2},
            {"uid": 11, "name": "0011", "real_name": "黄月英", "email": "0010@sina.cn", "age": 27, "sex": 2},
            {"uid": 12, "name": "0012", "real_name": "诸葛亮", "email": "0011@sina.cn", "age": 29, "sex": 1},
        ]
        res = MongoPool().test.user.insert_many(user_list)
        print(res.inserted_ids)
        pass

    def test_insert_custom_ids(self):
        """
        批量插入数据，自定义_id
        @return:
        """
        ids_list = [
            {"_id": 1, "name": "曹操"},
            {"_id": 2, "name": "曹丕"},
            {"_id": 3, "name": "典韦"},
            {"_id": 4, "name": "许诸"}
        ]
        res = MongoPool().test.user_ids.insert_many(ids_list)
        print(res.inserted_ids)
        pass

    def test_update_one(self):
        """
        更新一条记录
        @return:
        """
        # 更新条件
        condition = {"uid": 5}
        # 更新的数据
        update_data = {"email": "0004@163.com"}
        res = MongoPool().test.user.update_one(filter=condition, update={"$set": update_data})
        print(res.modified_count)
        pass

    def test_update_upsert(self):
        """
        更新一条记录，存在更新，不存在新增
        @return:
        """
        # 更新的数据
        update_data = {
            "uid": 13,
            "name": "0013",
            "real_name": "铁桥三",
            "email": "013sina.cn",
            "age": 27,
            "sex": 1,
        }
        # 更新条件
        condition = {"uid": 13}
        # 更新的选项，存在更新，不存在新增
        upsert_option = True
        res = MongoPool().test.user.update_one(filter=condition, update={"$set": update_data}, upsert=upsert_option)
        print(res)
        pass

    def test_update_many(self):
        """
        更新多条记录，更新年龄大于30岁的人，武力值更新为95
        @return:
        """
        # 更新的数据
        update_data = {"$set": {"force_value": 95}}
        # 更新条件
        condition = {"age": {"$gt": 30}}
        # 更新选项，存在更新，不存在就新增
        upsert_option = True
        res = MongoPool().test.user.update_many(filter=condition, update=update_data, upsert=upsert_option)
        print(res)
        pass

    def test_update_many_init(self):
        """
        更新多条记录，筛选出name包含000的人员，武力值更新为90，初始化登录次数
        @return:
        """
        # 更新的数据,$setOnInsert类似新增数据后进行初始化操作
        update_data = {"$set": {"force_value": 90}, "$setOnInsert": {"login_nums": 1}}
        # 更新条件
        condition = {"name": {"$regex": "^000"}}
        # 更新选项，存在更新，不存在就新增
        upsert_option = True
        res = MongoPool().test.user.update_many(filter=condition, update=update_data, upsert=upsert_option)
        print(res)
        pass

    def test_delete_one(self):
        """
        删除单条数据
        @return:
        """
        res = MongoPool().test.user.delete_one({'uid': 1})
        print(res.deleted_count)

    def test_delete_many(self):
        """
        批量删除uid<=2的记录
        @return:
        """
        condition = {"uid": {"$lte": 2}}
        res = MongoPool().test.user.delete_many(condition)
        print(res)

    def test_delete_all(self):
        """
        批量删除所有数据,这个得慎用
        @return:
        """
        res = MongoPool().test.user.delete_many({})
        print(res)

    def test_find_one(self):
        """
        查询一条数据
        @return:
        """
        coll = MongoPool().test.user
        data = coll.find_one({"uid": 1})
        print(data)
        pass

    def test_find_all(self):
        """
        查询所有数据，不建议查找全部数据，影响性能
        @return:
        """
        coll = MongoPool().test.user
        list_data = coll.find()
        for data in list_data:
            print(data)
        pass

    def test_find_and(self):
        """
        and条件，查询sex=1并且年龄大于30岁的数据
        @return:
        """
        coll = MongoPool().test.user
        # 多个and条件用列表表示
        condition = [
            {"sex": 1},
            {"age": {"$gte": 30}}
        ]
        list_data = coll.find({"$and": condition})
        for data in list_data:
            pprint(data)

    def test_find_or(self):
        """
        或条件，查询sex=2或者年龄大于35岁的数据
        @return:
        """
        coll = MongoPool().test.user
        # 多个or条件用列表表示
        condition = [
            {"sex": 2},
            {"age": {"$gte": 35}}
        ]
        list_data = coll.find({"$or": condition})
        for data in list_data:
            pprint(data)

    def test_find_eq(self):
        """
        查询sex=2的数据
        @return:
        """
        coll = MongoPool().test.user
        data_list = coll.find({"sex": 2})
        for data in data_list:
            print(data)

    def test_find_ne(self):
        """
        查询sex!=1的数据
        @return:
        """
        coll = MongoPool().test.user
        data_list = coll.find({"sex": {"$ne": 1}})
        for data in data_list:
            print(data)

    def test_find_more_filter(self):
        """
        查询sex=1 并且，年龄大于30岁的数据
        @return: 
        """
        coll = MongoPool().test.user
        data_list = coll.find({"sex": 2, "age": {"$gte": 30}})
        for data in data_list:
            print(data)

    def test_find_gt(self):
        """
        查询age大于30岁的数据
        @return:
        """
        coll = MongoPool().test.user
        data_list = coll.find({"age": {"$gt": 30}})
        for data in data_list:
            print(data["uid"])

    def test_find_gte(self):
        """
        查询 age 大于等于35岁的数据
        @return:
        """
        data_list = MongoPool().test.user.find({"age": {"$gte": 35}})
        for data in data_list:
            print(data["real_name"])

    def test_find_lt(self):
        """
        查询 age 小于30岁的数据
        @return:
        """
        data_list = MongoPool().test.user.find({"age": {"$lt": 30}})
        for data in data_list:
            print(data)

    def test_find_lte(self):
        """
        查询 age 小于等于27岁的数据
        @return:
        """
        data_list = MongoPool().test.user.find({"age": {"$lte": 27}})
        for data in data_list:
            print(data)

    def test_find_between(self):
        """
        查询 age 在 20到25 之间的数据
        @return:
        """
        data_list = MongoPool().test.user.find({"age": {"$gte": 20, "$lte": 25}})
        for data in data_list:
            print(data)

    def test_find_range(self):
        """
        范围查找,指定 uid来查找
        @return:
        """
        data_list = MongoPool().test.user.find({"uid": {"$in": [1, 2, 4, 5]}})
        for data in data_list:
            print(data)

    def test_find_like(self):
        """
        查询 name 包含 000的数据，类似mysql like
        这和mongodb的shell命令行不同，mongodb 的 shell 命令行是/000/
        """
        data_list = MongoPool().test.user.find({"name": {"$regex": "000"}})
        for data in data_list:
            print(data)

    def test_find_pos(self):
        """
        查找邮箱以某个字符串001开头的数据
        @return:
        """
        data_list = MongoPool().test.user.find({"email": {"$regex": "^001"}})
        for data in data_list:
            print(data)

    def test_find_rpos(self):
        """
        查找邮箱以某个字符串163.com结尾的数据
        @return:
        """
        data_list = MongoPool().test.user.find({"email": {"$regex": "163.com$"}})
        for data in data_list:
            print(data)

    def test_find_projection(self):
        """
        投影查找，只返回uid,real_name,age,sex
        注意：除了_id之外，其他字段的投影操作只能全部是1或0，不能共存，否则会报错
        @return:
        """
        # 筛选条件
        condition = {"age": {"$lte": 25}}
        # 字段_id 不显示，uid,real_name,age,sex显示
        data_list = MongoPool().test.user.find(condition, {"_id": 0, "uid": 1, "real_name": 1, "age": 1, "sex": 1})
        for data in data_list:
            print(data)

    def test_find_sort(self):
        """
        查找 sex=1,按年龄降序排序
        @return:
        """
        coll = MongoPool().test.user
        data_list = coll.find({"sex": 1}).sort({"age": -1})
        for data in data_list:
            print(data)

    def test_find_comp_sort(self):
        """
        复合排序，先按性别升序，再按武力值降序
        注意：排序字段的顺序，决定了排序的优先级
        @return:
        """
        coll = MongoPool().test.user
        data_list = coll.find({"age": {"$lt": 35}}).sort({"age": 1, "force_value": -1})
        for data in data_list:
            print(data)

    def test_find_page(self):
        """
        分页查找
        @return:
        """
        coll = MongoPool().test.user
        page_size = 10
        page_num = 1
        skip_num = (page_num - 1) * page_size
        data_list = coll.find().sort({"age": -1}).skip(skip_num).limit(page_size)
        for data in data_list:
            print(data)

    def test_find_count(self):
        """
        查找总数,条件，年龄小于35岁的人数
        @return:
        """
        coll = MongoPool().test.user
        count_num = coll.count_documents({"age": {"$lt": 35}})
        print(count_num)

    def test_aggr_group_sex(self):
        """
        $group分组示例1，分组查找，按性别分组，统计每个性别的人数
        @return:
        """
        coll = MongoPool().test.user
        groups = {
            "$group": {
                "_id": "$sex",
                "user_count": {"$sum": 1}
            }
        }
        # 这是一个游标对象，可以遍历出来
        res = coll.aggregate([groups])
        for data in res:
            print(data)

    def test_aggr_group_total(self):
        """
        $group分组示例2，分组查找，统计每个性别的总武力值
        @return:
        """
        coll = MongoPool().test.user

        # $sum 后面跟上要求和的字段
        groups = {
            "$group": {
                "_id": "$sex",
                "total_force_value": {"$sum": "$force_value"}
            }
        }
        res = coll.aggregate([groups])
        for data in res:
            print(data)

    def test_aggr_group_avg(self):
        """
        $group分组示例3，分组查找，统计每个年龄的平均武力值
        @return:
        """
        coll = MongoPool().test.user
        groups = {
            "$group": {
                "_id": "$age",
                "avg_force_value": {"$avg": "$force_value"}
            }
        }
        res = coll.aggregate([groups])
        for data in res:
            print(data)

    def test_aggr_group_max(self):
        """
        分组查找，统计每个性别的最大武力值
        @return:
        """
        coll = MongoPool().test.user
        groups = {
            "$group": {
                "_id": "$sex",
                "max_force_value": {"$max": "$force_value"}
            }
        }
        res = coll.aggregate([groups])
        for data in res:
            print(data)

    def test_aggr_group_addtoset(self):
        """
        分组查找，统计每个年龄的用户名字，武力值列表
        @return:
        """
        coll = MongoPool().test.user
        groups = {
            "$group": {
                "_id": "$age",
                "real_names": {"$addToSet": "$real_name"},
                "force_values": {"$addToSet": "$force_value"}
            }
        }
        res = coll.aggregate([groups])
        for data in res:
            print(data)

    def test_aggr_match1(self):
        """
        筛选出年龄大于27岁的人,小于等于35岁的人员信息
        @return:
        """
        coll = MongoPool().test.user
        matches = {
            "$match": {
                "age": {"$gt": 27, "$lte": 35}
            }
        }
        res = coll.aggregate([matches])
        for data in res:
            pprint(data)

    def test_aggr_group_match(self):
        """
        筛选出性别为男，根据年龄段分组的武力值信息组成数组，可以重复的
        @return:
        """
        coll = MongoPool().test.user
        matches = {
            "$match": {
                "sex": 1
            }
        }
        groups = {
            "$group": {
                "_id": "$age",
                "force_values": {"$push": "$force_value"}
            }
        }
        # 这里是先将match的结果作为输入，再进行group操作
        res = coll.aggregate([matches, groups])
        for data in res:
            print(data)

    def test_aggr_project(self):
        """
        投影查找，只返回uid,real_name,force_value
        注意：除了_id之外，其他字段的投影操作只能全部是1或0，不能共存，否则会报错
        @return:
        """
        coll = MongoPool().test.user
        # 筛选条件
        matches = {
            "$match": {
                "age": {"$gt": 30}
            }
        }
        # 需要查询的字段
        project = {
            "$project": {
                "_id": 0,
                "uid": 1,
                "real_name": 1,
                "force_value": 1
            }
        }
        res = coll.aggregate([matches, project])
        for data in res:
            print(data)

    def test_aggr_sort(self):
        """
        排序查找，按武力值降序排序
        @return:
        """
        coll = MongoPool().test.user
        matches = {
            "$match": {"sex": 1}
        }
        groups = {
            "$group": {
                "_id": "$age",
                "force_values": {"$sum": "$force_value"},
                "force_value_avg": {"$avg": "$force_value"}
            }
        }
        # 根据前面的 match,group 的结果，再进行排序操作
        sort = {
            "$sort": {"force_value_avg": -1}
        }
        res = coll.aggregate([matches, groups, sort])
        for data in res:
            print(data)


if __name__ == '__main__':
    unittest.main()
