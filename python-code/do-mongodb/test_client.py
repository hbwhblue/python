# --*-- conding:utf-8 --*--
# todo：单元测试高级查询，过滤
#
# @Time : 2024/3/10 14:27
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import unittest

from pymongo import MongoClient

from db.mongo_pool import MongoPool


class TestClientCase(unittest.TestCase):
    def test_client1(self):
        """
        简单连接1
        @return:
        """
        # 创建连接
        client = MongoClient(host="127.0.0.1", port=27017, username="admin", password="YrnKzEubSv6")

        # 选择一个库下面的集合，如果集合不存在，则自动新建
        coll = client['test']['user']
        print(coll.find_one({'uid': 1}))
        pass

    def test_client2(self):
        """
        简单连接方式2：使用连接字符串
        @return:
        """
        # 创建连接
        uri = f"mongodb://admin:YrnKzEubSv6@127.0.0.1:27017/?maxPoolSize=20"
        client = MongoClient(uri, connectTimeoutMS=5000)

        # 选择一个库下面的集合，如果集合不存在，则自动新建
        coll = client['test']['user']
        print(coll.find_one({'uid': 1}))
        pass

    def test_client3(self):
        """
        简单连接方式3：使用单例
        @return:
        """
        data = MongoPool().test.user.find_one({'uid': 1})
        print(data)


if __name__ == '__main__':
    unittest.main()
