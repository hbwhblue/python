# --*-- conding:utf-8 --*--
# todo：测试mongodb单例模式
#
# @Time : 2024/3/9 21:16
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading
import time

from db.mongo_pool import MongoPool


def task(arg):
    mongo_obj = MongoPool()
    # 连接池对象是否是同一个地址
    str = f"执行编号：{arg}, 连接对象的内存地址: {id(mongo_obj)}"
    # 等待2秒
    time.sleep(2)
    print(str)


# 使用多线程进行测试
if __name__ == '__main__':
    thread_list = []
    for i in range(10):
        t = threading.Thread(target=task, args=(i,))
        thread_list.append(t)
        t.start()

    for t in thread_list:
        t.join()
