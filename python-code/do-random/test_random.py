# --*-- conding:utf-8 --*--
# @Time : 2023/12/25 11:53
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 随机种子测试
# todo 随机数种子规则：
#      > 设置了初始化随机种子,可输出相同随机数列
#      > 不设置时，默认以系统时间为种子数

import random
import unittest


class TestRandom(unittest.TestCase):
    def test_seed(self):
        """随机种子测试"""

        print("没有设定种子时：")
        for i in range(5):
            # 随机生成1-10的整数1<=ret<=10
            ret = random.randint(1, 10)
            print(ret, end=" ")

        print()

        print("设定种子时，随机数生成不变：")
        random.seed(1)
        for i in range(5):
            ret = random.randint(1, 10)
            print(ret, end=" ")

    def test_random(self):
        """
        用于生成0.0-1.0的随机浮点数
        """
        float_data = random.random()
        print(float_data)

    def test_uniform(self):
        """
        生成一个[start,stop]之间的随机小数；a,b取整数 或 浮点数
        """
        float_data = random.uniform(10, 20)
        print(float_data)

    def test_randint(self):
        """
        todo 生成[start,stop]之间的随机整数,即start<=int_data<=stop
        """
        int_data = random.randint(0, 1)
        print(int_data)

    def test_randrange(self):
        """
        生成一个[start,stop]之间以step为步数的随机整数；start,stop,step取整数，step不设时，默认值为1
        """
        int_data = random.randrange(1, 100)
        print("整数：", int_data)

        # 随机生成奇数
        odd_num = random.randrange(1, 100, 2)
        print("奇数：", odd_num)

        # 偶数
        even_num = random.randrange(2, 100, 2)
        print("偶数：", even_num)

    def test_choice(self):
        """
        从序列类型seq中随机返回一个元素；seq取序列类型：如字符串，列表，元祖
        """
        lst = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
        i = 0
        while i < len(lst):
            item = random.choice(lst)
            print(item)
            i += 1

    def test_shuffle(self):
        """
        将序列类型中元素随机排序，返回打乱后序列，seq被改变(改变原列表)，shuffle为洗牌之意；seq取序列类型：如字符串,列表，元祖
        """
        lst = ['one', 'two', 'three', 'four', 'five']
        random.shuffle(lst)
        print(lst)

    def test_sample(self):
        """
        random.sample(seq,N)
        从序列中选取了N个元素，以列表类型返回（不改变原列表）。
        seq取序列类型，N取整数，代表选取个数
        """
        lst = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
        newList = random.sample(lst, 4)
        print(newList)


pass
