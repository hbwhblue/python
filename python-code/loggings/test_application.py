# --*-- conding:utf-8 --*--
# @Time : 2024/1/1 12:04
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: logging的高级应用,logging模块采用了模块化设计，主要包含四种组件
# todo Loggers: 记录器，提供应用程序代码能直接使用的接口
# todo Handlers：处理器，将记录器产生的日志发送到目的地
# todo Filters：过滤器，提供更好的粒度控制，决定哪些日志会被输出
# todo Formatters：格式化器，设置日志内容的组成结构和消息字段
# <p>
# todo logging模块的工作流程
#                                  | 创建屏幕StreamHandler-->设置日志等级->|
# todo 创建一个logger并设置默认等级--->|                                  ->|--->创建Formatter-->用Formatter渲染所有的Handler-->用Formatter渲染所有Handler-->将所有Handler加入Logger-->程序调用Logger
#                                  | 创建文件FileHandler-->设置日志等级  ->|
# <p>
# todo Formatters格式
#   属性          格式          说明
#   asctime     %(asctime)s    日志产生时间，默认格式为：2024-01-01 12:54:38,123
#   created     %(created)f    time.time()生成的日志创建时间戳
#   filename    %(filename)s   生成日志的程序名
#   funcName    %(funcName)s   调用日志的函数名
#   levelname   %(levelname)s  日志级别(DEBUG,INFO,WARNNING,ERROR,CRITICAL)
#   levelno     %(levelno)s    日志级别对应的数值
#   lineno      %(lineno)d     日志所针对的代码行号(报错代码所在行数)
#   module      %(module)s     生成日志的模块名
#   msecs       %(msecs)d      日志生成时间的毫秒部分
#   message     %(message)s    具体的日志信息
#   name        %(name)s       日志调用者
#   pathname    %(pathname)s   生成日志的完整路径
#   processName %(processName)s 进程名(如果可用)
#   process     %(process)d     生成日志的进程id
#   threadName  %(threadName)s  线程名(如果可用)
#   thread      %(thread)d      生成日志的线程id
import logging
import logging.config
import unittest


class TestApplication(unittest.TestCase):
    def test_logger(self):
        """
        测试创建日志记录器
        """
        logger = logging.getLogger(__name__)    # 当前模块下
        print('日志对象：', logger)  # 默认是warning级别
        print('日志对象的类型：', type(logger))
        logger.setLevel(logging.DEBUG)  # 设置日志级别

        # 以文件方式记录
        fh = logging.FileHandler("test1.log", mode="a", encoding="utf-8", delay=False)

        # 格式化日志信息
        formatter = logging.Formatter("%(asctime)s %(name)s：%(levelname)s：%(message)s|%(filename)s：%(lineno)s")
        # 关联日志格式化器
        fh.setFormatter(formatter)

        # 定义过滤器,过滤以test名称为test的文件
        filter = logging.Filter("test")
        # 关联过滤器
        fh.addFilter(filter)

        # 设置日志的处理器
        logger.addHandler(fh)

        logger.debug("这里记录debug信息！")

    def test_logconf(self):
        # 使用配置文件的方式来处理日志
        logging.config.fileConfig('logging.conf')

        rootLogger = logging.getLogger()
        rootLogger.debug("This is root Logger, debug")

        # 记录器
        logger = logging.getLogger("applog")  # 修改的显示名称
        # 打印日志的代码
        logger.debug('This is debug log，bug')  # 调试级别

        # 求商的的异常信息
        try:
            print(5/0)
        except Exception as e:
            logger.exception(e)