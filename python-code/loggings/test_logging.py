# --*-- conding:utf-8 --*--
# @Time : 2023/12/30 05:08
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: logging模块的测试
# todo 日志级别
#      DEBUG    10(级别最低)，常用于开发环境,常用于调试
#      INFO     20(重要)     程序正常运行过程中产生一些信息
#      WARNING  30(警告)     警告用户，虽然程序还在正常工作，但有可能发生
#      ERROR    40(错误)     更严重的问题了，程序已经不能执行一些功能
#      CRITICAL 50(严重)     严重错误，程序已经不能继续运行了
#   默认情况的日志级别是warning，日志级别从上到下依次升高，即DEBUG < INFO < WARNING < ERROR < CRITICAL。而日志的信息量是依次减少的
# <p>
# todo 不同开发环境，不同的日志级别
#      > 开发或测试环境：应该使用debug或info级别，获取详细的日志信息来进行开发或部署调试
#      > 生产环境：     应该使用warning或critical级别，来降低机器的IO压力，和提高获取日志信息的效率
# <p>
# todo 知识点
#      > 当为某个应用程序指定一个日志级别后，应用程序会记录所有日志级别大于或等于指定日志级别的日志信息，而不是仅仅记录指定级别的日志信息
#      > 只要级别大于或等于指定日志级别的记录才会被输出,小于该级别的日志记录将会被丢弃
# <p>
# todo 概念理解
#      > Logger：日志,暴露给应用程序,基于日志记录器和过滤器级别决定使用哪些日志有效
#      > LogRecord：日志记录器，将日志传到相应的处理器进行处理
#      > Handler：处理器,将(日志记录产生的)日志记录发送到合适的目的地
#      > Filter：过滤器，提供了更好的粒度控制，它可以决定输出哪些内容
#      > Formatter：格式化器,指明了最终输出中日志记录布局
import logging
import unittest


class TestLogging(unittest.TestCase):
    def test_debug(self):
        """
        日志的级别信息测试1
        """
        # 使用basicConfig()来指定debug模式输出级别
        logging.basicConfig(level=logging.DEBUG)

        logging.debug("这是debug级别！")
        logging.info("这是info级别！")
        logging.warning("这是warning级别！")
        logging.error("这是error级别！")
        logging.critical("这是critical级别")

    def test_to_logfile(self):
        # 使用baseconfig()来指定debug模式输出，并记录到test.log日志文件，以覆盖方式写入
        logging.basicConfig(filename="test.log", level=logging.DEBUG, filemode="w+")

        logging.debug('This is debug log')  # 调试级别
        logging.info('This is info log')  # 信息级别
        logging.warning('This is warning log')  # 警告级别
        logging.error('This is error log')  # 错误级别
        logging.critical('This is critical log')  # 严重错误级别

    def test_format_log(self):
        logging.basicConfig(level=logging.DEBUG)

        name = "张三"
        age = "39"
        logging.debug("姓名：%s，年龄：%s", name, age)
        logging.debug("姓名:%s，年纪:%s" % (name, age))
        logging.debug("姓名：{}，年龄：{}".format(name, age))
        logging.debug(f"姓名：{name}，年龄：{age}")

    def test_showinfo(self):
        logging.basicConfig(filename="test1.log", filemode="a+",
                            format="%(asctime)s %(name)s:%(levelname)s:%(message)s|%(filename)s:%(lineno)s",
                            datefmt="%Y-%m-%d %H:%M:%S",
                            level=logging.DEBUG
                            )
        logging.debug("这是一个debug模式！")
        logging.info('This is info log')  # 信息级别
        logging.warning('This is warning log')  # 警告级别
        logging.error('This is error log')  # 错误级别
        logging.critical('This is critical log')  # 严重错误级别

    pass