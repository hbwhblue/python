# --*-- conding:utf-8 --*--
# @Time : 2023/12/25 16:47
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 使用有参数的装饰器
import functools


def haveArgs(arg):
    def tmp(func):
        @functools.wraps(func)
        def execFunc(*args, **kwargs):
            res = func(*args, **kwargs)
            newRes = "arg:{} | {}".format(arg,res)
            return newRes

        return execFunc

    return tmp

# 使用函数装饰器
@haveArgs("hello world!")
def test():
    return "test func run ok!"
    pass

if __name__ == "__main__":
    res = test()
    print("res:",res)