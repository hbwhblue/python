# --*-- conding:utf-8 --*--
# @Time : 2023/12/25 11:53
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description:
# todo 在Python中，装饰器是一种高级语言特性，它可以用于修改现有函数或类的行为,在不改动这个函数的前提下，扩展这个函数的功能。比如计算函数耗时、给函数加缓存等。
# todo @classmethod
#      python语言不像其他语言，可以在类中通过关键词声明静态方法,而是通过装饰器`@classmethod`来实现
# todo @staticmethod
#      staticmethod也代表的静态方法，与@classmethod区别是,他和类没有绑定,不能直接访问类的属性
class Base(object):
    pass