# --*-- conding:utf-8 --*--
# @Time : 2023/12/25 17:02
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 缓存类
import functools


class Cache(object):
    @classmethod
    def setCache(cls, cacheKey):
        def tmpFunc(func):
            @functools.wraps(func)
            def do(*args, **kwargs):
                res = func(*args, **kwargs)
                print("设置缓存>>> key：{},value：{}".format(cacheKey, res))
                return res

            return do

        return tmpFunc


@Cache.setCache(cacheKey="add_key_compute")
def compute(a: int, b: int) -> int:
    return a + b
    pass


if __name__ == "__main__":
    r = compute(1, 9)
    print("r=:", r)
