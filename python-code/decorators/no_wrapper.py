# --*-- conding:utf-8 --*--
# @Time : 2023/12/25 13:14
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 不使用@wrapper的示例

def wrapperDemo(func):
    """
    函数式编程，调用func函数
    :param func:
    :return:
    """

    def execFunc(*args, **kwargs):
        res = func(*args, **kwargs)
        return res

    return execFunc


@wrapperDemo
def test():
    print("test func run ok！")
    pass


if __name__ == "__main__":
    # todo 通过上面示例可以看出，虽然我们打印的是test.__name__，希望输出的是test,结果却是:execFunc
    print("执行函数名称：", test.__name__)
