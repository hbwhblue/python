# --*-- conding:utf-8 --*--
# @Time : 2023/12/25 15:08
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 使用@wrapper的示例
import functools


# 修改修饰器
def doWrapper(func):
    # 这里加上wraps
    @functools.wraps(func)
    def execFunc(*args, **kwargs):
        res = func(*args, **kwargs)
        return res

    return execFunc


@doWrapper
def test():
    print("测试")
    pass

if __name__ == "__main__":
    print("执行函数名称：", test.__name__)

