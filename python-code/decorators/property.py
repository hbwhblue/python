# -*- encoding: utf-8 -*-

# author: hjc_042043@163.com
# datetime:2023/4/5 08:37
# software: PyCharm

"""
文件说明：property装饰器，类似javabean
@property广泛应用在类的定义中，可以让调用者写出简短的代码，同时保证对参数进行必要的检查，这样，程序运行时就减少了出错的可能性。
"""


class Person(object):
    def __init__(self):
        pass

    # 访问器 - getter方法
    @property
    def name(self):
        return self._name

    # 修改器 -setter方法
    @name.setter
    def name(self, name):
        self._name = name

    # 访问器 - getter方法
    @property
    def age(self):
        return self._age

    # 修改器 - setter方法
    @age.setter
    def age(self, age):
        self._age = age

    # 访问器 - getter方法
    @property
    def sex(self):
        return self._sex

    # 修改器 - setter方法
    @sex.setter
    def sex(self, sex):
        self._sex = sex

    # 访问器 - getter方法
    @property
    def email(self):
        return self._email

    # 访问器 - setter方法
    @email.setter
    def email(self,email):
        self._email = email

    # 玩耍
    def play(self):
        if self._age <= 16:
            print(f"{self._name}性别{self._age},正在玩飞行棋！")
        else:
            print(f"{self._name}性别{self._age},正在打地主！")


# 编写主函数
def main():
    person = Person()

    person.name = "hjc" # setter方法
    print(person.name) # getter方法

    person.sex = 1      # setter方法
    print(person.sex)   # getter方法

    person.age = 18     # setter方法
    print(person.age)   # getter方法

    person.email = "hjc_042043@sina.cn" # setter方法
    print(person.email) # getter方法

    # person.play()


# 调用主函数
if __name__ == "__main__":
    main()
