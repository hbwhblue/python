# --*-- conding:utf-8 --*--
# @Time : 2023/12/25 12:50
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 使用静态方法举例2，不能调用类的静态属性，它不直接和类绑定
class Book2(object):
    @staticmethod
    def echoPrice():
        """
        类的静态方法，参数不用带cls参数
        :return:
        """
        print("书的价格：59.9元")
    pass

if __name__ == "__main__":
    Book2.echoPrice()