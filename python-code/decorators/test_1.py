import unittest


class TestDecorator1(unittest.TestCase):
    def test_fn1(self):
        fn1()

    def test_fn2(self):
        fn2()

    def test_fn3(self):
        fn3()

    pass


def w1(func):
    """
    装饰器函数
    """

    def inner():
        func()
        print("这是新增的功能！")

    return inner


def w2(func):
    def inner():
        print("----2----")
        return "<b>" + func() + "</b>"

    return inner


@w1  # 等同的执行结果：fn1 = w1(fn1)
def fn1():
    """业务函数1"""
    print("----fn1----")


@w1  # 等同于 fn2 = w1(fn2)
def fn2():
    """业务函数2"""
    print("-----fn2----")


@w1
@w2
def fn3():
    """业务函数3"""
    print("----fn3----")
    return "python"
