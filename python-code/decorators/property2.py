# --*-- conding:utf-8 --*--
# @Time : 2023/12/22 02:22
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 举例2，类似于javabean

class Student(object):
    def __init__(self):

        self._birthday = ""
        self._name = "" # 初始化私有属性name
        self._score = 0 # 初始化私有属性score

    @property
    def score(self):
        """
        getter
        :return:
        """
        return self._score

    @score.setter
    def score(self,value):
        """
        setter
        :param value:
        :return:
        """
        if not isinstance(value,int):
            raise ValueError("score必须是一个整数")    # 抛异常
        if value < 0 or value > 100 :
            raise ValueError("score必须在0-100之间")   # 抛异常

        self._score = value

    @property
    def name(self):
        """
        getter
        :return: 
        """
        return self._name
    
    @name.setter
    def name(self,value):
        if len(value < 3 or value > 20):
            raise ValueError("名字长度在3-10")

        self._name = value


    @property
    def birthday(self):
        return self._birthday

    @birthday.setter
    def birthday(self,value):
        if value == "" :
            raise ValueError("生日不能为空")

        self._birthday = value


if __name__ == "__main__":
    stu = Student()

    try:
        stu.birthday = "1985-04-30" # setter
        print(stu.birthday) # getter

        stu.score = 129 # setter
        print(stu.score) # getter

        stu.name = "黄锦潮" # setter
        print(stu.name) # getter
    except Exception as e:  # 位置的异常
        print(e)
        pass