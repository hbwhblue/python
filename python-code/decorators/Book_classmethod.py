# --*-- conding:utf-8 --*--
# @Time : 2023/12/25 12:11
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 使用静态方法举例1，可以调用类的静态属性

class Book(object):
    bookName: str = "西游记"   # 类属性，也可以叫静态属性

    # 使用装饰器来声明静态方法
    @classmethod
    def echoBookName(cls):
        """
        类的参数需要带上cls
        :return:
        """
        print("书名：{} ".format(cls.bookName))
        pass

    @staticmethod
    def echoBookName2(cls):
        """
        使用staticmethod来调用
        :return:
        """
        print("书名：{}".format(cls.bookName))

if __name__ == '__main__':
    Book.echoBookName()
    print(Book.bookName)

    Book.echoBookName2()