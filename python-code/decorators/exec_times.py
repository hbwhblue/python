# --*-- conding:utf-8 --*--
# @Time : 2023/12/25 15:52
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 自定义函数修饰器，无参数
import functools
import time


# 这是无参的修饰器
def useTime(func):
    @functools.wraps(func)
    def execFunc(*args, **kwargs):
        # 定义开始时间
        beginTime = time.time()
        print("函数执行前：", beginTime)
        res = func(*args, **kwargs)
        endTime = time.time()

        print("函数执行时间：%s" % int(endTime - beginTime))
        return res

    return execFunc
    pass


# 使用函数装饰器
@useTime
def test():
    time.sleep(3)
    print("test func run ok！")
    pass


# 运行函数装饰器
if __name__ == "__main__":
    test()
