# --*-- conding:utf-8 --*--
# @Time : 2023/12/26 11:41
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 测试文件1
import time
import unittest

from common import get_sources_dir


class TestFile(unittest.TestCase):
    def test_read(self):
        """
        以默认只读方式打开文件--->读取内容--->关闭文件
        1.read方法默认会把文件的 所有的内容一次性读取到内存
        2.如果文件太大，对内存的占用会非常严重
        3.这个异常来处理文件的操作，不是很好的方式，因为不管异常是否处理,文件都要关闭操作,而 file.close()又容易忘记，
        所以使用with...as上下文语句来处理更佳，它会自动帮我们关闭文件
        :return:
        """
        global file
        try:
            file = open(get_sources_dir() + "2.txt")
            text = file.read()  # 读取文件内容
            print(text)
        except Exception as e:
            print(e)
        finally:
            file.close()
        pass

    def test_read_line(self):
        """
        读取行的数据,可以一次读取一行内容
        方法执行后,会把文件指针移动到下一行,准备再次读取
        :return:
        """
        try:
            file = open(get_sources_dir() + "1.txt")
            while True:
                # 读取一行的内容
                text = file.readline()

                # 如果没读取到内容就退出
                if not text:
                    break

                # 每读取一行的末尾已经有一个`\n`
                print(text, end='')
            file.close()
        except Exception as e:
            print(e)

    def test_write_add(self):
        """
        以追加的方式打开文件，如果文件存在指针指向文件末尾，文件内以追加的方式写入
        如果文件不存在，就创建
        :return:
        """
        try:
            file = open(get_sources_dir() + "1.txt", "a")
            file.write("hjc\n")
            file.close()
        except Exception as e:
            print(e)
        pass

    def test_write(self):
        """
        以覆盖的方式写入,如果文件不存在，就创建新文件
        :return:
        """
        try:
            file = open(get_sources_dir() + "2.txt", "w")
            res = file.write("hello,python\n")
            print(res)
            res = file.write("天天都要开心，进步！\n")
            print(res)
            file.close()
        except Exception as e:
            print(e)
            pass
        pass

    def test_copy_bigfile(self):
        try:
            file_read = open(get_sources_dir() + "read_big.txt")
            file_write = open(get_sources_dir() + "write_big.txt", "w")

            # 读取文件，并写入到另一个文件
            while True:
                # 每次读取一行
                txt = file_read.readline()

                # 判断是否读取到内容
                while not txt:
                    break

                file_write.write(txt)

            # 关闭文件
            file_read.close()
            file_write.close()
        except Exception as e:
            print(e)

    def test_read_bigFile(self):
        start_time = time.time()  # 记录开始执行时间

        fdata = read_big_file(get_sources_dir() + "2.txt")
        for fdatum in fdata:
            print(fdatum)

        end_time = time.time()  # 结束执行时间

        print("执行时间：", end_time - start_time)
        pass


pass


def read_big_file(file_path):
    block_size = 1024
    with open(file_path, "r", encoding="utf-8") as file:
        while True:
            block = file.read(block_size)
            if block:
                yield block
            else:
                return
