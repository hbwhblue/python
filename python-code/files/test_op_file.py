# --*-- conding:utf-8 --*--
# @Time : 2023/12/26 17:08
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description:
import os
import unittest

from common import get_sources_dir


class TestOpFile(unittest.TestCase):
    def test_basename(self):
        """
        获取基础文件名
        """
        base_name = os.path.basename(get_sources_dir() + "1.txt")
        print(base_name)

    def test_dirname(self):
        """
        获得文件的目录部分
        """
        dir_name = os.path.dirname(get_sources_dir() + "2.txt")
        print(dir_name)

    def test_abc_path(self):
        """
        文件的绝对路径
        """
        abs_path = os.path.abspath(get_sources_dir() + "1.txt")
        print(abs_path)

    def test_rename(self):
        """
        测试重命名文件
        :return:
        """
        print(get_sources_dir())
        res = os.rename(get_sources_dir() + "new-2.txt", get_sources_dir() + "new2.txt")
        print(res)
        pass

    def test_remove(self):
        """
        删除文件
        :return:
        """
        res = os.remove(get_sources_dir() + "new-2.txt")
        print(res)

    def test_listdir(self):
        """
        当前目录列表
        :return:
        """
        # 获取当前工作目录
        current_dir = os.getcwd()

        # 获取根目录
        root_dir = os.path.dirname(current_dir)
        listdir = os.listdir(root_dir)
        print(listdir)

    def test_getcmd(self):
        curr_dir = os.getcwd()  # 获取当前目录
        print(curr_dir)

    def test_mkdir(self):
        """
        创建目录
        :return:
        """
        res = os.mkdir(get_sources_dir() + "test1")
        print(res)

    def test_mkdirs(self):
        """
        可以生成多层递归目录，如果目录全部存在,则创建目录失败
        """
        os.makedirs(get_sources_dir() + "abc/edf/1")
        lst = os.listdir(get_sources_dir() + "abc")
        print(lst)

    def test_rmdirs(self):
        """
        递归删除多层的空目录,如果目录中有文件则无法删除
        """
        os.removedirs(get_sources_dir() + 'abc/edf/1')
        pass

    def test_rmdir(self):
        """
        如果目录存在就删除目录
        :return:
        """
        rm_dir = get_sources_dir() + "test1"
        try:
            if os.path.isdir(rm_dir):
                # 如果是合法目录
                res = os.rmdir(rm_dir)
                print("res=", res)
            else:
                print("目录不存在！")
        except OSError as e:
            print(e)
            pass

    def test_rmfile(self):
        """
        如果文件存在就删除文件
        :return:
        """
        rm_file = get_sources_dir() + "write_big.txt"
        try:
            if os.path.isfile(rm_file):
                res = os.remove(rm_file)
                print("res=", res)
            else:
                print("文件不存在！")
        except OSError as e:
            print(e)
            pass

    def test_os_name(self):
        """
        操作系统的名字
        """
        os_name = os.name
        print(os_name)

    def test_os_env(self):
        """
        系统的环境变量
        """
        os_env = os.environ
        print(os_env)

    def test_os_sep(self):
        """
        系统路径分隔符
        """
        print("系统路径分隔符：", os.sep)

    def test_os_linesep(self):
        """
        文件换行分隔符
        """
        print("文件换行分隔符：", os.linesep)

    pass
