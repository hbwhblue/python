# --*-- conding:utf-8 --*--
# @Time : 2023/12/26 17:19
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 公共方法

from tools import Tools


def get_sources_dir() -> str:
    return Tools.get_root_dir() + "/sources/"
    pass
