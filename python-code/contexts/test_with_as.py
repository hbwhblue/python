# --*-- conding:utf-8 --*--
# @Time : 2023/12/26 18:09
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 在Python中，网络连接、数据库连接、文件句柄、锁等资源操作处理时，都需要在程序执行完毕进行清理工作；
#               很多时候我们经常忘记手动关闭，因此Python集成了一种自动操作,例如文件使用之后，自动释放资源
#
# todo with...as说明：
#   1. 上述场景的描述,转换成python语法就是with...as语句，就是上下文管理器，他在python中实现自动分配并释放资源(内存对象或网络连接)
#   2. 它可以看做是对try...finally语句的简化
# todo 语法格式如下:
#   with 表达式 as 变量名：
#       with-block1
#       with-block2
#   表达式：返回一个支持上下文管理协议的对象，返回的对象可以在with-block开始前执行初始化程序，在with-local完成后执行终止化程序代码，
#          在with-block完成后执行终止化程序,不论with-block中是否发生异常
#   变量：  一般来说被赋值的是"表达式"返回的对象
# todo with语句的作用：
#   1. with语句可以自动管理上下文资源，不论什么原因跳出with块，资源都能自动关闭,以此来达到释放资源的目的
#   2. 简单来说不用手动来写close(),自动释放资源
import random
import re
import string
import unittest

from common import get_sources_dir


class TestWithAs(unittest.TestCase):
    def test_read(self):
        """
        读取文件的内容
        :return:
        """
        with open(get_sources_dir() + "1.txt", "r", encoding="utf-8") as f:
            content = f.read()
            print(content)
        pass

    def test_read2(self):
        with open(get_sources_dir() + "github-profile.html", "rb") as f:
            # 读取文件内容,将字节码文件转为字符串，默认是utf-8编码
            content = f.read().decode("utf-8")
            # 匹配<title>hjc1985 (allen.huang) · GitHub</title>中是否有GitHub
            res = re.findall(r'<title>(.+?)(GitHub)?</title>', content)
            try:
                end_str = res[0][1]
            except IndexError:
                end_str = ""
            print(end_str)

    def test_write(self):
        """
        往2.txt中写入内容
        :return:
        """
        with open(get_sources_dir() + "2.txt", "a", encoding="utf-8") as f:
            i = 0
            while (i < 10000000):
                f.write(generate_code(100) + "\n")
                i += 1
        pass

    def test_readline(self):
        """
        统计行数
        :return:
        """
        with open(get_sources_dir() + "2.txt", "r", encoding="utf-8") as f:
            cnt = sum([1 for _ in f])
            print(cnt)


def generate_code(code_len=20):
    """
    生成指定长度的验证码
    :param code_len:验证码长度,默认是4
    :return:有大小写字母，数字构成的随机验证码字符串
    """
    res = "".join(random.choices(string.digits + string.ascii_letters, k=code_len))
    return res
