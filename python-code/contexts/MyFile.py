# --*-- conding:utf-8 --*--
# @Time : 2023/12/27 17:54
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 文件管理上下文处理类
class MyFile(object):
    def __init__(self, file_name, mod):
        """
        文件操作的构造器
        :param file_name:
        :param mod:
        """
        self.file_name = file_name
        self.mod = mod
        self.file = None

    def __enter__(self):
        """
        创建文件资源对象，自动执行在with语句块
        返回的结果用来赋值给as后面的变量
        :return:
        """
        self.file = open(self.file_name, self.mod, encoding="utf-8")
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()
