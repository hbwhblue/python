# --*-- conding:utf-8 --*--
# @Time : 2023/12/26 19:53
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 测试自定义上下文管理器
#
# todo 定义原则：是 with所求值的对象必须要有一个__enter__()方法，一个__exit__()方法
# todo 执行步骤：
#   1. 紧跟with后面的语句被求值后，返回对象的__enter__()方法被调用，这个方法的返回值将被赋值给"as"后面的变量
#   2. 当with后面的代码代码块语句执行结束后，将调用前面对象的__exit__()方法
# <p>
# todo 定义一个类具体说明with是如何工作：
#   1.实现了特殊方法__enter__()，__exit()__称为该类对象遵守了上下文管理器协议
#   2.该类对象的实例对象,称为上下文管理器
import unittest

from MyFile import MyFile
from common import get_sources_dir


class TestWithAS2(unittest.TestCase):
    def test_custom1(self):
        """
        测试上下文管理
        :return:
        """
        with MyContentMgr() as obj:
            obj.show()

    def test_custom2(self):
        """
        测试上下文管理器2，自动处理异常
        :return:
        """
        with MyContentMgr2() as obj:
            obj.show()

    def test_custom3(self):
        """
        测试上下文管理器3，用来管理文件资源的调用
        :return:
        """
        with CustomWithAsFile() as f:
            print("自定义文件处理上下文")
            f.write("文件上传，上下文测试！")
        print("厉害")

    def test_custom4(self):
        """
        自定义文件类的处理,with语句还能同时打开多个文件
        :return:
        """
        with (MyFile(get_sources_dir() + "read_big.txt", "r") as f,
              MyFile(get_sources_dir() + "2.txt", "a") as f2):  # 将文件对象读取数据
            content = f.read()
            print(content)
            res = f2.write("上下文管理测试！\n")
            print("res=", res)

    def test_mysql(self):
        pass


class MyContentMgr(object):
    """
    自定义上下文管理器1
    """

    def __enter__(self):
        """
        todo 在with语句块中执行，返回的对象赋值给as语句中的变量
        :return:
        """
        print("enter方法被调用执行了！")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        todo 等with语句块执行完，退出上下文，释放资源
        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return:
        """
        print("exit方法被调用执行了！")

    def show(self):
        print("show方法被调用执行！")


class MyContentMgr2(object):
    """
    自定义上下文管理器2
    """

    def __enter__(self):
        """
        在with语句块中执行，返回的对象赋值给as语句中的变量
        :return:
        """
        print("enter方法被调用执行了！")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        等with语句块执行完，释放资源
        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return:
        """
        print("exit方法被调用执行了！")

    def show(self):
        print("show方法被调用执行", 5 / 0)


class CustomWithAsFile(object):
    """
    自定义上下文管理器3
    用来管理文件资源的调用
    """

    def __init__(self):
        self.f = None

    def __enter__(self):
        """
        在with语句块中执行，返回的对象赋值给as语句中的变量
        :return:
        """
        print("with语句开始")
        self.f = open(get_sources_dir() + "read_big.txt", mode="a", encoding="utf-8")
        return self.f

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        等with语句块执行完，释放资源
        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return:
        """
        print("with语句结束")
        self.f.close()
