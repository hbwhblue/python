# --*-- conding:utf-8 --*--
# @Time : 2023/12/26 17:19
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 公共方法
import os


def get_sources_dir() -> str:
    # 获取当前工作目录
    current_dir = os.getcwd()
    # 获取根目录
    root_dir = os.path.dirname(current_dir)
    return root_dir + "/sources/"
    pass
