# --*-- conding:utf-8 --*--
# @Time : 2023/12/27 16:27
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 使用上下文来处理pymysql
import pymysql


class SqlManager(object):
    def __init__(self):
        """
        构造器
        """
        self.conn = None
        self.cursor = None
        self.connect()

    def connect(self):
        """
        连接数据库
        :return:
        """
        self.conn = pymysql.connect(
            host="127.0.0.1",
            port=3306,
            database="test",
            user="root",
            password="123456",
            charset="utf-8"
        )
        self.cursor = self.conn.cursor(cursor=pymysql.cursors.DictCursor)

    def get_list(self, sql, args=None):
        """
        查询多条记录
        :param sql: sql语句
        :param args: sql语句的参数
        :return:
        """
        self.cursor.execute(sql, args)
        result = self.cursor.fetchall()
        return result

    def get_one(self, sql, args=None):
        """
        查询单条数据
        :param sql: sql语句
        :param args: sql语句的参数
        :return:
        """
        self.cursor.execute(sql, args)
        result = self.cursor.fetchone()
        return result

    def insert(self, sql, args=None):
        """
        插入数据库的语句
        :param sql: sql语句
        :param args: sql语句的参数
        :return:
        """
        self.cursor.execute(sql, args)
        self.cursor.commit()
        last_id = self.cursor.lastrowid
        return last_id

    def modify(self, sql, args=None):
        """
        执行单条的update语句
        :param sql:
        :param args:
        :return:
        """
        self.cursor.execute(sql, args)
        self.conn.commit()

    def multi_modify(self, sql, args=None):
        """
        执行多条SQL语句
        :param sql:
        :param args:
        :return:
        """
        self.cursor.executemany(sql, args)
        self.conn.commit()

    def close(self):
        """
        关闭数据库连接
        :return:
        """
        print("数据库关闭资源")
        self.cursor.close()
        self.conn.close()

    def __enter__(self):
        """
        进入with语句自动执行
        :return:
        """
        print("enter方法被调用执行了，返回数据库资源对象！")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        退出with语句，释放资源
        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return:
        """
        self.close()
