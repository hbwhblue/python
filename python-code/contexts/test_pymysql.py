# --*-- conding:utf-8 --*--
# @Time : 2023/12/27 18:55
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: pymysql测试
import unittest

from SqlManager import SqlManager


class TestPymysql(unittest.TestCase):

    def test_list(self):
        with SqlManager() as db:
            sql = "select * from t_dept limit 10"
            dataList = db.get_list(sql,None)

            if not dataList:
                print("数据为空")
            else:
                for item in dataList:
                    print("deptno=",item['deptno'])
                    print("dname=",item['dname'])
                    print("loc=",item['loc'],"\n")
