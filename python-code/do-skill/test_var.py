# --*-- conding:utf-8 --*--
# todo：变量交换
#
# @Time : 2024/3/16 15:02
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import unittest


class TestVar(unittest.TestCase):
    def setUp(self):
        self.a = 1
        self.b = 2

    def test_swap_var1(self):
        print(swap_var1(self.a, self.b))

    def test_swap_var2(self):
        print(swap_var2(self.a, self.b))

    def test_judge1(self):
        """
        常规判断 成绩是否及格
        """
        num = int(input("请输入一个数字："))
        if num >= 60:
            print("及格")
        else:
            print("不及格")

    def test_judge2(self):
        """
        使用三元运算符判断  成绩是否及格
        @return:
        """
        num = int(input("请输入一个数字："))
        # 这个三元运算符和 JAVA，C不同，它if后的结果是写在最前面的
        res = "及格" if num >= 60 else "不及格"
        print(res)

    def test_judge3(self):
        """
        判断字典中是否有某个键值

        @return:
        """
        dict_data = {
            "user_base": {
                "uid": 1,
                "uname": "allen",
                "email": "hjc_042043@sina.cn"
            },
            "user_info": {
                "age": 18,
                "sex": "男"
            }
        }
        try:
            score = dict_data["user_base"]["score"]
        except KeyError:
            score = 0
        print("学生成绩：", score)

    def test_judge4(self):
        """
        判断列表中是否有某一个索引值
        @return:
        """
        list_data = [78, 82, 93, 84, 65]
        try:
            score = list_data[6]
        except IndexError:
            score = 0
        print("学生成绩：", score)

    def test_judge5(self):
        """
        判断一个值得的范围大小
        @return:
        """
        num = int(input("请输入一个数字："))
        # 常规的写法
        if num >= 0 and num <= 100:
            print("数字在0-100之间")
        else:
            print("数字不在0-100之间")

    def test_judge6(self):
        """
        判断一个值得的范围大小
        @return:
        """
        num = int(input("请输入一个数字："))
        # 使用更简洁的写法
        if 0 <= num <= 100:
            print("数字在0-100之间")
        else:
            print("数字不在0-100之间")


def swap_var1(a, b):
    """
    变量交换方法1,JAVA,PHP使用常规的方法，比较繁琐
    @param a: 变量1
    @param b: 变量2
    @return:
    """
    temp = a
    a = b
    b = temp
    return a, b


def swap_var2(a, b):
    """
    变量交换方法2，比较简单
    @param a:
    @param b:
    @return:
    """
    a, b = b, a
    return a, b


if __name__ == '__main__':
    unittest.main()
