# --*-- conding:utf-8 --*--
# todo：字符串格式化
#
# @Time : 2024/3/16 15:09
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import unittest


class StringTestCase(unittest.TestCase):
    def test_format(self):
        """
        字符串的格式化连接
        @return:
        """
        uid = 123
        uname = 'allen'
        age = 18
        # python 之前就像java一样，拼接字符串使用+号，字符串长了之后也会比较麻烦，并且对于整型还需要做类型转换
        string = 'uid=' + str(uid) + '&uname=' + uname + '&age=' + str(age)
        print(string)

        # 用格式化字符串的方式来处理就比较方便
        # 格式化方式一(python3.6之前)：
        string = 'uid={}&uname={}&age={}'.format(uid, uname, age)
        print("字符串对象的format方法：", string)

        # 格式化方式f-string(python3.6之后)：
        string = f'uid={uid}&uname={uname}&age={age}'
        print("f-string：", string)
        pass

    def test_split(self):
        """
        字符串分割，默认以空白符分割，除了空格还有其他符号，比如：\t,\n,\r
        也可以指定分隔符，比如：,或|，#等其他字符
        @return:
        """
        uname = "Tom green yellow"
        new_list = uname.split()
        print(new_list)

        num_str = "1,2,3,4,5,6,7,8,9,10"
        new_list = num_str.split(",")
        print(new_list)

        num_str = "1|2|3|4|5|6|7|8|9|10"
        new_list = num_str.split("|")
        print(new_list)
        pass

    def test_join(self):
        """
        字符串连接，默认以空白符连接，除了空格还有其他符号，比如：\t,\n,\r
        @return:
        """

        list = ["Tom", "green", "yellow"]
        new_str = "".join(list)
        print(new_str)

        list = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
        new_str = ",".join(list)
        print(new_str)

        list = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
        new_str = "|".join(list)
        print(new_str)
        pass

    def test_str2bytes(self):
        """
        字符串转为字节码
        @return:
        """
        s = "Hello, World!"
        b = s.encode('utf-8')  # 默认编码为utf-8
        print(b)

    def test_bytes2str(self):
        """
        字节码转为字符串
        @return:
        """
        bytes_data = b"china good!"
        s = bytes_data.decode('utf-8')  # 默认编码为utf-8
        print(s)


if __name__ == '__main__':
    unittest.main()
