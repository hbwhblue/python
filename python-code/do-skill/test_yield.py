# --*-- conding:utf-8 --*--
# todo：生成器技巧
#
# @Time : 2024/3/16 15:42
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import sys
import unittest


class YieldTestCase(unittest.TestCase):
    """
    todo: 计算100以内的平方数
    todo: yield即生成器，相比return的优势是 循环体内，程序在执行到yield时，程序会暂停，并把值出栈；
        这样子就不会占用内存，下次循环进来时再接着上一次的yield的位置继续往下执行，而return的方式是先把数据都存放在栈中，一次性全部返回
    todo：yield可以处理比较耗时，占用内存的操作，比如网络请求，文件读取等，类似异步处理，比如一边下载文件，一边把文件内容显示出来
    @return:
    """

    def test_return_square(self):
        lst = return_square()
        print(lst)
        pass

    def test_yield_square(self):
        gen = yield_square()
        for e in gen:
            print(e)
        pass

    def test_yield_memory(self):
        # 先用列表推导式生成一个1000000的列表，再求和
        list1 = [x for x in range(1000000)]
        sum1 = sum(list1)
        print("推导式求和：", sum1)
        print("内存使用：", sys.getsizeof(list1), "字节")

        # 用生成器生成一个1000000的列表，再求和
        list2 = (x for x in range(1000000))
        sum2 = sum(list2)
        print("生成器求和：", sum2)
        print("内存使用：", sys.getsizeof(list2), "字节")
        pass

    pass


def return_square():
    """
    todo 使用return用于生成100以内的平方数
    @return:
    """
    lst = []
    for i in range(100):
        lst.append(i ** 2)
    return lst


def yield_square():
    """
    todo 使用yield实现一个生成器，用于生成100以内的平方数
    """
    for i in range(100):
        yield i ** 2
