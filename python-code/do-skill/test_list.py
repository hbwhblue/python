# --*-- conding:utf-8 --*--
# todo：测试列表
#
# @Time : 2024/3/16 16:33
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import unittest
from pprint import pprint


class ListTestCase(unittest.TestCase):
    def setUp(self):
        self.fruites = ['apple', 'banana', 'orange', 'grape', 'pear']

    def test1(self):
        """
        使用常规的for循环遍历列表，将列表中的元素转换为大写
        @return:
        """
        new_list = []
        for fruit in self.fruites:
            new_list.append(fruit.upper())
        print(new_list)
        pass

    def test2(self):
        """
        todo：使用列表推导式将列表中的元素转换为大写
        @return:
        """
        # 这个推导式中的含义是，①先看for循环，把元素遍历出来，②再看 fruit.upper(),把循环体中元素转换为大写，③最后把结果赋值给new_list
        new_list = [fruit.upper() for fruit in self.fruites]
        print(new_list)
        pass

    def test3(self):
        """
        筛选出列表中以a开头的元素，并将其转换为大写
        @return:
        """
        new_list = []
        for fruit in self.fruites:
            if fruit.startswith('a'):
                new_list.append(fruit.upper())
        print(new_list)
        pass

    def test4(self):
        new_list = [fruit.upper() for fruit in self.fruites if fruit.startswith('a')]
        print(new_list)
        pass

    def test5(self):
        """
        循环带有索引值列表
        @return:
        """
        for i, value in enumerate(self.fruites):
            print(i, value)

    def test6(self):
        """
        反向遍历1
        @return:
        """
        for i, value in enumerate(reversed(self.fruites)):
            print(i, value)

    def test7(self):
        """
        反向遍历2,使用切片步长为-1的方式来实现
        @return:
        """
        for i, value in enumerate(self.fruites[::-1]):
            print(i, value)

    def test8(self):
        """
        顺序遍历
        @return:
        """
        for i, value in enumerate(sorted(self.fruites)):
            print(i, value)

    def test9(self):
        lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        # 取最后一个元素
        print(lst[-1])

    def test10(self):
        """
        判断list是否为空
        @return:
        """
        lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        # 不好的写法：
        if len(lst) > 0:
            print('列表不为空')

        # 好的写法
        if lst:
            print('列表不为空')
        pass

    def test11(self):
        """
        判断某个值是否包含在序列中，包含列表，字符串，元组,字典等
        @return:
        """
        lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        num = 5

        # 某个值是否包含在列表中，类似于 JAVA 的 a.contains(b)方法
        if num in lst:
            print('列表中包含该元素')
        else:
            print('列表中不包含该元素')

        # 某个字符子串是否包含着一个字符串中
        string = 'hello world'
        if 'hello' in string:
            print('字符串中包含该子串')
        else:
            print('字符串中不包含该子串')

        # 字典的键是否包含在字典中
        dic = {'name': 'allen', 'age': 20}
        if 'name' in dic:
            print('字典中包含该键')
        else:
            print('字典中不包含该键')
        pass

    def test12(self):
        """
        去掉列表中的重复值
        @return:
        """
        lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3]
        my_set = set(lst)
        print(list(my_set))
        pass

    def test13(self):
        list1 = [1, "hjc", "email", 3.14, True]
        list2 = [2, "username"]
        print("将list+list2两个列表合并:", list1 + list2)

        list3 = [1, 2, 3, 4, 5]
        list4 = [6, 7, 8, 9, 10]
        print("将list3+list4两个列表合并:", [*list3, *list4])
        pass

    def test_format_print(self):
        """
        列表，字典格式化输出
        """
        lst = [
            {
                'url': 'https://read.douban.com/reader/ebook/52497819/',
                'title': 'Java高并发编程：多线程与架构设计',
                'author': '王文君',
                'price': '59.00',
                'score': '8.7',
                'publisher': '机械工业出版社'
            },
            {
                'url': 'https://read.douban.com/reader/ebook/153139284/',
                'title': 'Java高并发编程详情',
                'author': '王文君',
                'price': '55.00',
                'score': '8.2',
                'publisher': '机械工业出版社'
            },
            {
                'url': 'https://read.douban.com/reader/ebook/128052544/',
                'title': '深入理解Java虚拟机：JVM高级特性与最佳实践（第3版）',
                'author': '周志明',
                'price': '99',
                'score': '9.4',
                'publisher': '机械工业出版社'
            }
        ]
        # print("没有个格式化：", lst)
        print("格式化输出：")
        pprint(lst)


if __name__ == '__main__':
    unittest.main()
