# --*-- conding:utf-8 --*--
# todo：字典的小技巧
#
# @Time : 2024/3/16 21:17
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import unittest


class TestDictCase(unittest.TestCase):
    def test_reverse(self):
        """
        字典反转

        @return:
        """
        dict1 = {
            'one': 1,
            'tow': 2,
            'three': 3
        }
        new_dict = dict(map(reversed, dict1.items()))
        print(new_dict)

    def test_sort_item(self):
        """
        根据字典列表中的字典元素年龄进行倒序排列
        @return:
        """
        dict_list = [
            {'name': 'allen', 'age': 18},
            {'name': 'john', 'age': 20},
            {'name': 'tom', 'age': 15}
        ]
        new_dict_list = sorted(dict_list, key=lambda x: x['age'], reverse=True)
        print(new_dict_list)
        pass

    def test_lists_to_dict(self):
        """
        将两个列表合并到一个字典中,一个列表的值作为 key,另一个列表的值作为 value
        @return:
        """
        keys = ['a', 'b', 'c']
        values = [1, 2, 3]
        new_dict = zip(keys, values)
        print(dict(new_dict))
        pass


if __name__ == '__main__':
    unittest.main()
