# --*-- conding:utf-8 --*--
# todo：检查端口是否被开放
#
# @Time : 2024/1/31 14:09
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import socket


def check_port_open(host, port):
    """
    @desc: 检查端口是否被开放
    @param host: string 服务器地址
    @param port: string 端口
    @return:
    """
    try:
        conn = socket.create_connection((host, port), timeout=5)
        conn.close()
        return True
    except(socket.timeout, ConnectionRefusedError) as e:
        # 输出日志
        # logging.basicConfig(level=logging.ERROR)
        # logging.error(e)
        print("{}:{},{}".format(host, port, e).format(host, port, e))
        return False


def print_check_res(host, port):
    if check_port_open(host, port):
        print("服务器地址{},端口{}已经被开放".format(host, port))
    else:
        print("服务器地址{},端口{}没有被开放".format(host, port))


if __name__ == '__main__':
    # 验证本地docker的端口是否被开放
    print_check_res('127.0.0.1', 2375)
    # 验证本地redis的端口是否被开放
    print_check_res('127.0.0.1', 6379)
    # 验证本地的docker-redis端口是否被开放
    print_check_res('127.0.0.1', 6330)
    print_check_res('127.0.0.1', 6380)
    # 验证本地的mysql端口是否被开放
    print_check_res('127.0.0.1', 3306)
    # 验证本地的docker-mysql端口是否被开放
    print_check_res('127.0.0.1', 3406)
    # 验证本地的docker-mongodb端口是否被开放
    print_check_res('127.0.0.1', 27017)
    # 验证本地的docker-rabbitmq端口是否被开放
    print_check_res('127.0.0.1', 5672)
    print_check_res('127.0.0.1', 15672)
    # 验证本地的docker-elasticsearch端口是否被开放
    print_check_res('127.0.0.1', 9200)
    print_check_res('127.0.0.1', 9300)
    # 验证本地的docker-kibana端口是否被开放
    print_check_res('127.0.0.1', 5601)
    print_check_res('127.0.0.1', 5600)
    # 验证本地的docker-logstash端口是否被开放
    print_check_res('127.0.0.1', 5044)
    print_check_res('127.0.0.1', 5000)
    # 验证本地的docker-nginx端口是否被开放
    print_check_res('127.0.0.1', 80)

    print()
    print("======================end======================")
    print()

    # 验证本地tp-docker-nginx端口是否被开放
    print_check_res('192.168.0.102', 80)
    # 验证本地tp-docker-docker端口是否被开放
    print_check_res('192.168.0.102', 2375)
    # 验证本地tp-docker-redis端口是否被开放
    print_check_res('192.168.0.102', 6379)
    # 验证本地tp-docker-mysql端口是否被开放
    print_check_res('192.168.0.102', 3306)
    # 验证本地tp-docker-mongodb端口是否被开放
    print_check_res('192.168.0.102', 27017)
    # 验证本地tp-docker-rabbitmq端口是否被开放
    print_check_res('192.168.0.102', 5672)
    print_check_res('192.168.0.102', 15672)
    # 验证本地tp-docker-elasticsearch端口是否被开放
    print_check_res('192.168.0.102', 9200)
    print_check_res('192.168.0.102', 9300)
    # 验证本地tp-docker-kibana端口是否被开放
    print_check_res('192.168.0.102', 5601)
    print_check_res('192.168.0.102', 5600)
    # 验证本地tp-docker-logstash端口是否被开放
    print_check_res('192.168.0.102', 5044)
    print_check_res('192.168.0.102', 5000)
