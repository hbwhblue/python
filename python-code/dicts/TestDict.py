#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : TestDict.py
@Time       : 2023/6/26 18:07
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 字典的使用
"""
import unittest


class TestDict(unittest.TestCase):
    def test_create_dic(self):
        """
        创建字典的各种方式
        :return:
        """
        # 创建空的dict对象
        d = {}
        d1 = dict()

        # 用{}创建有初始值的dict对象
        d1 = {"hjc": 78.5, "wujun": 90.5, "lc": 100}
        d2 = {"hjc": 88.5, 2: [1, 2, 3]}

        # 用dict()函数创建有初始值的dict对象
        d3 = dict({"hjc": 78.5, 1: [1, 2, 3]})
        d4 = dict(hjc=89, lst=[2, 3, 4])  # 类似函数传递参数值

        print("d=", d)
        print("d1=", d1)
        print("d2=", d2)
        print("d3=", d3)
        print("d4=", d4)

        # 通过fromkeys创建dict对象
        keys = (0, 1, 2, 3)
        d5 = dict.fromkeys(keys)
        print("create dict fromkeys：", d5)

        # 通过推导式创建字典
        d5 = {x: x ** 2 for x in range(5)}
        print("推导式d5=", d5)

    def test_get_dict_item(self):
        """
        获取字典的元素
        :return:
        """
        d = dict(hjc=89, wujun=78, lc=92)
        item1 = d["hjc"]
        print("item1=", item1)  # 使用[]来获取，如果key不存在就会报错
        # print("item2=", d[2]) #这个key不存在，就会报错

        # 使用get来获取一个值
        item1 = d.get("hjc")
        item2 = d.get("wujun")
        item3 = d.get(2, 0)  # 不存在，默认值为0.0
        print(f"item1={item1}，itme2={item2}，itme3={item3}")

    def test_update_item(self):
        """
        更新或添加元素
        :return:
        """
        d = {"hjc": 78, "lst": [1, 2, 3]}
        d["hjc"] = ("男", 78)  # 更新一个存在键值
        print(d)

        # 键值不存在，则新增一个键值
        d["wujun"] = ("男", 90)
        print("最终结果：", d)

        # 使用update方法插入或更新多个键值
        d = {"lc": 89}
        d.update({"hjc": 77})
        print("d=", d)

        # 传递一个dict对象
        d.update(dict({"wujun": 80}))
        print("d=", d)

        # 传递关键词参数,类似于函数传参,key不存在就新增
        d.update(wyj=90, hjc=88)
        print("d=", d)

        d.update({"python": 3.11})  # 不存在就新增
        print("d=", d)

        # 更新多个值,没有新增，存在就更新
        d.update({"hjc": 100, "zxy": 87, "liulj": 98, "mxm": 67})
        print("d=", d)

        dic1 = {1: 20, 2: 20}
        dic2 = {3: 30, 4: 40}
        dic3 = {5: 50, 6: 60}
        dic1.update(dic2)
        dic1.update(dic3)
        print("new-dic1:", dic1)

        new_iter = zip(dic1, dic2, dic3)
        print("new-iter:", list(new_iter))

    def test_del_dict(self):
        """
        删除集合的元素
        :return:
        """
        d = {"hjc": 100, 1: [2, 3, 4], "python": 61}
        del d["python"]  # 删除key=python的值，如果key不存在，就会报错
        print("d=", d)
        print("子元素：", d[1][1])

        del_item = d.pop("hjc")
        print("删除的元素：", del_item)
        print("d=", d)

        # 清空所有元素
        d.clear()
        print("d=", d)

    def test_list_dict(self):
        """
        遍历一个集合
        :return:
        """

        d = {"a": 1, "b": 2, "c": 3, "d": 35}
        print("所有的key=", d.keys())
        print("所有值values=", d.values())
        print("所有键值对items=", d.items())

        # 遍历键
        print("遍历每个键：")
        for key in d.keys():
            print(key, end=",")
        print()

        print("遍历每个值：")
        # 遍历值
        for val in d.values():
            print(val, end=",")
        print()

        # 遍历键值对
        print("遍历键值对：")
        for key, val in d.items():
            print(f"key={key},value={val}")

    def test_method_dict(self):
        """
        集合的方法
        todo zip 函数是可以接收多个可迭代对象(list,tuple,str)，然后把每个可迭代对象中的第i个元素组合在一起，形成一个新的迭代器，类型为迭代器
         注意：多个可迭代对象中的元素的个数必须一致，即索引必须一致
        :return:
        """
        d = dict(user_id=1, user_name="hjc", email="hjc_042043@163.com")
        print("获取字典长度：", len(d))
        print("将字典转换成一个字符串", str(d))

        # 将两个可迭代对象创建一个dict,比如有两个元祖，第一个元祖是key,第二个元祖是value,可以重新组合成keyN:valN的集合
        keys = ("a", "b", "c")
        vals = (1, 2, 3)
        new_dict = dict(zip(keys, vals))
        print("new_dict=", new_dict)

    def test_stat_word_nums(self):
        """
        统计一个文字字母出现了多少次
        :return:
        """
        words = input("请输入一段文字：")
        counters = {}

        for ch in words:
            if "a" <= ch <= "z" or "A" <= ch <= "Z":
                counters[ch] = counters.get(ch, 0) + 1

        for ch, cnt in counters.items():
            print(f"字母{ch}出现了{cnt}次")

    def test_dict_derivation(self):
        """
        利用推导式计算一组股票价格大于100元的值
        :return:
        """
        d = {
            "601800": 10.74,
            "300776": 62.98,
            "300576": 44.10,
            "002594": 260.5,
            "300750": 223.47,
            "600519": 1711.05,
        }
        # 还原字典推导式
        # new_dict = {}
        # for key,val in d.items():
        #     if val > 100:
        #       new_dict[key] = val

        new_dict = {key: val for key, val in d.items() if val > 100}
        print("new_dict=", new_dict)

    def test_key_exists(self):
        """
        判断一个key是否存在
        @return:
        """
        res = key_exists("a");
        print(res)

    def test_dict_merge(self):
        """
        合并两个字典
        :return:
        """
        dict1 = {"a": 1, "b": 2, "c": 3}
        dict2 = {"d": 4, "e": 5, "f": 6}

        # 这里两个字典的**表示解包，将两个字典的键值对合并成一个新的字典
        new_dict = {**dict1, **dict2}
        print(new_dict)


def key_exists(key):
    d = dict(a=1, b=2, c=3)
    if key in d:
        return True
    else:
        return False
