#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@NAME      :test_generator.py
@TIME      :2023/12/21 14:20:27
@AUTHOR    :hjc_042043@sina.cn
@VERSION   :1.0
'''
from enum import Enum


class Student(object):
    """
    Student类继承自object类,object类是最基础的类
    1. __init__是构造器
    2. 类方法在创建时，第一个参数永远是self参数,表示创建实例的本身,因此，在__init__方法内部,就可以把各种属性绑定到self.因为self就指向创建的实例本身
        > 定义对象实例属性格式：`self.属性名=初始值(形参)`
    3. 类在实例化时，参数需要和__init__构造器一致，但self不需要传，Python解释器自己会把实例变量传进去
        > 格式：在创建对象时,使用`类名(属性1，属性2....)`调用
    """

    def __init__(self, name, sex, score):
        """
        构造器，
        :param name: 姓名
        :param sex: 性别
        :param score: 成绩
        """
        self.name = name  # 将参数name设置到成员变量name中去
        self.sex = sex  # 将参数sex设置到成员变量sex中去
        self.score = score  # 将参数score设置到成员变量score中去
        pass

    def get_grade(self):
        if self.score >= 90:
            return 'A'
        elif self.score >= 75:
            return 'B'
        else:
            return 'C'

    def run(self, param):
        print("param：", param)
        pass

    def __str__(self):
        """
        打印对象的描述信息，需要使用格式化字符串
        :return:
        """

        return "姓名：%s，性别：%s,成绩：%d" % (self.name, self.sex, self.score)


class Gender(Enum):
    """
    性别枚举
    """
    Male = "男"
    Female = "女"


std = Student("hjc", "女", 93)
print(std)  # 打印对象信息
print(std.name, "成绩等级：", std.get_grade())
if std.sex == Gender.Female.value:
    print("此学生是巾帼英雄！")

std1 = Student("张三", '男', 74)
print(std1, "成绩等级：", std1.get_grade())
