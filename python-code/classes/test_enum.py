# --*-- conding:utf-8 --*--
# @Time : 2023/12/22 03:38
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 测试枚举类
import unittest
from enum import Enum, unique


class EnumTest(unittest.TestCase):
    """
    Enum可以把一组相关常量定义在一个class中，且class不可变，而且成员可以直接比较。
    """

    def test1(self):
        # 声明一个枚举对象
        Month = Enum('Month', ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'))
        print(Month)

        print("循环1：")
        for e in Month:
            print(e)

        print("循环2：")
        for name, item in Month.__members__.items():
            print(name, '=>', item)

    def test2(self):
        mon = EnumWeekday.Mon
        print(mon)

    def test3(self):
        for i, e in enumerate(Gender):
            print(i,"=>",e)
        pass


@unique
class EnumWeekday(Enum):
    """
    声明星期的枚举，并且保持唯一
    """
    Sun = 0
    Mon = 1
    Tue = 2
    Wed = 3
    Thu = 4
    Fri = 5
    Sta = 6


class Gender(Enum):
    Male = 0
    Female = 1
