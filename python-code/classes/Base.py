# --*-- conding:utf-8 --*--
# @Time : 2023/12/23 21:45
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 基类

"""
todo 所有的类都可以继承自一个称为object的基类。在Python 3中，这是默认的行为，即使你没有显式地指定object作为基类，Python也会自动将其作为基类.
 1、__init__: 类的构造函数；
 2、self.name 和 self.age: 指的是成员变量；
 3、say(self): 指的成员方法
"""


class Base(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say(self):
        content = "我是{},今年{}岁".format(self.name, self.age)

    def __repr__(self):
        """
        重写repr()方法，实现自我描述。当一个实例被print()时，Python会调用该方法，
        返回一个字符串，它将被用于显示该实例的字符串表示。
        @return:
        """
        return "Base(name={},age={})".format(self.name, self.age)


if __name__ == "__main__":
    # 实例化类
    person = Base("张三", 19)
    print("自动调用__repr__，", person)

    # 调用类的成员方法
    person.say()

    # 访问类的成员属性
    print("name=", person.name)
    print("age=", person.age)
