# --*-- conding:utf-8 --*--
# todo：使用__new__来实现单例
#  > __new__ 是一个由`object`基类提供的内置的静态方法，主要作用有两个：
#  > 1) 在内存中为 对象分配空间
#  > 2) 返回 对象的引用
# todo：
#  > __new__至少要有一个参数cls，代表要实例化的类，此参数在实例化时由Python解释器自动提供
#  > __new__必须要有返回值，返回实例化出来的实例，这点在自己实现__new__时要特别注意，可以return父类__new__出来的实例，或者直接是object的__new__出来的实例
#  > __init__有一个参数self，就是这个__new__返回的实例，__init__在__new__的基础上可以完成一些其它初始化的动作，__init__不需要返回值
#  > 我们可以将类比作制造商，__new__方法就是前期的原材料购买环节，__init__方法就是在有原材料的基础上，加工，初始化商品环节
#  > 使用 类名() 创建对象时，Python的解释器首先会调用 __new__方法为对象 分配空间
# @Time : 2024/1/7 22:58
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

class User(object):
    __instance = None  # 私有的实例属性

    def __new__(cls, *args, **kwargs):
        if not cls.__instance:  # 如果对象实例不存在
            cls.__instance = super(User, cls).__new__(cls)  # 新建一个实例
        return cls.__instance  # 返回实例对象

    def __init__(self, name, age):
        self.name = name
        self.age = age


if __name__ == '__main__':
    """
    单例模式的测试
    """
    user = User('小明', 18)
    print(user.__new__(User))

    user1 = User('小红',19)
    print(user1.__new__(User))

    if user == user1:
        print("两个值相等")
    else:
        print("两个值不想等")

    if user is user1:
        print("两个对象的内存地址相等！")
    else:
        print("两个对象的内存地址不想等！")