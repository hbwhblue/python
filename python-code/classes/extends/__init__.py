# --*-- conding:utf-8 --*--
# @Time : 2023/12/22 00:25
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description:
# 1.python是可以支持多继承的，
# 2.注:如果同时继承多个父类，并且存在一样的方法名,子类会按照按继承的顺序执行对应的父类方法.

# from .animal import Animal
# from .dog import Dog
# from .cat import Cat
#
# __ALL__ = ["Animal","Dog","Cat"]

