# 继承和多态的测试

import unittest
from dog import Dog
from cat import Cat
from animal import Animal


class TestExtend1(unittest.TestCase):
    def test_extends(self):
        dog = Dog()
        dog.walk()

        cat = Cat()
        cat.jump()
        pass

    def test_type(self):
        dog = Dog()
        cat = Cat()
        animal = Animal()

        print(isinstance(animal, Animal))
        print(isinstance(cat, Animal))
        print(isinstance(dog, Animal))
        print(isinstance(animal, Dog))
        pass

    def test_polymorphisms(self):
        # 狗逃跑
        run(Dog())
        # 猫逃跑
        run(Cat())
        pass


def run(animal):
    """
    动物奔跑
    :param animal: 动物类型，声明的类型是animal，使用的类型是Dog,Cat类型就是多态
    :return:
    """
    animal.run()


if __name__ == '__main__':
    unittest.main()
