# --*-- conding:utf-8 --*--
# @Time : 2023/12/23 23:17
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description:

class People(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say(self):
        print("姓名：{},年龄：{}".format(self.name, self.age))


class Boy(People):
    def say(self):
        print("我叫{},今年{}岁，是个帅气的男孩".format(self.name, self.age))
        pass


class Girl(People):
    pass


if __name__ == "__main__":
    # 实例化男孩
    boy = Boy("王麻子",38)
    girl = Girl("张晓丽",15)

    # 调用方法
    boy.say()
    girl.say()