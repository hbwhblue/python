# --*-- conding:utf-8 --*--
# @Time : 2023/12/22 00:26
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 猫继承动物类

from animal import Animal

class Cat(Animal):
    def __init__(self):
        print("这是猫")

    def run(self):
        print("奔跑！")

    def jump(self):
        print("猫能奔跑")

        pass
