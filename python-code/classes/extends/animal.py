# --*-- conding:utf-8 --*--
# @Time : 2023/12/22 00:15
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 继承和多态

class Animal(object):
    def __init__(self):
        print("动物类型")

    def run(self):
        print("动物能奔跑")
        pass

    def sleep(self):
        print("动物睡觉")
        pass

    def walk(self):
        print("动物喝水！")
        pass
