# --*-- conding:utf-8 --*--
# @Time : 2023/12/22 00:25
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 继承动物类

from animal import Animal


class Dog(Animal):
    def __init__(self):
        print("这是狗")

    def run(self):
        print("狗跑")

    def eat(self):
        print("狗吃骨头！")

    pass
