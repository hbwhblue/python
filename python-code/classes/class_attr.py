# --*-- conding:utf-8 --*--
# @Time : 2023/12/22 01:40
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description:

"""
实例属性属于各个实例所有，互不干扰；
类属性属于类所有，所有实例共享一个属性；
不要对实例属性和类属性使用相同的名字，否则将产生难以发现的错误。
"""


class Attr(object):
    name = "类属性"  # 类属性类似于静态成员变量

    def __init__(self, desc):
        """
        构造器
        :param desc: 将参数设置给实例属性
        """
        self.desc = desc

    pass


if __name__ == '__main__':
    At = Attr("helo,world")
    if hasattr(At, 'name'):
        print("At具类雷属性：", At.name)
    else:
        print("没有类属性：", At.name)

    if hasattr(At, 'desc'):
        print("At具有实例化属性：desc")
    else:
        print("没有实例属性desc")
