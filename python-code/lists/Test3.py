#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : Test3.py
@Time       : 2023/6/15 18:41
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 列表的使用
"""
import random
import unittest


class Test(unittest.TestCase):
    def test_do_face1(self):
        """
        色子投6000次，统计每个点数出现的次数
        :return:
        """
        f1 = f2 = f3 = f4 = f5 = f6 = 0
        for _ in range(6000):
            face = random.randint(1, 6)
            if face == 1:
                f1 += 1
            elif face == 2:
                f2 += 1
            elif face == 3:
                f3 += 1
            elif face == 4:
                f4 += 1
            elif face == 5:
                f5 += 1
            else:
                f6 += 1

        print(f"1点出现了{f1}次")
        print(f"2点出现了{f2}次")
        print(f"3点出现了{f3}次")
        print(f"4点出现了{f4}次")
        print(f"5点出现了{f5}次")
        print(f"6点出现了{f6}次")

    def test_do_face2(self):
        """
        利用list投掷色子的简便方法
        :return:
        """
        counters = [0] * 6
        for _ in range(6000):
            face = random.randint(1, 6)
            counters[face - 1] += 1
        for face in range(1, 7):
            print(f"{face}点出现了{counters[face - 1]}次")

    def test_create_list(self):
        """
        利用list函数将其他序列对象转换成list对象
        :return:
        """
        item1 = list(range(1, 10))  # list函数将其他序列转换成列表，它是创建列表对象的构造器
        print("itme1=", item1, "type=", type(item1))

        item2 = list("hello")  # 将字符串转换成字符序列
        print("item2=", item2, "type=", type(item2))

    def test_list_operator(self):
        """
        列表运算符的使用，只能做+拼接，*表示重复
        :return:
        """

        items1 = [1, 2, 3, 4, 5]
        items2 = [6, 7, 8, 9]

        # 列表的拼接
        items3 = items1 + items2
        print("items3=", items3)

        # 列表的重复
        items4 = [2, 3, 4] * 3
        print("items4=", items4)

        # 某个值是否包含在列表中，类似于 JAVA 的 contains()方法
        in_items3 = 100 in items3
        print("in_items3=", in_items3)
        in_items4 = 2 in items4
        print("in_items4=", in_items4)

        # 列表长度
        items3_len = len(items3)
        print("items3 length=", items3_len)
        print("items4 length=", len(items4))

        # 列表的索引
        print("items3[8]=", items3[8], "items3[-items3_len]=", items3[-items3_len])
        items3[-1] = 100  # 将最后一个元素改成100
        print("最后一个元素修改后，列表变了：", items3)
        print("顺序的最后一个元素", items3[items3_len - 1], "倒序的第一个元素：", items3[-1])

        # 列表的切片
        print(items3[:5])  # 获取下标从0开始到5的元素，不包括5
        print(items3[4:])  # 获取下标从4开始的元素
        print(items3[-5:-7:-1])  # 获取倒数第5到倒数第7的元素，并且不包含-7的元素，步长是-1这个是必不可少，不然是取不到数据
        print(items3[::-2])  # 取出所有元素，并且是倒序去取出，步长是-2

        # 列表的比较
        list1 = [1, 2, 3, 4]
        list2 = list(range(1, 5))
        # 两个列表比较大小比得是对应所以位置上元素的内容是否相同
        print("list1和list2的内容是否相同：", list1 == list2)
        # 获取两个列表的身份对象
        print("list1的身份 id", id(list1), "list2的身份 id", id(list2))
        # 比较两个列表的内存地址是否相同
        print("list1和list2的内存地址是否相同：", list1 is list2)

        # 列表遍历
        programs = ["python", "java", "go", "php"]
        # 方法一：
        for idx in range(len(programs)):
            print(f"方法1列表遍历{idx}：", programs[idx])
        # 方法二：
        for item in programs:
            print("方法2遍历：", item)

    def test_list_method(self):
        """
        列表方法
        :return:
        """

        # 声明一个列表
        items = ["Java", "C", "Go", "Python"]

        # 使用apppend方法在列表尾部添加元素
        items.append("PHP")
        print("append后的items：", items)

        # 使用insert方法在指定位置添加元素
        items.insert(0, "C++")
        print("insert后的items：", items)

        # 删除指定元素
        items.remove("PHP")
        print("remove后的items：", items)

        # 删除指定下标
        items.pop(0)
        print("pop后的items：", items)
        del items[1]  # 性能略优于pop
        print("删除元素的第二种方法del：", items)

        # 清空列表的元素
        items.clear()
        print("清空后的items：", items)

    def test_list_index(self):
        """
        列表的位置和次数
        :return:
        """
        items = ["Python", "Java", "C++", "Go", "Python", "PHP", "Python"]
        print("Python首次出现的位置", items.index("Python"))  # Python首次出现的位置
        print("Python第2此出现的位置：", items.index("Python", 2))  # Python第2次出现的位置
        print("Python最后一次出现的位置：", items.index("Python", -1))  # Python最后一次出现的位置
        print("Go首次出现的位置:", items.index("Go"))  # Go首次出现的位置

        print("Python出现的次数：", items.count("Python"))  # Python出现的次数
        print("Go出现的次数：", items.count("Go"))  # Go出现的次数

    def test_list_sort(self):
        """
        列表的排序和反转
        :return:
        """
        items = ["Python", "Java", "C++", "Go", "Python", "PHP", "Python"]
        items.sort()
        print("列表的排序", items)
        items.reverse()
        print("列表的反转", items)

    def test_list_sort2(self):
        """
        通过sorted函数来对数据进行排序就行
        :return:
        """
        alist = [39, 19, 2, 23]
        blist = sorted(alist)  # 顺序排序
        print("顺序排序：", blist)

        rlist = sorted(alist, reverse=True)
        print("逆序排序：", rlist)

        clist = [("b", 2), ("a", 1), ("c", 3), ("d", 4)]
        clist = sorted(clist, key=lambda x: x[1], reverse=True)  # 按元祖中的值进行倒序排
        print("根据value来进行排序", clist)

    def test_list_create(self):
        """
        列表的生成
        :return:
        """
        items1 = []
        for x in range(1, 10):
            items1.append(x)
        print("items1=", items1)

        # 将一个字符串生成到列表中
        items2 = []
        string = "hello,world"
        for x in string:
            items2.append(x)
        print("items2=", items2)

        # 两个字符串通过笛卡尔积生成新的列表
        items3 = []
        for x in "ABC":
            for y in "123":
                items3.append(x + y)
        print("items3=", items3)
        print("0到-1的打印：", items3[0:-1])  # 不包含最后一个

    def test_list_derivation(self):
        """
        列表推导式
        :return:
        """

        # 构建从1-9的数字列表
        items1 = [x for x in range(1, 10)]
        print("items1=", items1)

        # 构建由hello,world字符串组成的列表
        items2 = [x for x in "hello,world" if x != ","]
        print("items2=", items2)

        # 构建由两个字符笛卡尔积构成的列表
        items3 = [x + y for x in "ABC" for y in "123"]
        print("items3=", items3)

    def test_list_enumerate(self):
        """
        利用枚举函数将序列以key,value输出,还可以指定索引的初始值，默认从0开始，也可以设置从1开始
        enumerate(num,1)
        :return:
        """
        for key, val in enumerate(range(0, 9)):
            print(f"key={key},value={val}")
