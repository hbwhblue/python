# --*-- conding:utf-8 --*--
# todo：不管是列表还是字符串都属于序列，其元素可以通过索引访问，索引是从0开始的整数，也可以从-1开始的整数，表示从后往前数的位置。
# todo: 结构：list[start:end:step]
# todo: +----+----+----+----+
# todo: | h | e | l | l | o |
# todo: +----+----+----+----+
# todo:   0   1   2   3   4
# todo:  -5  -4  -3  -2  -1
#
# todo: 注意，如果start是负数，end必须是负数即 start<end<0，并且start 是包含的，end 是不包含的。
# todo: 如果start是正数，end可以是负数，end 是不包含
# @Time : 2024/3/17 15:50
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import unittest


class BaseTestCase(unittest.TestCase):
    def test_get_single(self):
        """
        取单个元素lst[index]
        @return:
        """
        lst = [1, 2, 3, 3, 5]
        print("第一个元素：", lst[0])
        print("最后一个元素：", lst[-1])
        pass

    def test_positive_range(self):
        """
        正向取值
        @return:
        """
        lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        print("start=0,end=5,但索引end不包括5", lst[0:5])
        print("start=2,end=7,但索引end不包括7", lst[2:7])
        # 正确的写法，start是正数，end可以是负数
        print("start=1,end=-1,但索引end不包括-1", lst[1:-1])

    def test_negative_range(self):
        """
        负向取值
        @return:
        """
        lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        # 正确的写法，start是负数，end必须是负数即 start<end<0
        print("start=-5,end=-2,表示从倒数第5个开始到倒数第2个，但不包含倒数第2个", lst[-5:-2])
        # 这是错误写法，如果start是负数，end必须是负数即 start<end<0
        print("start=-2,end=3,表示从倒数第2个开始到顺数第3个，但不包含顺数第3个", lst[-2:2])

    def test_none_start(self):
        """
        无起始位置
        @return:
        """
        lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        # 相当于是list[0:5]
        print("start=None,end=5,但索引end不包括5", lst[:5])
        # 相当于list[-10:-1]
        print("start=None,end=-1,但索引end不包括-1", lst[:-1])

    def test_none_end(self):
        """
        无结束位置
        @return:
        """
        lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        # 相当于lst[6:10]
        print("start=6,end=None,表示从6开始到最后", lst[6:10])
        # 相当于是lst[-1]
        print("start=-1,end=None,就取倒数第一个", lst[-1:])

    def test_reserve(self):
        """
        反转
        @return:
        """
        lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        print("反转", lst[::-1])


if __name__ == '__main__':
    unittest.main()
