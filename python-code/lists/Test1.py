#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@NAME      :test_generator.py，对列表的append,extend,insert,del，reserve操作
@TIME      :2022/05/21 16:38:15
@AUTHOR    :hjc_042043@sina.cn
@VERSION   :1.0
"""
import unittest


class Test1(unittest.TestCase):
    """
    列表测试1
    """

    def test_print_list1(self):
        list1 = [1, 2, 3, 4, 5]
        print("取倒数第1个元素：", list1[-1]);
        print("取倒数第2个元素：", list1[-2]);
        print("取倒数第3个元素：", list1[-3]);

        print("从第2个元素到第4个元素：", list1[2:4])  # 取值的时候是前开后闭的方式，>=start,<end
        print("列表取内容，只能顺序取,错误写法：", list1[-1:2])  # 顺序取出内容,[1:2],[-1:-2]这样子取出来是正确的

        list1[len(list1):] = [6, 7, 8]  # 类似于list1.extend(),只能是列表，不能是单个元素
        print("在列表末尾追加列表方法1：", list1)

        list1.extend([9, 10, 11])  # 将列表[9,10,11]从末尾追加到list1
        print("在列表末尾追加列表方法2,使用 extends：", list1)

        list1.append(12)  # 将元素12追加到list1
        print("在列表末尾追加元素：", list1)

        list1[:0] = [-1, 0]  # 这种操作只能是列表不能是单个元素
        print("在列表开头追加列表：", list1)

        list1.insert(0, "start")  # 在列表开头添加start元素
        print("在列表开头添加元素", list1)

        list1.insert(-1, "end")  # 在列表结尾添加end元素
        print("在列表结尾添加元素", list1)

        list2 = list1[:]  # 列表复制
        list2.reverse()  # 列表反转
        print("list2反转后的内容：", list2)

        list1[1:-1] = []  # 等同于del list1[1:-1],建议使用del操作
        print("将列表第2个元素到倒数第二个元素删除：", list1)

        del list2[1:-1]
        print("list2删除后的结果：", list2)

    """
    打印列表的长度
    """

    def test_print_listLen(self):
        list1 = [0]
        list2 = []
        list3 = [1, 2, [3, 4], [5, 6], 7]
        print("list1:", len(list1))
        print("list2:", len(list2))
        print("list3:", len(list3))

    """
    @description  : 将列表的末尾3个元素截取追加到列表开头
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def test_op_list(self):
        ls = [0, 1, 2, 3, 4, 7, 8, 9]
        print("最后一个元素", ls[-1:])

        ls2 = ls[-3:]
        print("ls2的最后的3个元素:", ls2)

        print("ls[:0]", ls[:0])

        ls[:0] = ls2
        del ls[-3:]
        print("列表ls的最终结果：", ls)

        print("获取前两个元素：", ls[:2])


if __name__ == '__main__':
    unittest.main()
