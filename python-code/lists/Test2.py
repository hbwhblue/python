#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@Description:       :list的排序操作
@Date     :2022/05/22 23:47:44
@Author      :hjc_042043@sina.cn
@version     1.0
"""
import unittest


class List2:
    ls = []  # 声明数字列表
    ls_str = []  # 字符串列表
    ls_list = []  # 子列表的排序
    ls_mixed = []  # 字符串和数字混合内容
    """
    @description  :初始化信息
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def __init__(self):
        self.ls = [0, 1, 5, 6, 3, 2, 4, 7]
        self.ls_str = ["a", "b", "c", "h", "i", "x", "y", "z", "e", "f"]
        self.ls_mixed = ["a", "b", "c", 1, 2, 3, 4]
        self.ls_list = [
            [3, 4, 5],
            [2, 3, 9],
            [2, 3, 6],
            [4, 1, 7],
            [3, 1, 2],
            [4, 2, 3],
        ]

    """
    @description  :顺序排序数字，字母列表
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def sort_list(self):
        self.ls.sort()  # sort在使用时，是要求列表中所有数据项均为可比较的类型,所以不能用混合类型的
        print("数字列表的顺序排序：", self.ls)

        self.ls_str.sort()
        print("字符串列表的列表排序：", self.ls_str)

        take_second = lambda ls: ls[1]
        self.ls_list.sort(key=take_second)
        print("列表的自定义排序：", self.ls_list)

        # self.ls_mixed.sort()  # 既有字符串，又有数字的不能做排序
        # print("混合列表的排序：", self.ls_mixed)

    """
    @description  :排序子列表的内容
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def sort_sublist(self):
        self.ls_list.sort()  # 排序子列表时，规则是 先升序比较第一个元素，再升序序比较第二个元素
        print("子列表的顺序排序：", self.ls_list)


class Test2(unittest.TestCase):
    def test_sort_list(self):
        lst = List2()
        lst.sort_list()

    def test_sort_sublist(self):
        lst = List2()
        lst.sort_sublist()


if __name__ == "__main__":
    unittest.main()
