# --*-- conding:utf-8 --*--
# @Time : 2023/12/25 19:05
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 日期的测试程序
import calendar
import time
import unittest
from datetime import datetime, timedelta


class TimeTest(unittest.TestCase):
    def test_time(self):
        """
        获得时间戳
        :return:
        """
        currentTime = time.time()
        print("当前时间戳：{} 单位秒：".format(currentTime))  # 是double类型
        print("当前时间戳：{} 单位秒：".format(int(currentTime)))  # 是int类型
        print("当前时间戳：{} 单位毫秒".format(int(currentTime * 1000)))  # 毫秒
        # 时间元祖
        time_tuple = time.localtime(time.time())
        print("当前时间信息：", time_tuple)
        pass

    def test_timeinfo(self):
        """
        当前的本地时间信息
        :return:
        """
        currentTime = time.localtime()  # 获得本地时间,跟随系统时间，如果系统时间不准，它也不准
        print("当前年：", currentTime.tm_year)
        print("当前月：", currentTime.tm_mon)
        print("当前日：", currentTime.tm_mday)
        print("时：", currentTime.tm_hour)
        print("分：", currentTime.tm_min)
        print("秒：", currentTime.tm_sec)
        print("周：", currentTime.tm_wday)
        print("一年中的第几天：", currentTime.tm_yday)
        pass

    def test_format(self):
        """
        时间格式化
        :return:
        """
        currentTime = time.localtime()
        # 这个格式化可认为是国际读法
        print("使用time.asctime:", time.asctime(currentTime))
        # 使用time.strftime,将时间戳转换为日期字符串
        print("使用time.strftime:", time.strftime("%Y-%m-%d %H:%M:%S", currentTime))
        print("使用time.strftime:", time.strftime("%Y-%m-%d", currentTime))

        currentTime2 = time.time()
        print("开始睡眠3秒: {}".format(time.strftime("%Y-%m-%d %H:%M:%S")))
        time.sleep(3)
        print("睡眠后：{}".format(time.strftime("%Y-%m-%d %H:%M:%S")))
        print("时间相差：",time.time() - currentTime2)
        pass

    def test_datetime(self):
        print("当前的日期时间：",datetime.now())

        t = datetime(2023,12,25,20,29,51)
        print("获取指定的日期时间：",t)

        # 日期时间转换成时间戳
        tt = t.timestamp()
        print("转换为时间戳：",tt)

        # 时间戳转化成日期时间
        print("时间戳反转：",datetime.fromtimestamp(tt))

    def test_str2datetime(self):
        """
        日期字符串转换成日期
        :return:
        """
        # 字符串时间转datetime
        strTime = "2023-12-25 20:30:35"
        d = datetime.strptime(strTime,"%Y-%m-%d %H:%M:%S")
        print("日期字符串转化为datetime：",d)
        print("格式化后的类型：",type(d))

        print("格式化后换成时间戳：",d.timestamp())

        # datetime转换成字符串
        print("datetime转字符串：",d.strftime("%Y-%m-%d %H:%M:%S"))

    def test_timedelta(self):
        """
        计算时间,做多计算到周
        :return:
        """
        currentTime = datetime.now()
        print("当前时间：",currentTime.strftime("%Y-%m-%d %H:%M:%S"))

        # 5秒前的日期时间
        print("5秒前：",currentTime - timedelta(seconds=5))
        # 3分钟前的日期时间
        print("3分前：",currentTime - timedelta(minutes=3))
        # 2个小时前
        print("2小时前：",currentTime - timedelta(hours=2))
        # 3天前
        print("3天前：",currentTime - timedelta(days=3))
        # 1周前
        print("1周前：",currentTime - timedelta(weeks=1))

    def test_calendar(self):
        print("月日历：",calendar.month(2023,12))
        print("整年日历：",calendar.prcal(2023))

    pass