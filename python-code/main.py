from collections import namedtuple
from datetime import datetime

from files.common import get_sources_dir
from test.testmath import Cal
from utils.tools import Tools

if __name__ == "__main__":
    print(111)

    now = datetime.now()
    print(now)

    Point = namedtuple("Point", ["x", "y"])
    p = Point(1, 2)
    print(p.x, p.y)

    cal = Cal(8, 9)
    sum = cal.addNum()
    print(sum)

    sub = cal.subNum()
    print(sub)
    # sum = testmath.add(1,2)
    # print(sum)

    print(Tools.get_root_dir())

    print(get_sources_dir())
