# --*-- conding:utf-8 --*--
# todo：进程管理
#  什么是进程？
#  1. 进程
# 	> 程序：例如xxx.py这是程序，是一个静态的
# 	> 进程：一个程序运行起来后，代码+用到的资源 称为进程，它是操作系统分配资源的基本单元。不仅可以通过线程完成多任务，进程也可以的
#  2. 进程的状态
#   在工作中，任务数往往大于cpu的核心数，即一定有一些任务正在执行，而另外一些任务在等待cpu进行执行，异常导致了有了不同的状态
#   新建------启动---->就绪<------调度----->运行----结束---->死亡
# 				↑					     ↓
# 				↑					     ↓等待条件
# 				↑					     ↓
# 				↑----满足条件-----等待(阻塞)
#  <p>
#   > 就绪：运行的条件都已经满足，正在等待cpu的执行
#   > 执行：cpu正在执行其他功能
#   > 等待：也就是阻塞，等待某些条件满足，例如一个程序等待sleep结束，此时就处于等待的状态
#
# @Time : 2024/1/8 16:49
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import os
import time
import unittest
from multiprocessing import Process


class TestProcess(unittest.TestCase):
    def test_run_proc(self):
        """
        创建子进程时，只需传入一个执行函数和函数参数,创建一个process实例,用start()方法启动
        """
        p = Process(target=run_proc)
        p.start()

        while True:
            print("----1----")
            time.sleep(1)

    def test_get_pid(self):
        print("父进程运行中,pid=%d..." % os.getpid())
        p = Process(target=run_get_pid)
        p.start()

    def test_run_getpid(self):
        p = Process(target=run_proc_args, args=("hjc", 19), kwargs={"email": "hjc_042043@sina.cn"})
        p.start()  # 开始进程
        time.sleep(1)
        p.terminate()  # 关闭子进程
        p.join()  # 等待进程p中的子进程都结束再退出


def run_proc():
    """子进程要执行的代码"""
    while True:
        print("----2----")
        time.sleep(1)


def run_get_pid():
    """子进程要执行的代码"""
    print("子进程运行中,pid=%d..." % os.getpid())
    print("子进程将要结束...")


def run_proc_args(name, age, **kwargs):
    """
    子进程的参数测试
    @param name: 参数1
    @param age: 参数2
    @param kwargs: 关键字参数
    @return:
    """
    for e in range(10):
        print("子进程运行中，name=%s,age=%d,pid=%d..." % (name, age, os.getpid()))
        print(kwargs)
        time.sleep(0.2)
