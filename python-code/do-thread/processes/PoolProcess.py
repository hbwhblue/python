# --*-- conding:utf-8 --*--
# todo：进程池
#   应用背景
#   > 当需要创建的子进程数量不多时，可以直接利用multiprocessing中的process动态生成多个进程，
#   > 但如果是上百甚至上千个目标，手动的去创建工程的工作量巨大，此时就可以用到multiprocessing模块提供的`Pool`方法。
# <p>
# todo Pool类语法说明
#   语法格式：multiprocessing.pool.Pool([processes,initializer,initargs,maxtasksperchild,context)
#   参数说明：
#   - processes：工作进程数目，如果processes为None,则使用os.cpu_count()返回的值。
#   - initializer：如果initializer不为空，则为每个工作进程将会在启动时调用initializer(*initargs)。
#   - maxtasksperchild：一个工作进程在它退出或一个新的工作进程代替之前完成的任务数量，为了释放未使用的资源。
#   - context：用于指定启动的工作进程的上下文。
# <p>
# todo 两种方式向进程池提交任务
#   apply(func,args,kwds)：阻塞方式
#   apply_sync(func,args,kwds)：
#       非阻塞方式，使用非阻塞方式调用func（并行执行，阻塞方式必须等待上一个进程退出才能执行下一个进程），
#       args为传递给func参数列表，kwds为传递给func的关键字参数列表
# <p>
# todo Pool进程池的概念
#  > 初始化Pool时，可以指定一个最大进程数，当有新的请求提交到Pool中时，如果池还没有满，那么就会创建一个新的进程来执行该请求；
#  > 但如果池中的进程数已经达到指定的最大值，那么该请求就会等待，直到池中有进程结束，才会用之前的进程来执行新的任务。
# <p>
# todo multiprocessing.Pool常用函数
#   方法名             说明
#   close()           关闭Pool，使其不再接受新的任务
#   terminate()       不管任务是否完成，立即终止
#   join()            主进程阻塞，等待子进程退出，必须在close或terminate之后使用
#  对Pool对象调用join()方法会等待所有子进程执行完毕，调用join()之前必须先调用close()，调用close()之后就不能继续添加新的Process了
# <p>
# todo multiprocessing.Manager().Queue()
#   使用Pool创建进程,不能使用multiprocessing.Queue()
#
# @Time : 2024/1/10 19:44
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import datetime
import os
import random
import time
import unittest
from multiprocessing import Pool, Manager, Process


class TestPoolProcess(unittest.TestCase):

    def test_run_proc(self):
        print("父进程id（%s）。。。" % os.getpid())
        p = Process(target=run_proc, args=('test子进程',))
        p.start()  # 开始启动子进程
        p.join()  # 等待p中的所有子进程执行完成

    def test_work(self):
        p = Pool(3)  # 设置3个长度的线程池
        for e in range(0, 10):
            # Pool().apply_async(要调用的目标函数，(传递给目标函数的参数元祖,))
            # 每循环一次，将会用空闲出来的子进程去调用目标函数
            p.apply_async(do_work, (e,))
        print("------start-------")
        p.close()  # 关闭线程池，关闭p对象后，不再接收新的请求
        p.join()  # 等待p中所有子进程的执行完成，必须放在close()语句之后
        print("------end---------")

    def test_quque(self):
        print("(%s) start" % os.getpid())
        q = Manager().Queue()  # 在进程池中的队列
        po = Pool()
        po.apply_async(do_write, (q,))  # 异步调用do_write函数,将数据写入到队列中去
        print("(%s) end" % os.getpid())

        time.sleep(1)  # 先是上面的任务向Queue存入数据，然后再让下面的任务从中读取数据

        # 对Pool对象调用join()方法会等待所有子进程执行完毕，调用join()之前必须先调用close()，调用close()之后就不能继续添加新的子进程了
        po.apply_async(do_read, (q,))
        po.close()  # 关闭进程池，关闭po对象后，不在接收新的请求
        po.join()  # 等待P中所有子进程的执行完成，必须放在close()语句之后
        print("(%s) End" % os.getpid())


def do_work(msg):
    """
    进程池的应用
    """
    t_start = time.time()  # 开始

    print("%s开始执行，进程号为%d" % (msg, os.getpid()))
    # random.random()随机生成0-1之间的浮点数
    print(datetime.datetime.now())
    time.sleep(random.random() * 2)

    t_end = time.time()  # 结束
    print("%s执行完毕，耗时%.2f" % (msg, (t_end - t_start)))


def do_read(q):
    """
    在进程池中队列的读取数据
    @param q:
    @return:
    """
    print("reader启动(%s)，父进程为(%s)" % (os.getpid(), os.getpid()))
    for e in range(q.qsize()):
        msg = q.get(True)  # 如果队列为空，此程序将被阻塞(停留在读取状态)，直到读取到消息为止
        print("reader从Queue获取到消息：%s" % msg)


def do_write(q):
    """
    在进程池中队列的写入数据
    @param q:
    @return:
    """
    print("writer启动(%s)，父进程为(%s)" % (os.getpid(), os.getpid()))
    for e in "itcast":
        q.put(e)
        # time.sleep(random.random() * 2)


def run_proc(name):
    """
    子进程执行
    @param name:子进程名字
    @return:
    """
    print("执行子进程 %s (%s)。。。" % (name, os.getpid()))
