# --*-- conding:utf-8 --*--
# todo：进程之间，不能共享全局变量
#
# @Time : 2024/1/8 18:31
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import os
import time
from multiprocessing import Process

g_nums = [11, 22]


def work1():
    """
    子进程要执行的代码
    @return:
    """
    print("in process1 pid=%d，nums=%s" % (os.getpid(), g_nums))

    for e in range(3):
        g_nums.append(e)
        time.sleep(1)
        print("in process1 pid=%d，nums=%s" % (os.getpid(), g_nums))


def work2():
    """
    子进程要执行的代码
    @return:
    """
    print("in process2 pid=%d，nums=%s" % (os.getpid(), g_nums))


if __name__ == '__main__':
    p1 = Process(target=work1)
    p1.start()  # 进程开始
    p1.join()  # 进程终止

    p2 = Process(target=work2)
    p2.start()
