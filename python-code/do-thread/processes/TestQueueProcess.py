# --*-- conding:utf-8 --*--
# todo：进程间通信
#   > Process之间肯定是需要通信的，操作系统提供了很多机制来实现进程间的通信。
#   > Python的multiprocessing模块包装了底层的机制，提供了Queue、Pipes等多种方式来交换数据
#   > 我们以Queue为例，在父进程中创建两个子进程，一个往Queue里写数据，一个从Queue里读数据
#  我们以Queue为例，在父进程中创建两个子进程，一个往Queue里写数据，一个从Queue里读数据：
import os
import random
import time
from multiprocessing import Queue, Process


# @Time : 2024/1/11 15:59
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
def write(q):
    """
    写数据进程执行的代码
    @param q: 进程的Queue对象
    @return:
    """
    print("写数据的子进程：%s" % os.getpid())
    for e in ['A', 'B', 'C', 'D']:
        print("Put %s to queue..." % e)
        q.put(e)
        time.sleep(random.random() * 2)


def read(q):
    """
    读数据进程执行的代码
    @param q: 进程的Queue对象
    @return:
    """
    print("读数据的子进程：%s" % os.getpid())
    while True:
        value = q.get(True)  # 这里是如果消息为空，就会阻塞，直到等到有消息为止
        print("Get %s from queue。。。" % value)


if __name__ == '__main__':
    # 父进程创建Queue，并传给各个子进程
    q = Queue()
    pw = Process(target=write, args=(q,))
    pr = Process(target=read, args=(q,))

    # 启动子进程pw，写入数据：
    pw.start()
    # 启动子进程pr,读取数据
    pr.start()
    # 等待pw结束
    pw.join()
    # pr进程里死循环,无法等待其结束，只能强行终止
    pr.terminate()
