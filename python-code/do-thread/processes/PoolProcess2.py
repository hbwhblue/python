# --*-- conding:utf-8 --*--
# todo：如果要启动大量的子进程，可以用进程的方式批量创建子进程
#
# @Time : 2024/1/11 14:35
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import os
import random
import time
from multiprocessing import Pool


def long_time_task(name):
    """
    子进程程序
    @param name: 子进程名称
    @return:
    """
    print("运行子进程 %s (%s)..." % (name, os.getpid()))

    start_time = time.time()
    time.sleep(random.random() * 3)
    end_time = time.time()
    print("子进程 %s 执行 %0.2f seconds." % (name, (end_time - start_time)))


if __name__ == '__main__':
    print("父进程 %s" % os.getpid())

    p = Pool(4)  # 进程池的的大小是4
    for e in range(0, 5):
        p.apply_async(long_time_task, args=(e,))  # 并行异步执行子进程
    print("等待所有子进程完成...")

    p.close()  # 关闭进程池，关闭后，不再接收新的任务
    p.join()  # 等待进程池中的所有任务完成
    print("所有子进程完成！")
