# --*-- conding:utf-8 --*--
# todo：分布式进程
#   举个例子：如果我们已经有一个通过Queue通信的多进程程序在同一台机器上运行，现在，由于处理任务的进程任务繁重，希望把发送任务的进程和处理任务的进程分布到两台机器上。
#   怎么用分布式进程实现？
#    > 原有的Queue可以继续使用，但是，通过managers模块把Queue通过网络暴露出去，就可以让其他机器的进程访问Queue了。
#    > 我们先看服务进程，服务进程负责启动Queue，把Queue注册到网络上，然后往Queue里面写入任务
#  <p>
# todo：创建分布式进程的六个步骤：
#   1、简历Queue队列，用来进行进程间通信。服务进程创建任务队列task_queue,用来作为传递任务给任务进程的通道；
#   服务进程创建结果队列result_queue，作为任务进程完成任务后回复服务进程的通道；
#  <p>
#   2、把第一步中建立的队列在网络上注册，暴露给其他进程(主机)，注册后获得网络队列，相当于本地队列的映像。
#  <p>
#   3、建立一个对象(Queuemanager(BaseManager))实例manager，绑定端口和验证口令。
#  <p>
#   4、启动第三步建立的实例。即启动管理manager，监管信息通道。
#  <p>
#   5、通过管理实例的方法获得通过网络访问Queue对象，即再把网络队列实例化成可以使用的本地队列。
#  <p>
#   6、创建任务到"本地"队列中，自动上传任务到网络队列中，分配给任务进程进行处理。
#
# @Time : 2024/1/12 15:35
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import queue
from multiprocessing.managers import BaseManager

# 第一步，创建task_queue和result_queue，用来存放任务和结果
TASK_MAX = 10
task_queue = queue.Queue(TASK_MAX)
result_queue = queue.Queue(TASK_MAX)


# 队列管理类继承继承管理类
class Queuemanager(BaseManager):
    pass


def main():
    # 第二步：把创建的两个队列注册在网络上，利用register方法，callable参数关联了Queue对象，将Queue对象在网络中暴露
    Queuemanager.register('get_task_queue', callable=lambda: task_queue)
    Queuemanager.register('get_result_queue', callable=lambda: result_queue)

    # 第三步:绑定端口8001，设置验证口令`abc`,这个相当于对象的初始化，authkey字符串前面加b表示bytes类型的对象
    manager = Queuemanager(address=('127.0.0.1', 8001), authkey=b'abc')

    # 第四步：启动Queue，监听信息通道
    manager.start()

    # 第五步：获得通过网络访问的Queue对象
    task = manager.get_task_queue()
    result = manager.get_result_queue()

    # 第六步：添加任务
    for url in ["image_url_" + i for i in range(10)]:
        print('put task %s ....' % url)
        task.put(url)

    # 第七步：返回结果
    for e in range(10):
        print("result is %s" % result.get(timeout=10))

    # 关闭管理
    manager.shutdown()
    print("master exit.")


if __name__ == '__main__':
    main()
