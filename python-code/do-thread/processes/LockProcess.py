# --*-- conding:utf-8 --*--
# todo：加锁的操作
#
# @Time : 2024/1/10 19:40
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import time
from multiprocessing import Process, Lock


def add_for_lock(num: int, value: int, lock: Lock) -> None:
    """
    加锁的情况
    @param num: 初始值
    @param value: 添加加的值
    @param lock: 进程锁
    @return:
    """
    try:
        lock.acquire()
        print("add{0}:num={1}".format(value, num))
        for i in range(0, 2):
            num += value
            print("add{0}:num={1}".format(value, num))
            time.sleep(1)
    except Exception as e:
        raise e
    finally:
        lock.release()


if __name__ == '__main__':
    num = 0
    lock = Lock()
    p1 = Process(target=add_for_lock, args=(num, 1, lock))
    p2 = Process(target=add_for_lock, args=(num, 2, lock))
    p1.start()
    p2.start()
