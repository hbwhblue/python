# --*-- conding:utf-8 --*--
# todo：进程间同步使用锁
#   进程间同步-Lock
#   > 锁是为了确保数据一致性。比如读写锁，每个进程给一个变量增加+1，但是如果在一个进程读取但还没写入的时候，另外的进程也同时读取了，并写入该值，
#     则最后写入的值是错误的，这个时候就需要加锁来保持数据一致性。
#   > 通过使用Lock来控制一段代码在同一时间只能被一个进程执行。Lock对象的两个方法，acquire()用来获取锁，release()用来释放锁。
#     当一个进程调用acquire()时，如果锁的状态为unlocked，那么会立即修改为locked并返回，这是该进程即获得了锁。如果锁的状态为locked,那么调用acquire()的进程则阻塞。
# <p>
#  todo：Lock的语法说明：
#   1、lock = multiprocessing.Lock()：创建一个锁
#   2、lock.acquire()：获取锁
#   3、lock.release()：释放锁
#   4、with lock：自动获取、释放锁类似于with open() as f：
#
# @Time : 2024/1/10 13:20
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import time
import unittest
from multiprocessing import Process


class TestNoLockProcess(unittest.TestCase):
    def test_no_add_lock(self):
        """
        没有加锁，两个进程交互计算
        @return:
        """
        num = 0
        p1 = Process(target=add, args=(num, 1))  # 声明一个子进程对象p1，调用add函数
        p2 = Process(target=add, args=(num, 2))  # 声明一个子进程对象p2，调用add函数
        p1.start()
        p2.start()


def add(num, value):
    """
    不加锁的情况
    @param num:
    @param value:
    @return:
    """
    print("add{0}:num={1}".format(value, num))
    for e in range(0, 2):
        num += value
        print("add{0}:num={1}".format(value, num))
        time.sleep(1)
