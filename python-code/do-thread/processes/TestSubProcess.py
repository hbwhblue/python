# --*-- conding:utf-8 --*--
# todo：子进程的测试
#
# @Time : 2024/1/11 15:16
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import subprocess

if __name__ == '__main__':
    print('$ nslookup www.douban.com')
    res = subprocess.call(['nslookup', 'www.douban.com'])
    print('Exit code：', res)
