# --*-- conding:utf-8 --*--
# todo：利用队列，进程之间进行通信
#   可以使用multiprocess模块的Queue实现多进程之间的数据传递,Queue本身是一个消息队列程序。
#
# @Time : 2024/1/8 18:52
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import time
import unittest
from multiprocessing import Queue, Process


class TestQueueProcess(unittest.TestCase):
    def test_queue(self):
        queue_put_get()
        pass

    def test_queue2(self):
        # 父进程中创建Queue，并传给各个子进程
        q = Queue(2)
        pw = Process(target=write, args=(q,))
        pr = Process(target=read, args=(q,))

        # 启动子进程pw,写入
        pw.start()
        # 启动子进程pw,读取
        pr.start()

        # 等待pw结束
        pw.join()
        # 等待pr结束
        pr.join()

        # pr进程这里是死循环，无法等待其结束，只能强行终止：
        print('')
        print('所有数据都写入并且读完！')


def queue_put_get():
    """
    消息推送
    Queue.put(item,[block,timeout])，将item消息写入队列，block默认值=True。
    1、如果block使用默认值，且没有设置timeout(单位秒)，消息队列如果已经没有空间可写入，此时程序将被阻塞（停在写入状态），直到从消息队列腾出空间为止，
    如果设置了timeout，则会等待timeout秒，若没空间，则抛出"Queue.Full"异常。
    2、如果block值为False，消息队列没有空间可写入，则会立刻抛出Queue.Full异常
    @return:
    """
    q = Queue(3)  # 初始化一个Queue对象,最多接收3条put消息
    q.put("消息1")
    q.put("消息2")
    print("队列是否满了？", q.full())
    q.put("消息3")
    print("队列是否满了？", q.full())

    # 因为消息列队已满下面的try都会抛出异常，第一个try会等待2秒后再抛出异常，第二个Try会立刻抛出异常
    # try:
    #     q.put("消息4", True, 2)
    # except Exception as e:
    #     print("Exception：", e)
    #     # print("消息队列已满，现有消息数量:%d" % q.qsize())
    #
    # try:
    #     q.put_nowait("消息4")  # 相当于是q.put("消息4", False)
    # except Exception as e:
    #     print("Exception2：", e)
    #     print("消息队列已满，现有消息数量:%d" % q.qsize())

    # 推荐的方式,在put之前，先判断队列是否已经满了
    if not q.full:
        q.put_nowait("消息4")  # 相当于是q.put("消息4", False)

    # 消息消费
    """
    消息拉取
    Queue.get([block,timeout])，获取队列中的一条消息，然后将其从队列中移除，block默认值为True。
    1、如果block使用默认值，且没有设置timeout(单位秒)，消息队列如果为空，此时程序将会被阻塞(停在读取状态)，直到从消息队列读到消息为止，
        如果设置了timeout，则会等待timeout，若没有读到任何消息，则抛出"Queue.Empty"异常
    2、如果block为False,消息队列如果为空,则会立即抛出"Queue.Empty"异常
    """
    if not q.empty():
        for i in range(3):
            q.get_nowait()  # 相当于是q.get("消息", False)


def write(q: Queue) -> None:
    """
    往队列中写数据
    @param q: 队列类型
    @return:
    """
    for e in ['A', 'B', 'C']:
        print("put %s to queue..." % e)
        q.put(e)
        time.sleep(2)


def read(q: Queue) -> None:
    """
    从队列中读取数据
    @param q:
    @return:
    """
    while True:
        if not q.empty():
            value = q.get()
            print("get %s from queue。" % value)
            time.sleep(1)
        else:
            break
