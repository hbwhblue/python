# --*-- conding:utf-8 --*--
# todo：线程污染
#   来看看多个线程同时操作一个变量怎么把内容给改乱了
#
# @Time : 2024/1/11 16:49
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import random
import threading
import time

blance = 0


def change_it(n):
    # 先存后取，结果应该=0
    global blance
    blance = blance + n
    blance = blance - n


def run_thread(n):
    for e in range(20000):
        change_it(n)
        if e % 500 == 0:
            time.sleep(random.random())


if __name__ == '__main__':
    t1 = threading.Thread(target=run_thread, args=(5,))
    t2 = threading.Thread(target=run_thread, args=(8,))
    t1.start()
    t2.start()
    t1.join()
    t2.join()

    print(blance)
