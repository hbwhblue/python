# --*-- conding:utf-8 --*--
# todo：学生类
#
# @Time : 2024/1/11 20:07
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
class Student(object):
    """
    学生类
    """

    def __init__(self, name, score):
        self.name = name
        self.nation = "中国"
        self.score = score

    def print_user_info(self):
        """
        打印用户信息
        @return:
        """
        print("国籍：" + self.nation + "，姓名：" + self.name)

    def print_grade(self):
        """
        打印学生成绩
        @return:
        """
        if self.score >= 90:
            print(self.name + "成绩等级：A")
        elif 75 <= self.score < 90:
            print(self.name + "成绩等级：B")
        elif 60 <= self.score < 74:
            print(self.name + "成绩等级：C")
        else:
            print(self.name + "成绩等级：D")
