# --*-- conding:utf-8 --*--
# todo：共享全局变量举例2
#
# @Time : 2024/1/7 17:22
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading
import time


def work1(nums: list) -> None:
    nums.append(33)
    print("-----in work1----", nums)


def work2(nums: list) -> None:
    time.sleep(1)
    print("-----in work2-----", nums)


# 全局变量
g_nums = [5, 7, 9]
t1 = threading.Thread(target=work1, args=(g_nums,))
t1.start()
t1.join()  # 等待t1线程完成

t2 = threading.Thread(target=work2, args=(g_nums,))
t2.start()
t1.join()
