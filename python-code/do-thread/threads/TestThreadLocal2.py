# --*-- conding:utf-8 --*--
# todo：使用ThreadLocal来解决每层函数的传递问题
#
# @Time : 2024/1/11 20:10
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading
import unittest

from Student import Student

# 设置本地线程全局对象
thread_local = threading.local()


def do_student():
    std = thread_local.student
    std.print_user_info()
    std.print_grade()


def process_thread(user_name, score):
    thread_local.student = Student(user_name, score)
    do_student()
    pass


class TestThreadLocal2(unittest.TestCase):
    def test_thread_local(self):
        t1 = threading.Thread(target=process_thread, args=('沈西林', 62))
        t2 = threading.Thread(target=process_thread, args=('王小二', 91))
        t3 = threading.Thread(target=process_thread, args=('阮小七', 55))
        t4 = threading.Thread(target=process_thread, args=('李逵', 77))

        t1.start()
        t2.start()
        t3.start()
        t4.start()
        t1.join()  # 等待线程结束
        t2.join()
        t3.join()
        t4.join()
