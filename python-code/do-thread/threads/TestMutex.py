# --*-- conding:utf-8 --*--
# todo：互斥锁
#  当多线程几乎同时修改某一个共享数据的时候,需要进行同步控制
#  >1、线程同步能够保证多个线程安全访问竞争资源,最简单的同步机制是引入互斥锁
#  >2、互斥锁为资源引入一个状态：锁定/非锁定
#  某个线程要更改共享数据时,先将其锁定，此时资源的状态为"锁定"，其他线程不能修改;
#  直到该线程释放资源,将资源状态变成"非锁定"，其他的线程才能再次锁定该资源。互斥锁保证了每次只有一个线程进行写入操作,从而保证了多线程情况下数据的正确性
# <p>
# todo：使用格式
#  创建锁
#  mutex = threading.Lock()
#  锁定
#  mutex.acquire()
#  释放
#  mutex.release()
#
# @Time : 2024/1/8 14:25
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading

g_num = 0
mutex = threading.Lock()


def work1():
    global g_num
    mutex.acquire()  # 先锁定第一块内容

    for e in range(50000000):
        g_num += 1
    print("在task1中,g_num=%d" % g_num)

    mutex.release()  # 释放锁


def work2():
    global g_num
    mutex.acquire()  # 锁定第二块内容，在第一块内容未完成之前，无法启动

    for e in range(50000000):
        g_num += 1
    print("在task2中,g_num=%d" % g_num)

    mutex.release()


if __name__ == '__main__':
    t1 = threading.Thread(target=work1)
    t2 = threading.Thread(target=work2)
    t1.start()
    t1.join()

    t2.start()
    t2.join()
    pass
