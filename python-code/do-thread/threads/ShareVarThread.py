# --*-- conding:utf-8 --*--
# todo：共享全局变量(重点)
#   总结：
#   > 在一个进程内的所有线程共享全局变量，很方便在多个线程间共享数据
#   > 缺点就是，线程对全局变量随意修改可能造成多线程之间对全部变量的混乱(即线程非安全)
#
# @Time : 2024/1/7 15:50
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading
import time

global_num = 100

def work1():
    """
    全局变量设置
    """
    global global_num
    for i in range(3):
        global_num +=1

    print("-----in work1,global_num is %d-----" % global_num)

def work2():
    global global_num
    print("-----in work2,global_num is %d-----" % global_num)

print("----线程创建之前global_num is %d------" % global_num)

# 启动一个线程
t1 = threading.Thread(target=work1)
t1.start()
t1.join()

# 延迟1秒钟来保证线程t1的任务完成
time.sleep(1)

# 启动第二个线程
t2 = threading.Thread(target=work2)
t2.start()
t2.join()