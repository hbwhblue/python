# --*-- conding:utf-8 --*--
# todo：锁
#  > Lock锁是Python的原始锁，在锁定时不属于任何一个线程。在调用了 lock.acquire() 方法后，进入锁定状态，lock.release()方法可以解锁。
#    底层是通过一个函数来实现的，会根据不同的操作系统选择一个最有效的版本实例
#  > 1、RLock被称为重入锁，RLock锁是一个可以被同一个线程多次 acquire 的锁，但是最后必须由获取它的线程来释放它，
#    不论同一个线程调用了多少次的acquire，最后它都必须调用相同次数的 release 才能完全释放锁，这个时候其他的线程才能获取这个锁。
#    2、acquire()/release() 对可以嵌套，重入锁必须由获取它的线程释放。一旦线程获得了重入锁，同一个线程再次获取它将不阻塞。
#    3.RLock内部的锁的实现和Lock用的是同一个函数。
# <p>
# todo: Lock和RLock的区别
#   锁	    是否同一线程	是否可重复加锁
#   Lock	否	        否
#   RLock	是	        是
# todo: 总结
#  > Lock在锁定时不属于特定线程，也就是说，Lock可以在一个线程中上锁，在另一个线程中解锁。
#  > 而对于RLock来说，只有当前线程才能释放本线程上的锁，即 解铃还须系铃人。
#
# @Time : 2024/1/8 12:51
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading
import unittest


class TestLock(unittest.TestCase):
    def test_lock(self):
        """
        原始锁
        """
        t = threading.Thread(target=lock_func)
        t.start()
        t.join()

    def test_rlock(self):
        """
        重入锁
        """
        t = threading.Thread(target=rlock_func)
        t.start()
        t.join()


def lock_func():
    """
    Lock的简单使用，它可以在所有线程中使用
    它可以在线程A中加锁，在线程B中解锁
    """
    lock = threading.Lock()
    lock.acquire()
    print("print lock")
    lock.release()

def rlock_func():
    """
    RLock允许在同一线程中被多次acquire
    不论同一个线程调用了多少次的acquire，最后它都必须调用相同次数的 release 才能完全释放锁
    """
    rlock = threading.RLock()
    if rlock.acquire(): # 第一把锁
        print("first lock")

        if rlock.acquire(): # 第二把锁
            print("second local")
            rlock.release() # 解锁第二把锁

        rlock.release() # 解锁第一把锁


if __name__ == '__main__':
    pass
    # unittest.main()