# --*-- conding:utf-8 --*--
# todo：利用初始化线程来创建线程
#
# @Time : 2024/1/6 23:11
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading
import time
import unittest


class TestThread(unittest.TestCase):
    def test_single(self):
        """
        单线程处理
        """
        for i in range(5):
            saySorry()

    def test_multi(self):
        """
        多线程处理
        """
        for i in range(5):
            t = threading.Thread(target=saySorry)
            t.start()
            t.join()

    def test_multi2(self):
        print("-----开始-----：%s" % time.time())

        t1 = threading.Thread(target=sing)
        t2 = threading.Thread(target=dance)
        t1.start()
        t2.start()
        t1.join()  # 等待线程结束
        t2.join()

        while True:
            length = len(threading.enumerate())
            print("当前线程数为：%d" % length)
            if length <= 1:
                break
            time.sleep(0.5)

        print("-----结束-----：%s" % time.time())


def saySorry():
    print("hello,world!")
    time.sleep(1)


def sing():
    for i in range(5):
        print("正在唱歌...%d" % i)
        time.sleep(1)


def dance():
    for i in range(5):
        print("正在跳舞...%d" % i)
        time.sleep(1)


if __name__ == '__main__':
    unittest.main()
