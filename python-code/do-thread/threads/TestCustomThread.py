# --*-- conding:utf-8 --*--
# todo：自定义创建线程
#  > 为了让每个线程的封装性更完美，所以使用threading模块时，往往定义一个子类class,只要继承threading.Thread就可以了，然后重写run方法
#  > threading.Thread类有一个run方法,用于定义线程的功能函数，可以在自己的线程类中覆盖该方法。
#  > 而创建自己的线程实例后，通过Thread类的start()，可以启动该线程，交给虚拟机进行调度，当该线程获得执行的机会时，就会调用run方法进行执行
#  总结：
#   1、每个线程默认有一个名字，尽管下面的例子中没有指定线程对象的name，但是python会自动为线程指定一个名字。
#   2、当线程的run()方法结束是该线程完成。
#   3、无法控制线程调度程序，但可以通过别的方式来影响线程调度的方式。
#
# @Time : 2024/1/7 14:19
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading
import time
import unittest


class TestCustomThread(unittest.TestCase):
    def test_run(self):
        """
        启动一个自定义线程去执行任务
        """
        t = MyThread()
        t.start()  # 开始启动一个线程
        t.join()

    def test_run_seq(self):
        """
        todo 测试执行顺序,启动5个线程
         > 从代码和执行结果我们可以看出，多线程程序的执行顺序是不确定。
         > 当执行到sleep语句时，线程将被阻塞(Blocked)，到sleep结束后，线程进入就绪（Runable）状态，等待调度。
         > 而线程调度将自行选择一个线程执行。下面的代码中只能保证每个线程都运行完整个run函数，但是线程的启动顺序、run函数中每次循环的执行顺序都不能确定。
        """
        for e in range(5):
            t = MyThread()
            t.start()
            t.join()


class MyThread(threading.Thread):
    def run(self):
        for i in range(3):
            time.sleep(1)
            msg = "I'm " + self.name + " @ " + str(i)
            print(msg)


if __name__ == '__main__':
    unittest.main()
