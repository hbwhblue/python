# --*-- conding:utf-8 --*--
# todo：使用dict来存储每个线程标识
#  > 在多线程环境下，每个线程都有自己的数据。一个线程使用自己的局部变量比使用全局变量好，因为局部变量只有线程自己能看见，不会影响其他线程，而全局变量的修改必须加锁。
#  > 但局部变量也有问题，就是在函数调用的时候，传递起来很麻烦
#
# @Time : 2024/1/11 17:57
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading

from Student import Student

# 全局变量集合，用来存放学生对象
global_dict = {}


def process_student(name, score):
    """
    进程处理学生任务
    如果用一个全局dict存放所有的Student对象，然后以thread自身作为key获得线程对应的Student对象如何？
    """
    std = Student(name, score)
    # 把std放到全局变量global_dict中去
    global_dict[threading.current_thread()] = std
    do_task_1()
    do_task_2()


def do_task_1():
    """
    不传入std，而是根据当前线程查找
    @return:
    """
    std = global_dict[threading.current_thread()]
    std.print_user_info()
    pass


def do_task_2():
    """
    不传入std，而是根据当前线程查找
    @return:
    """
    std = global_dict[threading.current_thread()]
    std.print_grade()
    pass


if __name__ == '__main__':
    t1 = threading.Thread(target=process_student, args=('黄锦潮', 62))
    t2 = threading.Thread(target=process_student, args=('张三', 91))
    t3 = threading.Thread(target=process_student, args=('李四', 55))
    t4 = threading.Thread(target=process_student, args=('王五', 77))

    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()  # 等待线程结束
    t2.join()
    t3.join()
    t4.join()
