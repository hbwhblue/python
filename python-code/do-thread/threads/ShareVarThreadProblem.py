# --*-- conding:utf-8 --*--
# todo：共享全局变量存在的问题
#   多线程开发可能遇到的问题：
#   假设两个线程t1和t2都要对全局变量g_num(默认是0),进行加1运算，t1和t2都各对g_num加10次，g_num的最终的结果应该为20。
#   但是由于是多线程同时操作，有可能出现下面情况：
#   1、在g_num=0时，t1取得g_num=0。此时系统把t1调度为"sleeping"状态，把t2转换为"running"状态，t2也获得g_num=0
#   2、然后t2对得到的值进行+1并赋值给g_num，使得g_num=1
#   3、然后系统又把t2调度为"sleeping"，把t1转为"running"。线程t1又把它之前得到的0，+1赋值给g_num
#   4、这样导致虽然t1和t2都对g_num加1，但结果仍然是g_num=1
#   结论：如果多线程同时对同一个全局变量操作，会出现资源竞争问题，从而数据结果会不正确
#
# @Time : 2024/1/7 18:10
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading
import time

g_num = 0


def work1(num):
    """
    在work1方法中，给全局变量自增计算
    """
    global g_num
    for e in range(num):
        g_num += 1
    print("----in work1，g_num is %d----" % g_num)


def work2(num):
    """
    在work2方法中，给全局变量自增计算
    """
    global g_num
    for e in range(num):
        g_num += 1
    print("----in work2，g_num is %d----" % g_num)


print("-----线程创建之前g_num = %d----" % g_num)

# 启动线程1，给全局变量g_num计数
t1 = threading.Thread(target=work1, args=(10000000,))
t1.start()
t1.join()

# 启动线程2，给全局变量g_num计数
t2 = threading.Thread(target=work2, args=(10000000,))
t2.start()
t2.join()

while len(threading.enumerate()) != 1:
    time.sleep(1)

print("2个线程对同一个全局变量操作之后的最终结果是：%d" % g_num)