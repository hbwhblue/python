# --*-- conding:utf-8 --*--
# todo：线程同步
#  > 同步就是协同步调，按预定的先后次序进行运行。
#  > 如果多线程共同对某个数据修改，则可能出现不可预测的结果,为了保证数据的正确性,需要对多个线程进行同步。
#  > 使用Thread对象的`Lock`和`Rlock`可以实现简单的线程同步，这两个对象都有`acquire()`方法和`release（）`，
#  > 对于哪些需要每次只允许一个线程操作的数据，可以将其操作放到`acquire`和`release`方法之间。acquire表示线程锁定，release表示释放线程
# todo 具体实现的流程
#  对于ShareVarThreadProblem.py提出来的那个计算错误的问题，可以通过线程同步来进行解决思路如下
#  >1、系统调用t1，然后获取到g_num的值为0，此时上一把锁，即不允许其他线程操作g_num
#  >2、t1对g_num的值+1
#  >3、t1解锁，此时g_num的值为1，其他的线程就可以使用g_num了，而且是g_num的值不是0，而是1
#  >4、同理其他线程对g_num进行修改时，都要先上锁，处理完后再解锁，在上锁的整个过程中不允许其他线程访问，就保证了数据的正确性
# @Time : 2024/1/7 22:42
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading
import time

# 声明锁对象,Lock（互斥锁）是一个控制多个线程，即在线程A锁定资源，可以在线程B中去释放它
my_lock = threading.Lock()

# 声明全局变量
g_num = 0


def work1(num):
    """
    对全局变量进行自增操作
    """
    my_lock.acquire()  # 上锁

    global g_num
    for e in range(num):
        g_num += 1  # 计算
    print("----in work1，g_num is %d----" % g_num)

    my_lock.release()  # 解锁


def work2(num):
    """
    对于work1计算结果进行再次计算
    """
    my_lock.acquire()  # 上锁

    global g_num
    for e in range(num):
        g_num += 1
    print("-----in work2,g_num is %d----" % g_num)

    my_lock.release()  # 解锁


if __name__ == '__main__':
    t1 = threading.Thread(target=work1, args=(10000000,))
    t1.start()

    t2 = threading.Thread(target=work2, args=(10000000,))
    t2.start()

    while len(threading.enumerate()) != 1:
        time.sleep(1)

    print("2个线程对同一个全局变量g_num计算的结果是：%d" % g_num)
