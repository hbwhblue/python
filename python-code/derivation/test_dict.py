# --*-- conding:utf-8 --*--
# @Time : 2023/12/28 16:20
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 字典推导式和列表推导式类似，只不过中括号改成大括号而已，区别就是字典推导式返回的类型是字典
# todo 格式1：{key:value for value in iterable(可迭代对象)}
# todo 格式2：{key:value for value in iterable(可迭代对象) if 条件}
import unittest


class TestDict(unittest.TestCase):
    def test1(self):
        """
        字典推导式1
        @return:
        """
        # 还原字典推导式的实现
        # new_dict = {}
        # for x in range(1, 10):
        #     new_dict[x] = x ** 2
        # print(new_dict)
        dic = {x: x ** 2 for x in range(1, 10)}
        print(dic)

    def test2(self):
        lst = ['hello', 'python', 'java']
        # 还原字典推导式的实现
        # new_dict = {}
        # for x in lst:
        #     new_dict[x] = len(x)
        # print(new_dict)
        dic = {x: len(x) for x in lst}
        print(dic)
