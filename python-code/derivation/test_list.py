# --*-- conding:utf-8 --*--
# @Time : 2023/12/28 16:20
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description:
# todo 列表推导式：[表达式表示最终需要得到的结果 for 变量 in iterable(可迭代对象) if 条件]
# todo 列表推导式的执行顺序：从左到右依次递进,语句之间是嵌套关系
import unittest


class TestList(unittest.TestCase):
    def test1(self):
        """
        生成一个列表,类似实现：
        lstData = []
        for x in range(1,10):
            lstData.append(x)
        """
        lstData = [x for x in range(1,10)]

        print(lstData)

    def test2(self):
        """
        取范围的整数进行遍历，将遍历出来的结果+1再加入列表中
        类似实现：
        lst1 = []
        for x in range(1,10):
            lstData.append(x+1)
        """
        lst1 = [x+1 for x in range(1,10)]
        print(lst1)

        # 取范围的整数进行遍历，将遍历出来的结果*10再加入列表中
        # lst2 = []
        # for x in range(1,10):
        #     lst2.append(x*10)
        lst2 = [x*10 for x in range(1,10)]
        print(lst2)

        # 取范围的整数进行遍历，将遍历出来的的结果平方再加入列表
        # lst3 = []
        # for x in range(1,10):
        #     lst3.append(x*x)
        lst3 = [x*x for x in range(1,10)]
        print(lst3)
        
    def test_condition1(self):
        """
        求整数1-9的偶数
        取范围1-9的整数进行遍历，遍历时判断求余==0的数值加入列表
        lst1 = []
        for x in range(1,10):
            if x % 2 == 0:
                lstData.append(x+1)
        """
        lst1 = [x for x in range(1,10) if x % 2 == 0]
        print(lst1)

    def test_condition2(self):
        """
        取范围1-9的整数进行遍历，遍历时判断求余==0的数值+1，并加入列表
        :return:
        """
        lst2 = [x+1 for x in range(1,10) if x % 2 == 0]
        print(lst2)
        pass