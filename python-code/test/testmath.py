#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@Description:       : 包调用测试
@Date     :2023/04/04 15:05:59
@Author      :hjc_042043@sina.cn
@version      :1.0
"""


class Cal(object):
    # 定义两个属性,这是类属性，不是对象属性
    num1 = 0
    num2 = 0

    def __init__(self, num1, num2):
        self.num1 = num1
        self.num2 = num2
        self._color = 1

    """
    @description  : 加法运算
    ---------
    @param  : 当前对象
    -------
    @Returns  : 整型
    -------
    """

    def addNum(self):
        return self.num1 + self.num2

    """
    @description  : 减法运算
    ---------
    @param  : 当前对象
    -------
    @Returns  : 整型
    -------
    """

    def subNum(self):
        return self.num1 - self.num2

    def take(self, idx, item):
        """
        乘积
        :param idx:
        :param item:
        :return:
        """
        return 1


def main():
    cal = Cal(5, 6)
    print("加法：", cal.addNum())
    print("减法：", cal.subNum())


if __name__ == "__main__":
    main()
