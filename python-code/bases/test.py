#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : test_extend.py
@Time       : 2023/8/13 00:03
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : None
"""

import docker

client = docker.from_env()
container = client.containers.get("portainer")
inspect = container.attrs
os_info = inspect["Platform"]
print(os_info)
