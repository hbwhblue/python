#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : op_example.py
@Time       : 2023/6/8 23:11
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 运算符例子
"""


def is_leap_year():
    year = 2023
    is_leap = (year % 4 == 0) and (year % 100 != 0) and (year % 400 == 100)
    print("是否闰年：", is_leap)


is_leap_year()


def tp_conversion():
    """
    将华氏温度转换成摄氏度
    :return:
    """

    f = float(input("请输入华氏温度："))
    c = (f - 32) / 1.8
    print("%.1f华氏度=%d摄氏度" % (f, c))  # 字符串中格式化数字,.1f表示保留1位小数
    pass


# tp_conversion()


def area_infos():
    """
    计算圆的面积和周长
    :return:
    """
    PI = 3.14
    radius = float(input("请输入圆的半径："))
    perimeter = 2 * PI * radius  # 周长
    area = PI * (radius**2)
    print("周长：%.2f" % perimeter)  # .2f表示保留2位小数
    print("面积：%.2f" % area)


area_infos()
