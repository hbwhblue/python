#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : op_test.py
@Time       : 2023/6/6 12:13
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 运算符说明，算术运算符>位运算符>比较运算符>身份运算符>成员运算符>逻辑运算符>赋值运算符
"""


def ari_operator():
    """
    算术运算符
    :return:
    """
    print(12 + 10)  # 加法
    print(12 - 10)  # 减法
    print(12 * 10)  # 乘法
    print(12 / 10)  # 除法
    print(12 % 5)  # 取模
    print(12 // 5)  # 整除
    print(12**2)  # 幂运算
    pass


ari_operator()


def assign_operator():
    """
    算术运算符
    :return:
    """
    a = 10
    b = 3
    a += b
    a *= a + 2
    print("a=", a)
    pass


assign_operator()


def comp_operator():
    """
    比较运算符
    :return:
    """
    flag0 = 1 == 1
    flag1 = 3 >= 2
    flag2 = 3 < 1
    flag3 = not (1 == 2)
    print("flag0=", flag0)
    print("flag1=", flag1)
    print("flag2=", flag2)
    print("flag3=", flag3)
    pass


comp_operator()


def logic_operator():
    """
    逻辑运算符
    :return:
    """
    flag0 = 1 == 1
    flag1 = 3 > 2
    flag2 = 3 < 1
    res1 = flag0 and flag1
    res2 = flag0 or flag2
    res3 = flag1 and flag2

    print("res1=", res1)
    print("res2=", res2)
    print("res3=", res3)
    pass


logic_operator()
