#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@NAME      :test_generator.py
@TIME      :2022/05/21 12:16:24
@AUTHOR    :hjc_042043@sina.cn
@VERSION   :1.0
'''

num = 3.14
string = "hello,world"
lists = ["1", "hello", "haha"]
print(num, string, lists)
