#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : loop.py
@Time       : 2023/6/14 13:56
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 循环
"""
import random


def range1():
    """
    range(start,end),范围值是从start开始，到end-1结束，不包含end
    :return:
    """
    total = 0
    for i in range(1, 10):
        total += i
    print("total=", total)


# range1()


def range2():
    """
    通过range的步长，来循环求偶数的和
    :return:
    """
    total = 0
    for i in range(2, 101, 2):
        total += i
    print("total=", total)


# range2()


def while_example():
    """
    while的应用，以猜数字为例
    :return:
    """

    # 随机产生一个整数
    answer = random.randint(1, 100)
    # 记录猜数字的次数
    i = 0

    # while死循环要结合break使用，要不然就会系统崩溃
    while True:
        i += 1
        input_num = int(input("请输入一个整数："))
        if input_num < answer:
            print("小了一点！")
        elif input_num > answer:
            print("大了一点！")
        else:
            print("恭喜你猜中了！")
            break
    print(f"共猜了{i}次")


# while_example()


def for_example():
    """
    打印乘法口诀
    :return:
    """

    for i in range(1, 10):
        for j in range(1, i + 1):
            print(f"{i}*{j}={i * j}", "\t")
        print()


for_example()
