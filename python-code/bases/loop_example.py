#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : loop_example.py
@Time       : 2023/6/14 17:27
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 循环和分支结构的例子
"""


def find_sxhs():
    """
    寻找水仙花数
    水仙花数也被称为超完全数字不变数、自恋数、自幂数、阿姆斯特朗数，它是一个3位数，该数字每个位上数字的立方之和正好等于它本身，
    例如：153=1^3+5^3+3^3
    :return:
    """
    for num in range(100, 1000):
        low = num % 10
        mid = num // 10 % 10
        hight = num // 100

        if (hight**3 + mid**3 + low**3) == num:
            print("水仙花数：", num)


# find_sxhs()


def reversed_num():
    """
    正整数反转
    :return:
    """
    num = int(input("num="))
    reversed_num = 0
    while num > 0:
        reversed_num = reversed_num * 10 + num % 10
        num = num // 10
    print(reversed_num)


reversed_num()
