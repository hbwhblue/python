#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : branch.py
@Time       : 2023/6/11 21:49
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 分支结果
"""


def if_else():
    """
    利用if...else来验证登录用户名和密码是否正确
    :return:
    """
    user_name = input("请输入用户名：")
    password = input("请输入密码：")

    # 用户名如果是admin,密码是123456提示登录成功
    if user_name == "admin" and password == "123456":
        print("登录成功！")
    else:
        print("登录失败！")


# if_else()


def op_elif():
    x = int(input("x= "))
    if x > 1:
        y = 3 * x - 5
    elif x >= -1:
        y = x + 1
    else:
        y = 5 * x + 3
    print(f"f({x})={y}")  # 格式化字符串,用{}括起来的变量和表达式，包含的{}表达式在程序运行时会被表达式的值代替


# op_elif()


def op_elif2():
    x = int(input("x= "))
    if x > 1:
        y = 3 * x - 5
    else:
        if x >= -1:
            y = x + 1
        else:
            y = 5 * x + 3

    print(f"f({x})={y}")


# op_elif2()


def cm_parse():
    """
    英寸和厘米的转换公式
    :return:
    """
    value = float(input("请输入长度："))
    unit = input("请输入单位：")

    if unit == "in" or unit == "英寸":
        print("%.2f英寸=%.2f厘米" % (value, value * 2.54))
    elif unit == "cm" or unit == "厘米":
        print("%.2f厘米=%.2f英寸" % (value, value / 2.54))
    else:
        print("请输入有效的单位：")


# cm_parse()


def grade_parse():
    """
    百分制成绩转换成等级制成绩
    :return:
    """

    score = float(input("请输入成绩："))
    if score >= 90:
        grade = "A"
    elif score >= 80:
        grade = "B"
    elif score >= 70:
        grade = "C"
    elif score >= 60:
        grade = "D"
    else:
        grade = "E"
    print(f"成绩{score},对应等级是{grade}")

grade_parse()
