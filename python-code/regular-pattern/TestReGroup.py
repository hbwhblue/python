# --*-- conding:utf-8 --*--
# todo：尝试从字符串的起始位置匹配一个模式,如果不是起始位置匹配成功的话,match()就返回none
#   pattern：匹配的正则表达式
#   string：要匹配的字符串
#   flags：标志位，用于控制正则表达式的匹配方式，如：是否区分大小写，多行匹配
#
# @Time : 2024/1/2 23:51
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import re
import unittest


class TestReGroup(unittest.TestCase):
    def test_re(self):
        # todo 使用match方法进行匹配操作
        # todo result = re.match(正则表达式, 要匹配的字符串)
        result = re.match("(.)+@(.)+", "hjc_042043@163.com")

        # 如果上一步匹配到数据的话，可以使用group方法来提取数据
        matchData = result.group()
        print(matchData)

    def test_re2(self):
        # todo 非贪婪模式
        result = re.match("t(.+?)o", "twotwo")
        print("结果1：", result.group())

        # todo 贪婪模式
        result = re.match("t(.+)o", "twotwo")
        print("结果2：", result.group())

        result = re.match("w.o", "wto")
        print("结果3：", result.group())

    def test_re3(self):
        result = re.match("[Hh]", "Hello,Python")
        print("结果3：", result.group())

    def test_num(self):
        """测试匹配数字"""
        result = re.match("嫦娥\d号", "嫦娥2号发送成功！")
        print(result.group())

        result = re.match("嫦娥\d号", "嫦娥3号发射成功！")
        print(result.group())

    def test_nilOrMore(self):
        """测试匹配字符串0次或多次"""
        result = re.match("[A-Za-z]*", "Mma12354")
        print(result.group())

    def test_nilOrOne(self):
        result = re.match("(Pp)?", "hello,python")
        print(result.group())

    def test_email(self):
        """
        匹配163邮箱
        """
        email_list = ["hjc_042043@sina.cn", "hjc_042043@163.com", "531881823@qq.com"]
        for email in email_list:
            ret = re.match("[\w]{4,20}@163\.com$", email)
            if ret:
                print("%s,是符合规定的邮件地址,匹配后的结果是：%s" % (email, ret.group()))
            else:
                print("%s,不符合条件" % email)

    def test_email2(self):
        """
        利用分组模式`()`和或`|`的应用，匹配126,163,qq邮箱
        """
        email_list = ["hjc_042043@sina.cn", "hjc_042043@163.com", "1323234329@126.com"]
        for email in email_list:
            ret = re.match("[\w]{4,20}@(163|126|qq)\.com", email)
            if ret:
                print("%s,是符合规定的邮件地址,匹配后的结果是：%s" % (email, ret.group()))
            else:
                print("%s,不符合条件" % email)

    def test_mobile(self):
        """
        不是以4，7结尾的手机号码(11位)
        """
        tels = ["13634199417", "15323985400", "10086", "13500892344"]
        for tel in tels:
            res = re.match("1\d{9}[0-35-68-9]", tel)
            if res:
                print("符合要求的手机号码：", res.group())
            else:
                print("%s，不是想要的手机号码" % tel)

    def test_tel(self):
        """
        提供区号和电话号码
        """
        res = re.match("([^-]+)-(\d+)", "0571-88236745")
        print(res.group())
        # 打印两个分组的内容
        print("分组1：", res.group(1))
        print("分组2：", res.group(2))

    def test_group_alias(self):
        """
        1. (?P<name>) 分组起别名
        2. (?P=name)  引用别名为name分组匹配到的字符串
        """

        # 给html的匹配起别名name1，来作为匹配项
        # 给<h1>的匹配项起别名name2，来作为匹配项
        # 给</html>的匹配引用别名name1，给</h1>的匹配项引用别名name2
        pattern = r"<(?P<name1>\w*)><(?P<name2>\w*)>.*</(?P=name2)></(?P=name1)>"  # 字符串前面加上r表示不转义
        html = "<html><h1>www.itcast.cn</h1></html>"
        res = re.match(pattern, html)
        print(res.group())

        html2 = "<html><h1>www.baidu.com</h2></html>"
        res = re.match(pattern, html2)
        print(res.group())

    def test_num(self):
        """
        匹配0-100的数字
        """
        res = re.match("[1-9]?\d", "89")
        print(res.group())

        res = re.match("[1-9]?\d", "7")
        print(res.group())

        res = re.match("[1-9]?\d", "09")
        print(res.group())

        # 匹配1-99或100的数值
        res = re.match("[1-9]?\d$|100", "100")
        print(res.group())


if __name__ == '__main__':
    unittest.main()
