# --*-- conding:utf-8 --*--
# todo：正则表达式之捕获模式
#  特殊语法讲解
#  (?:pattern)
#   ()表示捕获分组，()会把每个分组里的匹配的值保存起来，从左往右，以分组的左括号为标志，第一个出现的分组的组号为1，第二个为2，以此类推。
#   而(?:)表示非捕获分组，唯一的区别在于，非捕获分组匹配的值不会保存起来。
#  (?=pattern)
#   正向肯定预查，匹配pattern前面的位置。这是一个非获取匹配，也就是说，该匹配不需要获取供以后使用
#  (?:pattern)和(?=pattern)的区别：
#   (?:pattern)匹配得到的结果包含pattern，(?=pattern)则不包含。
#  (?!pattern)
#   正向否定预查，在任何不匹配pattern的字符串开始处匹配查找字符串。这是一个非获取匹配，也就是说，该匹配不需要获取供以后使用
#
# @Time : 2024/1/5 13:29
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import re
import unittest


class TestCapture(unittest.TestCase):
    def test_group(self):
        """
        ()表示捕获分组，()会把每个分组里的匹配的值保存起来
        """
        string = "123abc456"
        res = re.search("([0-9]*)([a-z]*)([0-9]*)",string)
        print("分组1：",res.group(1))
        print("分组2：",res.group(2))
        print("分组3：",res.group(3))

    def test_no_group(self):
        """
        非匹配分组
        (?:pattern)匹配pattern但不获取匹配结果,也就是说这是一个非捕获匹配,不进行存储供以后使用
        """
        string = "123abc456"
        res = re.search("(?:[0-9]*)([a-z]*)([0-9]*)",string)
        print("分组1：",res.group(1))
        print("分组2：",res.group(2))
        print("分组3：",res.group(3))

    def test_no_group2(self):
        """
        正向预查
        (?=pattern)是指匹配pattern前面的位置，这是一个非获取匹配，也就是说该匹配不需要获取供后面使用
        """
        pattern = "Windows(?=2000|98|95)"
        res = re.search(pattern,"windows98",re.I) # 只匹配?=前面部分，后面的就不保存了
        print("获得数据：",res.group())

        '''
        Windows(?=2000|98|95)能匹配`windos2000`中的windows,但不能匹配`windows10`中的windows 
        虽然只保存前面部分的匹配内容，但后面的内容还是会查找的。查找不到就返回空。
        '''
        res = re.search(pattern,"windows10",re.I)
        print("匹配结果：",res)

    def test_diff_capture(self):
        """
        (?:pattern)和(?=pattern)的区别：
        > (?pattern)匹配得到的结果包含pattern,(?=pattern)则不包含。如：
        > (?pattern)消耗字符，下一个字符会从以匹配后的位置开始。(?=pattern)不消耗字符,下一个字符匹配会从预查之前的位置开始
        """
        '''(?:pattern)'''
        res = re.search("industr(?:y|ies)","industry abc industries")
        print("(?:pattern)匹配结果：",res.group())

        '''(?=pattern)'''
        res = re.search("industr(?=y|ies)","industries abc")
        print("(?=pattern)匹配结果：",res.group())

    def test_diff_capture2(self):
        string = "I'm Windows 2000Abc end."
        '''(?:pattern)'''
        res = re.search("Windows (?:98|2000|xp)ABC",string,re.I)
        print("(?:pattern)匹配结果：",res.group())

        '''(?=pattern)'''
        res = re.search("Windows (?:98|2000|xp)ABC", string, re.I)
        print("(?=pattern)匹配结果：", res.group())

if __name__ == '__main__':
    unittest.main()
