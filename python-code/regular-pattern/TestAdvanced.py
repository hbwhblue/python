# --*-- conding:utf-8 --*--
# todo：高级用法
#  re.search扫描整个字符串并返回第一个成功匹配;匹配成功re.search方法返回一个匹配的对象,否则返回None
#   函数语法：re.search(pattern,string,flags=0)
#   参数说明：
#   > pattern: 匹配的正则表达式
#   > string: 要匹配的字符串
#   > flags：标志位，用于控制正则表达式的匹配方式，如：是否区分大小写，多行匹配等等
#
# todo：注意
#   > re.match()和re.search()只匹配依次，findall()匹配所有
#
# @Time : 2024/1/5 00:54
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import re
import unittest


class TestAdvanced(unittest.TestCase):
    def test_search1(self):
        """
        高级用法之search
        """
        line = "Cats are smarter than Dogs"
        searchObj = re.search(r"(.*) are (.*?) .*", line, re.M | re.I)
        if searchObj:
            print("searchObj.group()：", searchObj.group())
            print("searchObj.group(1)：", searchObj.group(1))
            print("searchObj.group(2)：", searchObj.group(2))
        else:
            print("Nothing found!")

    def test_search_diff_match(self):
        """
        re.match与re.search的区别：
        re.match只匹配字符串的开始，如果字符串开始不符合正则表达式，则匹配失败，函数返回None
        re.search匹配整个字符串，直到找到一个匹配
        """
        line = "Cats are smartter than Dogs"
        # 正则match的应用
        matchObj = re.match(r'dogs', line, re.M | re.I)
        if matchObj:
            print("match--->matchObj.group()：", matchObj.group())
        else:
            print("matchObj.group()--->No match!")

        # 正则search的应用
        searchObj = re.search(r'dogs', line, re.M | re.I)
        if searchObj:
            print("search--->searchObj.group()：", searchObj.group())
        else:
            print("searchObj.group()---->No match!")

    def test_findall(self):
        """
        todo 在字符串中找到正则表达式所匹配的所有子串,并返回一个列表,如果有多个匹配模式，则返回元祖列表,如果没匹配,则返回空列表
        """
        res = re.findall(r"\d+", "python = 9992,c = 2983,c++ = 3987")
        print(res)

        # 多个匹配模式
        res = re.findall(r"(\w+)=(\d+)", 'set width=20 and height=15')
        print(res)

    def test_sub(self):
        """
        将匹配到的数据进行替换
        语法: re.sub(pattern,replace,string,count=0,flags=0)
        pattern：正则表达式
        replace: 替换的字符串，也可以是函数
        string：原字符串
        count：模式匹配后,替换的最大次数,默认=0表示替换所有匹配
        flags：匹配模式,数字形式
        """
        # 将电话的-去掉
        phone = "0571-823450987"
        last_phone = re.sub(r"\D", "", phone)  # 将非数字替换为空字符串
        print(last_phone)

        # 将日期字符串，带`#`后面的字符串替换掉成空的
        date_time = "2023-01-05 23:59:59 #日期"
        res = re.sub(r"#.*$", "", date_time)
        print(res)

        # 用函数进行替换
        s = "A23G43BF567"
        res = re.sub('(?P<value>\d+)', replace_func, s)
        print(res)

    def test_split(self):
        """
        根据匹配进行匹配字符串,并返回一个列表
        语法：re.split(pattern,string,maxsplit=0,flags=0)
        """

        # 以空格或`:`切割字符串
        res = re.split(":| ", "hjc:浙江 杭州")
        print(res)


def replace_func(matched):
    """
    用来做为替换函数用
    """
    value = int(matched.group('value'))
    return str(value * 2)


if __name__ == '__main__':
    unittest.main()
