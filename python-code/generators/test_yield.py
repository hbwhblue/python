def my_range(start: int, end: int) -> int:
    """
    yield生成器
    :param start:
    :param end:
    :return:
    """
    current = start
    while start < end:
        yield current
        current += 1
        start += 1


def main():
    """
    主函数调用生成器
    :return:
    """
    # range_gen = my_range(1, 5)
    # while True:
    #     try:
    #         print(next(range_gen))
    #     except StopIteration as e:
    #         print('迭代器结束的返回值：', e.value)
    #         break

    for i in my_range(1, 10):
        print(i)
    pass


if __name__ == "__main__":
    main()
