# --*-- conding:utf-8 --*--
# @Time : 2023/12/28 22:36
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 测试迭代对象
# todo: 迭代是通过重复执行的代码处理相似的数据集的过程
# todo：__iter__()方法返回对象本身，即：self
# todo: __next__()方法返回下一个数据，如果没有数据了，就抛出一个StopIteration异常

import unittest
from collections.abc import Iterator
from typing import Iterable

"""
@todo 测试迭代器
"""


class TestIter(unittest.TestCase):
    def test1(self):
        """
        todo 判断是不是序列
        """
        print(isinstance([1, 2, 3, 4, 5], Iterable))
        print(isinstance(['a', 'b', 'c', 'd'], Iterable))
        print(isinstance('java+python', Iterable))
        print(isinstance(123456, Iterable))

    def test_custom_iter(self):
        myNumbers = MyIter([1, 2, 3, 4, 5])
        myIter = iter(myNumbers)  # 将可迭代对象转换成迭代器
        for e in myIter:
            print(e)


class MyIter(Iterator):
    """
    @todo 自定义一个迭代器
    """

    def __init__(self, num_list):
        self.num_list = num_list
        self.index = 0

    def __iter__(self):
        """
        todo 返回自身
        @return:
        """
        return self

    def __next__(self):
        """
        todo 返回下一个数据,如果没有数据了，就抛出一个StopIteration异常
        @return:
        """
        try:
            num = self.num_list[self.index]
        except IndexError:
            raise StopIteration

        self.index += 1
        return num
