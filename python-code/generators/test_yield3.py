# --*-- conding:utf-8 --*--
# todo：协程测试
#
# @Time : 2024/4/7 21:48
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
def func1():
    yield 1
    yield from func2()
    yield 2


def func2():
    yield 3
    yield 4


if __name__ == '__main__':
    f1 = func1()
    for e in f1:
        print(e)
