# --*-- conding:utf-8 --*--
# todo：生成器测试
#
# @Time : 2024/4/10 16:14
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

def gen_func():
    yield 1
    yield 2
    yield 3


if __name__ == "__main__":
    # 生成器对象，python编译字节码的时候就产生了
    gen = gen_func()
    # 其实生成器对象也是实现了迭代器协议
    for val in gen:
        print(val)
    pass
