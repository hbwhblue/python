# --*-- conding:utf-8 --*--
# todo：使用yield处理大文件
#
# @Time : 2024/4/10 18:24
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import time


def read_big_csv(file_path):
    """
    :param file_path: csv文件路径
    :return:
    """
    data = []
    with open(file_path, 'r') as f:
        while True:
            line_info = f.readline()
            if not line_info:
                break
            yield line_info.strip().split(',')


def write_big_csv(file_path):
    ope_big_csv = read_big_csv('000002.SZ.csv')
    # 读取第一行，其实要把表头先摘除
    titles = next(ope_big_csv)
    print(titles)

    while True:
        try:
            with open('000001.SZ.csv', 'a') as f:
                f.write(','.join(next(ope_big_csv)) + '\n')
        except StopIteration as e:
            print(e)
            break

    print("追加文件成功")
    pass


if __name__ == '__main__':
    ope_big_csv = read_big_csv('000001.SZ.csv')

    # 使用yield处理大文件
    start_time = time.time()
    for row in ope_big_csv:
        print(row)
    print("耗时：", time.time() - start_time)
