# --*-- conding:utf-8 --*--
# @Time : 2023/12/20 16:09
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# @Description: 生成器测试
import unittest


class GeneratorTest(unittest.TestCase):
    def test_create_list1(self):
        """
        使用推导式，生成1-10的平方数
        :return:
        """
        lst = [x * x for x in range(10)]
        print(lst)
        pass

    def test_create_list2(self):
        """
        todo  使用生成器，生成1-10的平方数
        :return:
        """
        lst = (x * x for x in range(0, 10))
        print("使用()生成器来生成列表：", lst)
        for item in lst:
            print(item)
        pass

    def test_yield1(self):
        """
        todo 这就是定义generator的另一种方法。如果一个函数定义中包含yield关键字，那么这个函数就不再是一个普通函数，
            而是一个generator函数，调用一个generator函数将返回一个generator，yield需要和next()配合使用。
        :return:
        """
        res = fb(6)  # 调用生成斐波那契数列，创建生成器
        print(res)

        # todo 我们把函数改成generator函数后，我们基本上从来不会用next()来获取下一个返回值，而是直接使用for循环来迭代
        for item in res:
            print(item)
        pass

    def test_yield2(self):
        num = odd()
        print(type(num))

        # todo odd不是普通函数，而是genrerator函数，使用next函数不断的获取下一个值，
        #  遇到yield就中断,再次执行时就照着上次来执行，直到没有yield了就退出
        n = next(num)
        print(n)  # 取到yield后的值
        n = next(num)
        print(n)  # 取到yield后的值
        n = next(num)
        print(n)  # 取到yield后的值
        pass

    def test_genrerator_return(self):
        g = fb(10)
        while True:
            try:
                x = next(g)
                print("yield data:", x)
            except StopIteration as e:  # 等到停止迭代为止再返回
                print('Gererator return value：', e.value)
                break
                pass
        pass

    pass


def fb(max):
    """
    :todo 创建斐波那契数列
        这里，最难理解的就是generator函数和普通函数的执行流程不一样。普通函数是顺序执行，遇到return语句或者最后一行函数语句就返回。
        而变成generator的函数，在每次调用next()的时候执行，遇到yield语句返回，再次执行时从上次返回的yield语句处继续执行。
    :param max:
    :return:
    """
    n, a, b = 0, 0, 1
    while n < max:
        yield b
        a, b = b, a + b
        n = n + 1
    return 'done'


def odd():
    """
    @todo 定义一个generator函数，依次返回数字1，3, 5
    :return:
    """
    print("step 1")
    yield 1
    print("step 2")
    yield 3
    print("step 3")
    yield 5
