#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File       : TestDict.py
@Time       : 2023/6/26 10:14
@Author     : hjc_042043@163.com
@Version    : 1.0
@Software   : PyCharm
@Desc       : 集合的使用
"""


def create_set():
    """
    创建集合
    :return:
    """
    set1 = {1, 2, 3, 3, 4, 5}
    print("set1=", set1)
    print("set1_len=", len(set1))

    # 使用构造器语法创建集合
    set2 = set("hello")
    print("set2=", set2)

    # 将列表转换成集合,并去掉重复元素
    set3 = set([1, 2, 3, 3, 2, 1, 5])
    print("set3=", set3)

    # 使用生成器
    set4 = {num for num in range(1, 20) if num % 3 == 0 or num % 5 == 0}
    print("set4=", set4)

    # 遍历集合4
    for item in set4:
        print("item=", item)


# create_set()


def op_set():
    """
    集合运算
    :return:
    """

    # 成员运算
    set1 = {1, 2, 3, 4, 5}
    print("6 is in=", 6 in set1)
    print("5 is in=", 5 in set1)
    set2 = {"Python", "Java", "Go", "Php"}
    print("Python is in=", "Python" in set2)
    print("C++ is in=", "C++" in set2)

    # 交并差运算,和数学上的交并差一样
    set1 = {1, 2, 3, 4, 5, 6, 7, 8}
    set2 = {2, 4, 6, 8, 10}

    # 求交集
    res = set1 & set2
    print("两集合交集：", res)
    print("两集合交集第二种方法：", set1.intersection(set2))
    # 求并集
    res = set1 | set2
    print("两集合并集：", res)
    print("两集合并集第二种方法：", set1.union(set2))
    # 求差集
    res = set1 - set2
    print("两集合差集：", res)
    print("两集合差集第二种方法：", set1.difference(set2))
    # 对称差,相当于是两集合的并集-两集合的交集,使用异或符号
    res = set1 ^ set2
    print("两集合的对称差：", res)
    print("两集合的对称差第二种方法：", set1.symmetric_difference(set2))
    print("两集合的对称差第三种方法：", (set1 | set2) - (set1 & set2))


# op_set()


def op_update_set():
    """
    集合运算的复合赋值
    :return:
    """
    set1 = {1, 3, 5, 7}
    set2 = {2, 4, 6}

    # 将set1和set2并集并重新赋值给set1
    set1.update(set2)
    print("合并后的集合重新赋值给set1=", set1)
    set1 |= set2
    print("合并后的集合重新赋值之方法二set1=", set1)

    set3 = {3, 6, 9}
    # 将set1和set3取交集后再赋值给set1
    set1.intersection_update(set3)
    print("交集后的集合重新赋值给set1=", set1)
    set1 &= set3
    print("交集的集合重新赋值之方法二给set1=", set1)


# op_update_set()


def method_set():
    """
    集合的方法
    :return:
    """

    # 创建一个空集合
    set1 = set()

    # 通过add方法添加元素
    set1.add(1)
    set1.add(2)
    # 添加多个元素
    set1.update(set1, {3, 4, 5, 6, 7})
    print("set1-add=", set1)

    # 通过discard方法删除指定元素
    set1.discard(7)
    set1.discard(8)
    print("set1-update=", set1)

    # 通过remove方法删除元素,建议先做成员运算在删除
    # 如果key不存在，就会引发KeyError异常
    if 10 in set1:
        set1.remove(10)
    print("set1-remove=", set1)

    # pop方法可以从集合中随机删除一个元素，并返回一个元素
    pop_item = set1.pop()
    print("set1-pop=", pop_item)

    # clear方法可以清空整个集合
    set1.clear()
    print("set1-clear=", set1)

    # 判断两个集合有没有相同元素(交集),使用isdisjoint方法，没有相同元素返回true,否则返回false
    set1 = {"Java", "C++", "Python", "Golang"}
    set2 = {"Swift", "C", "Golang"}
    set3 = {"Html", "Javascript", "Css"}
    print("set1和set2是否有相同的元素", set1.isdisjoint(set2))
    print("set1和set3是否有相同的元素", set1.isdisjoint(set3))

    a = {1, 2, 3, 4}
    a.update([1, 2, 3])
    print("a=", a)

# method_set()
