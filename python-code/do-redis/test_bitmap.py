# --*-- conding:utf-8 --*--
# todo：
#
# @Time : 2024/4/4 10:40
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import random
import unittest

from db.redis_pool import RedisPool


class BitmapTestCase(unittest.TestCase):
    def setUp(self):
        self.uid_list = ['1001', '1002', '1003', '1004', '1005', '1006', '1007', '1008', '1009', '1010']
        pass

    def test_do_sign(self):
        """
        测试人员签到
        将uid作为偏移量offset
        @return:
        """
        cache_key = "user_signin_bitmap"
        # 创建一个空位图
        RedisPool().setbit(cache_key, 0, 0)

        for uid in self.uid_list:
            val = random.randint(0, 1)
            RedisPool().setbit(cache_key, uid, val)
        pass

    def test_read_sign(self):
        """
        测试读取人员签到状态
        @return:
        """
        cache_key = "user_signin_bitmap"
        for uid in self.uid_list:
            val = RedisPool().getbit(cache_key, uid)
            print(f"{uid}:{val}")
        pass


if __name__ == '__main__':
    unittest.main()
