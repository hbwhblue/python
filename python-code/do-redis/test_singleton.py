# --*-- conding:utf-8 --*--
# todo：测试redis单例模式
#
# @Time : 2024/3/10 00:18
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
import threading
import time

from db.redis_pool import RedisPool


def task(arg):
    redis_obj = RedisPool()
    time.sleep(2)
    print(f"执行编号：{arg}, 连接对象的内存地址: {id(redis_obj)}\n")


if __name__ == '__main__':
    thread_list = []
    for i in range(10):
        t = threading.Thread(target=task, args=(i,))
        thread_list.append(t)
        t.start()

    for redis_t in thread_list:
        redis_t.join()
