# --*-- conding:utf-8 --*--
# todo：
#
# @Time : 2024/1/25 02:58
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
from celery_task import send_email, send_msg

if __name__ == "__main__":
    for i in range(10):
        result = send_email.delay(f"张三{i}")
        print(result.id)
        result2 = send_msg.delay(f"李四{i}")
        print(result2.id)
