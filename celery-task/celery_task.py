# --*-- conding:utf-8 --*--
# todo：创建任务，来给生产者异步调用
#
# @Time : 2024/1/25 02:46
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

import time

from celery_object import celery_app


@celery_app.task
def send_email(name):
    print("向%s发送邮件..." % name)
    time.sleep(5)
    print("向%s发送邮件完成" % name)
    return f"成功拿到{name}发送的邮件!"


@celery_app.task
def send_msg(name):
    print("向%s发送短信..." % name)
    time.sleep(5)
    print("向%s发送短信完成" % name)
    return f"成功拿到{name}发送的短信!"
