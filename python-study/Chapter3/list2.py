#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
'''
@Description:       :
@Date     :2022/05/02 23:14:18
@Author      :hjc_042043@sina.cn
@version      :1.0
'''

from cmath import sqrt


class OpList2:

    #声明一个列表
    __listInfo = []
    #声明一个数字列表
    __listNum = []

    #构造函数
    def __init__(self):
        self.__listInfo = [
            'hjc', 'liuchong', 'mxm', 'liulj', 'wangyajie', 'zhuhai'
        ]
        self.__listNum = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]

    """
    @description  :循环打印列表内容,连同键/值一起输出
    ---------
    @param  :对象本身
    -------
    @Returns  :
    -------
    """

    def cycleList(self):
        #将索引一起输出
        for index, item in enumerate(self.__listInfo):
            print(f"索引是{index}, 值是{item.title()}")

    """
    @description  :打印range产生的序列
    ---------
    @param  :start 开始的数值，end 结束的数字，range()不把end的值算上去
    -------
    @Returns  :
    -------
    """

    def rangeList(self, start, end):
        # range函数是用来生成序列，和元组类似，长度不可变。list函数就是将rang产生的序列转换成列表
        numbers = list(range(start, end))

        for value in numbers:
            print('循环的range值', value)

    """
    @description  :求平方
    ---------
    @param  :start 开始的数值，end 结束的数字，range()不把end的值算上去，表示到end截止
    -------
    @Returns  :
    -------
    """

    def squareList(self, start, end):
        square = []
        for item in range(start, end):
            val = item**2
            square.append(val)
        return square

    """
    @description  :将一个值进行开方
    ---------
    @param  :start 数列的开始，end 数列的结束
    -------
    @Returns  :
    -------
    """

    def sqrtList(self, start, end, step=2):
        sqrts = []
        for item in range(start, end, step):
            val = sqrt(item)
            sqrts.append(val)
        return sqrts

    """
    @description  :统计字典的值
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def statisList(self):
        return {
            'min': min(self.__listNum),
            'max': max(self.__listNum),
            'sum': sum(self.__listNum)
        }

    """
    @description  :推导式列表
    ---------
    @param  :对象本身
    -------
    @Returns  :newList 返回新的列表
    -------
    """

    def drivedList(self):
        newList = [item**2 for item in self.__listNum
                   if item % 2 == 0]  #将数字列表中的2的倍数的元素遍历出来，并计算出平方
        return newList

    """
    @description  :获得列表的属性内容
    ---------
    @param  :
    -------
    @Returns  :list类型
    -------
    """

    def getList(self):
        return self.__listInfo


OpList = OpList2()

OpList.cycleList()  #打印对象的属性列表

OpList.rangeList(1, 10)  #打印有数字生成的列表,这不会包含10，表示到10之前截止

squares = OpList.squareList(1, 11)  #打印1,10的平方
print('打印1到10的平方数：', squares)

# sqrts = OpList.sqrtList(2, 32)
# print('打印2到32的开方数：', sqrts)

statis = OpList.statisList()
print(f"最小值：{statis['min']},", f"最大值：{statis['max']},", f"求和：{statis['sum']}")

newList = OpList.drivedList()
print("利用推导式计算列表元素的平方：", newList)
