#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
'''
@Description:       :列表考试测试
@Date     :2022/05/03 02:36:55
@Author      :hjc_042043@sina.cn
@version      :1.0
'''

import sys
import time


class ExamList(object):
    def __init__(self):
        self.listInfo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        self.start = 1
        self.end = 20
        pass

    """
    @description  :打印1到20的数字，包含20的
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def printNum1(self):
        self.end = 21

        for index, item in enumerate(range(self.start, self.end)):
            print(f'index:{index}', item)

    """
    @description  :利用生成器打印1到100万的数据
    ---------
    @param  :
    -------
    @Returns  :一个迭代器对象
    -------
    """

    def yiedNums(self):
        self.start = 1
        self.end = 1000001

        for item in range(self.start, self.end):
            yield item
        pass

    """
    @description  :执行生成器，打印100W数据
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def printYiedNums(self):
        val = self.yiedNums()
        i = 0
        while (lastRes := next(val)):
            try:
                print(f'第{i}行的值为：', lastRes, "\n")
            except StopIteration:
                sys.exit()

    """
    @description  :序列求和
    ---------
    @param  :
    -------
    @Returns  :int求和
    -------
    """

    def sumNums(self):
        self.end = 1000001

        newList = list(range(self.start, self.end))
        sums = sum(newList)
        return sums

    """
    @description  :打印奇数
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def printOddNums(self):
        self.end = 21

        for item in range(self.start, self.end, 2):
            print(item)

    """
    @description  :利用推导式打印能够被3整除的数
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def printModNums(self):
        newList = [
            item for item in range(self.start, self.end) if item % 3 == 0
        ]

        for index, item in enumerate(newList):
            print(f"打印能够被3整除，key：{index}，value：{item}")

    """
    @description  :求立方数
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def printCubeNums(self):
        self.end = 10
        newList = [item ** 3 for item in range(self.start, self.end)]

        for index, item in enumerate(newList):
            print(f"打印立方，key:{index},value:{item}")


print("---------------------调用类--------------")

Examls = ExamList()

# 打印1-20的数值，包含20
# Examls.printNum1()

# # 利用生成器打印出
Examls.printYiedNums()
#
# # 对1到100W数据进行求和
# startTime = time.time()
# sums = Examls.sumNums()
# endTime = time.time()
# totalTime = endTime - startTime
# print("执行时间：{%.8s}s" % totalTime)  # 按微秒计算
# print(f'序列数据求和：{sums}')
#
# # 打印奇数
# Examls.printOddNums()
#
# # 打印能够被3整除的数
# Examls.printModNums()
#
# # 打印立方数
# Examls.printCubeNums()
