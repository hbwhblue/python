#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
'''
@Description:       :列表的排序
@Date     :2022/05/02 21:41:24
@Author      :hjc_042043@sina.cn
@version      :1.0
'''


class SortList:
    __listInfo = []

    def __init__(self):
        self.__listInfo = ['b', 'a', '1', '2', 'c', 'd', 'e', 'f', 'g']

    """
    @description  :对列表永久排序，顺序排序就是按字符的ascii排序
    ---------
    @param  :对象本身
    -------
    @Returns  :
    -------
    """

    def sortList(self):
        self.__listInfo.sort()
        return self

    """
    @description  :对列表永久排序，倒序排序
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def sortRList(self):
        self.__listInfo.sort(reverse=True)
        return self

    """
    @description  :
    获得临时排序后的值,逆序排序，临时排序其实就是将现有的列表赋值赋值一份出来排序后赋值给另一个变量，原来的值不会发生变化。
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def sortedRList(self):
        sortedLs = sorted(self.__listInfo, reverse=True)
        return sortedLs

    """
    @description  :反转列表，不做排序，只是将列表反转
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def reverseList(self):
        self.__listInfo.reverse()
        return self

    """
    @description  :统计列表的长度
    ---------
    @param  :
    -------
    @Returns  :int
    -------
    """

    def lenList(self):
        return len(self.__listInfo)

    """
    @description  :统计列表的元素出现的个数
    ---------
    @param  :
    -------
    @Returns  :int
    -------
    """

    def cntList(self, val):
        cnt = self.__listInfo.count(val)
        return cnt

    """
    @description  :获得列表的最终结果
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def getList(self):
        return self.__listInfo


print('====================测试===================')

Sortls = SortList()
lsinfo = Sortls.sortList().getList()
print('list的sort：', lsinfo)

lsinfo = Sortls.sortRList().getList()
print('list的倒序排列：', lsinfo)

lsinfo = Sortls.sortedRList()
print('list临时排序后的值：', lsinfo)

lsinfo = Sortls.reverseList().getList()
print('list反转后的值：', lsinfo)

lsleng = Sortls.lenList()
print('list的长度：', lsleng)

lscnt = Sortls.cntList('a')
print('list的a元素出现的次数：', lscnt)
