#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
'''
@Description:       :列表操作
@Date     :2022/05/02 19:09:51
@Author      :hjc_042043@sina.cn
@version      :1.0
'''


class opList:
    __listInfo = []  #设置私有属性listInfo，来存储列表内容
    __listPop = []  #被移除的元素存放的地方

    #构造函数
    def __init__(self):
        self.__listInfo = ['hjc', 'lc', 'llj']

    """
    @description  :列表末尾追加元素
    ---------
    @param  :item,元素值
    -------
    @Returns  :对象本身
    -------
    """

    def appendInfo(self, item):
        self.__listInfo.append(item)
        return self

    """
    @description  :插入元素
    ---------
    @param  :item 需要插入的元素值
    -------
    @Returns  :返回对象本身
    -------
    """

    def insertInfo(self, index, item):
        self.__listInfo.insert(index, item)
        return self

    """
    @description  :修改列表的元素
    ---------
    @param  :index 元素的位置
    @param  :item  元素的值
    -------
    @Returns  :返回对象本身
    -------
    """

    def modifyInfo(self, index, item):
        self.__listInfo[index] = item
        return self

    """
    @description  :从列表移除某个元素，是在不知道元素具体的位置的使用,在列表比较大的时候，存在性能问题
    ---------
    @param  :需要移除的元素值
    -------
    @Returns  :返回对象本身
    -------
    """

    def removeInfo(self, item):
        self.__listInfo.remove(item)
        return self

    """
    @description  :删除某个元素，不再使用了，就用del
    ---------
    @param  :需要删除元素的位置
    -------
    @Returns  :对象本身
    -------
    """

    def delInfo(self, index):
        del self.__listInfo[index]
        return self

    """
    @description  :删除某个索引的元素，默认是删除最后一个元素，并返回删除的元素，相当于是数据结构的出栈
    ---------
    @param  :index，索引位置
    -------
    @Returns  :返回对象本身
    -------
    """

    def popInfo(self, index=None):
        if (index is None):
            rmVar = self.__listInfo.pop()
        else:
            rmVar = self.__listInfo.pop(index)

        # self.__listPop.append(rmVar)
        self.__listPop.append(rmVar)
        return self

    """
    @description  :获得列表的内容
    ---------
    @param  :
    -------
    @Returns  :列表属性
    -------
    """

    def getListInfo(self):
        return self.__listInfo

    """
    @description  :获得被删除
    ---------
    @param  :
    -------
    @Returns  :
    -------
    """

    def getPopInfo(self):
        return self.__listPop


Opls = opList()
lsinfo = Opls.appendInfo('ggc').getListInfo()  #在列表的末尾添加元素
print('list的append：', lsinfo)

lsinfo = Opls.insertInfo(0, 'mxm').getListInfo()  #在列表开头插入元素
print('list的insert：', lsinfo)

lsinfo = Opls.modifyInfo(-1, 'wangyajie').getListInfo()  #倒数第二个元素修改内容
print('list的modify：', lsinfo)

lsinfo = Opls.removeInfo('mxm').getListInfo()  #根据某个元素移除值
print('list的remove：', lsinfo)

lsinfo = Opls.delInfo(0).getListInfo()  #删除第一个元素,永久的删除
print('list的del：', lsinfo)

lsinfo = Opls.popInfo().getPopInfo()  #删除最后一个元素,并返回被删除厚的列表
print('list的pop：', lsinfo)

listinfo = Opls.getListInfo()
print('list最后的结果：', listinfo)