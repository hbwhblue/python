def ptStr():
    str = "Runoob"

    print("所有字符：", str[::])  # 输出所有字符串
    print("输出所有：", str[0:])
    print("第1个字符：", str[0])  # 输出字符串的第一个字符
    print("输出到第2个字符：", str[:2])  # 输出字符串从0开始到第2个字符
    print("输出到第3个字符到结尾：", str[2:])  # 输出从第三个开始后的所有字符
    print("第1个到倒数第2个的所有字符：", str[0:-1])  # 输出第一个到倒数第二个的所有字符
    print("输出两次字符串：", 2 * str)  # 输出字符串两次
    print("连接字符串：", str + "Test")  # 连接字符串


# 字符串截取测试
ptStr()


# 打印字符，设置字符
def assgin():
    word = "python"
    print(word[0], word[5])  # 打印第一个，第六个字符

    # word[0] = "P"  # 字符串的容量是不可变的，在赋值时会报错
    print(word[-1], word[-6])  # 打印倒数第一个，倒数第六个字符


# 不转义字符串
def escape():
    str = r"hello\n world!"
    print(str)


# 主函数
def main():
    # 字符串转义
    escape()
    # 字符赋值测试
    assgin()


if __name__ == "__main__":
    main()
