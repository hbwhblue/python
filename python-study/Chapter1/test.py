import sys


def printInfo(msg):
    print(sys.version_info)
    print(msg)


printInfo("hello world!")

"""
slice举例,slice[start:end];
start,下标从0开始算起，如果start=1,就表示第2个字符。
end,表示第end个字符，其下标是end-1
"""


def ptSubstr():
    msg = "hello,world!"
    print(msg[0:-1])
    print(msg[2:5])  # 打印从第3个字符到第5个字符
    print("\n")  # 打印空行
    print(msg[2:])  # 打印从第3个字符开始后的所有字符
    print(r"-----\n")  # 字符串前面加r表示字符串不转义
    print(f"倒数第三个字符：{msg[-3:]}")  # 打印倒数3个字符


def main():
    ptSubstr()


if __name__ == "__main__":
    main()
