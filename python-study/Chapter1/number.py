from unicodedata import name


# 一次性打印多个变量
def ptMore():
    intNum = 100
    floatNum = 101.1
    nameStr = "hjc"
    print(intNum, floatNum, nameStr)

    a, b, c = 1, "hello", 10.23
    print(a, b, c)


"""
Number类型
"""


def ptNumberType():
    a, b, c, d = type(20), type(5.5), type(True), type(4 + 3j)  # 在赋值时，Number对象就会被创建
    print(a, b, c, d)  # 通过打印，这些类型都是对象类型
    print(isinstance(20, int))  # 打印对象a是否是int类型


# 打印数据类型
ptNumberType()


def ptBool():
    res = issubclass(bool, int)  # bool类型是否是int的子类
    print(res)

    valEQ1 = True == 1
    print("valeq1：", valEQ1)  # True和1是否值相等

    valEQ2 = False == 0
    print("valeq2：", valEQ2)  # False和0是否值相等

    typeEQ1 = 1 is True  # 从 python 3.8 开始，使用 is 和 is not 运算符时，会抛出 SyntaxWarning 语句警告信息
    print("typeEQ1：", typeEQ1)  # 1和True的类型是否相等，is是来判断类型
    typeEQ2 = 0 is False
    print("typeEQ2：", typeEQ2)  # 0和False的类型是否相等

    print(True + 1)  # bool类型和int类型值相加


# 打印布尔类型的特性
ptBool()


def delObj():
    var1, var2 = 1, 10  # 赋值，创建对象
    print(var1, var2)

    del var1, var2  # 删除对象
    # print(var1,var2) #报错，因为对象var1,var2已经被删除了


# 删除对象
delObj()


def oprations():
    print("求和：", 1 + 2)
    print("减法：", 10.8 - 5)
    print("乘法：", 5 * 10)
    print("除法-浮点型：", 12 / 7)  # 除法，得到一个浮点数
    print("除法-整型：", 12 // 8)  # 除法，得到整型
    print("求余：", 12 % 5)  # 求余
    print("乘方：", 2**3)  # 2的3次方


# 数值运算
oprations()
