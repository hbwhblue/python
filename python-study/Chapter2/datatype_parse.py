# 测试将整型隐式转换成浮点型
from xml.dom.minidom import TypeInfo


def testParse():
    numInt = 10
    numFloat = 10.5
    """
    因为int类型的优先级比float要底，所以计算时会自动转换成浮点数，不然精度容易溢出
    数据类型的优先级：byte,char-->int--->long--->float--->double
    不能将对象转换成不相关类的对象类型
    把容量大的类型转换成容量小的类型时，必须使用强制类型转换
    浮点数到整数，是通过舍弃小叔得到，而不是四舍五入
    """
    numNew = numInt + numFloat

    print("整数和浮点数相加：", numNew)


# 隐式转换
def strToInt():
    numInt = 10
    numStr = "122"
    numBool = True

    print("data type of numInt：", type(numInt))
    print("data type of numStr：", type(numStr))
    print("data type of numBool：", type(numBool))

    print("int + boolen：", numInt + numBool)

    numNew = numInt + int(numStr)  # 这样子会报错，因为字符串类型不是数字类型，需要进行显示转换。
    print("numNew is ", numNew)


# strToInt()


# 显示转换
def testParse2(num1, num2):
    return num1 + num2


# 主函数
def main():
    # 整数+浮点数
    testParse()
    # 隐式转换
    strToInt()

    # int类型相加
    intSum = testParse2(1, int("123"))
    print("int类型求和：", intSum)

    # float类型相加
    floatSum = testParse2(float(2), 3.14)
    print("float类型求和", floatSum)

    # str类型相加,是字符串拼接
    strNew = testParse2(str(2), str(3.14))
    print("str类型相加", strNew)


if __name__ == "__main__":
    main()
