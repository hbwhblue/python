# 测试字典的特性
def testDic1():
    """
    字典声明的第一种方式
    """
    dic = {}
    dic[0] = "test"
    dic["uid"] = "1234089"
    dic["user_name"] = "hjc_042043"
    dic["email"] = "hjc_042043@sian.cn"
    dic["phone"] = "13634199417"

    print("输出key=uid的值：", dic["uid"])
    # print('输出key=2的值：',dic[2])     #注意字典类型是不能隐式转换成list类型的，这里因为key中没有2的元素，所以会报错

    print("输出完整的字典dic：", dic)

    print("输出所有的键名：", dic.keys())
    print("输出所有的值：", dic.values())
    print("输出所有键值对：", dic.items())


# 测试字典2
def testDic2():
    dic = dict([("real_name", "黄锦潮"), ("age", 37), ("sex", 1)])
    print("字典声明的第2种方式：", dic)

    dic2 = dict(real_name="黄锦潮", sex=1, age=37)
    print("字典声明的第3种方式：", dic2)

    dic3 = {"order_sn": "009780832378", "address": "杭州市西湖区文一西路"}
    print("字典声明的第4种方式：", dic3)

    # 推导式
    dic4 = {x: x ** 2 for x in (2, 3, 4)}
    print("字典推导式的使用：", dic4)


def main():
    testDic1()
    testDic2()


if __name__ == "__main__":
    main()
