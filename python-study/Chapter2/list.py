# 打印list列表,测试分片，列表的合并
def testList():
    list = [1, "hjc", "email", 3.14, True]
    list2 = [2, "username"]

    print(list)  # 打印list列表
    print("list的第1个元素:", list[0])  # 打印第1个元素
    print("list下标为1开始,到第3个元素结束:", list[1:3])  # 打印下标从1开始，到第3个元素结束,不包括第三个元素
    print("list下标为2开始到结束的所有元素:", list[2:])  # 打印下标从2开始的所有元素
    print("list2输出两次:", list2 * 2)  # 打印两次list2
    print("将list+list2两个列表合并:", list + list2)


# 测试列表元素的改变，
def testList2():
    list = ["a", "b", "c", 1, 2, 3]
    list[0] = 9
    print("第1个元素被改变：", list)

    list[:3] = [4, 5, 6]
    print("从第1个元素到第三个元素被改变：", list)

    list[2:5] = [3, 6, 9]
    print("从第3个元素到第5个元素被改变：", list)

    list[-2:] = []
    print("从倒数第2个元素开始，设置为空：", list)


# 测试步长取值
def testStep():
    list = [1, 2, 3, 4, 5, 6]
    newList = list[0::2]
    print("从0开始到末尾,取值步长为2，即隔2个取一次：", newList)

    newList2 = list[-1::-1]
    print("从下标最后一个元素开始到末尾,取值步长为-1，即隔反向取一次：", newList2)


# 步长为负数，来做字符串反转
def reverseWords(str):
    inputWords = str.split(",")  # 分割字符串，放到列表中
    inputWords = inputWords[-1::-1]

    output = ",".join(inputWords)  # 重新拼接到字符串
    print("列表的反转：", output)


# 将字符串的字符塞入到列表中
def strPushList(str):
    list = []
    for item in str:
        list.append(item)

    return list


def main():
    # 测试列表
    testList()

    # 测试列表2
    testList2()

    # 测试步长
    testStep()

    # 字符串反转
    reverseWords("a,b,c,d,e,f")

    # 将字符串的字符塞入到列表中
    lastList = strPushList("hjc_042043@sian.cn")
    print(lastList)


if __name__ == "__main__":
    main()
