# 测试元组的赋值，取值
def testTuple():
    tuple = (1, 2, 3, 4, 5, 6)
    tuple2 = ("abc", "hjc")

    print("打印tuple的所有元素：", tuple)
    print("打印tuple的第一个元素：", tuple[0])
    print("打印tuple的第二个元素到第三个元素：", tuple[1:3])
    print("打印从第三个元素开始的所有元素：", tuple[2:])
    print("将tuple2的元素打印两次：", tuple2 * 2)
    print("将tuple2和tuple进行合并：", tuple + tuple2)

    # 元祖不能修改
    # tuple2[0] = "123"
    print("不能修改元组：", tuple2)


testTuple()


# 测试元组的声明
def declraTuple():
    tup1 = ()
    tup2 = 1  # 如果是一个元素，元素后面需要加,
    print(tup1, tup2)


declraTuple()
