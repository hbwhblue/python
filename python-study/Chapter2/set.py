# 集合声明，成员测试
def testSet():
    set1 = set()
    print("空集合初始化：", set1)

    set2 = {"taobao", "jingdong", "pingduoduo", "facebook", "baidu", "taobao"}
    print("输出集合，集合自动排序，重复的元素自动去掉：", set2)

    # 成员测试
    if "taobao" in set2:
        print("taobao在集合set2中")
    else:
        print("taobao不在集合set2中")


"""
集合运算
set1: 集合1
set2: 集合2
op: 操作符
"""


# 集合的运算
def testSet2(set1, set2, op):
    lastSet = set()

    if op == "+":
        # lastSet=set1+set2   #集合合并,这是错误的，正确用法使用'|'
        lastSet = {"集合合并,这是错误的，正确用法使用'|'"}
    elif op == "-":
        lastSet = set1 - set2  # 集合的差集
    elif op == "|":
        lastSet = set1 | set2  # 集合的并集
    elif op == "&":
        lastSet = set1 & set1  # 集合的交集
    elif op == "^":
        lastSet = set1 ^ set2  # 集合中不同时存在的元素
    else:
        lastSet = {"unknown"}  # 操作符不正确

    return lastSet


def main():
    testSet()

    set1 = {1, 2, 3, 4, 5, 6}
    set2 = {1, 3, 5, 7, 9, 11}

    print("集合合并：", testSet2(set1, set2, "+"))
    print("集合差集：", testSet2(set1, set2, "-"))
    print("集合合并：", testSet2(set1, set2, "|"))
    print("集合交集：", testSet2(set1, set2, "&"))
    print("集合的异或操作：", testSet2(set1, set2, "^"))


if __name__ == "__main__":
    main()
