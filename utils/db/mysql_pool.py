# --*-- conding:utf-8 --*--
# todo：SQLAchemy
#
# @Time : 2024/2/6 14:43
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

"""
利用SQLAchemy实现Mysql数据库的连接池
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# 1. 创建engine
engine = create_engine('mysql+pymysql://root:123456@127.0.0.1:3306/chatgpt?charset=utf8', pool_size=10, max_overflow=20)
# 2. 创建session
Session = sessionmaker(bind=engine)
session = Session()

# 3. 创建数据
# user = User(name='allen', password='123456')
# session.add(user)
# session.commit()
# # 4. 查询数据
# user = session.query(User).filter(User.name == 'allen').first()
# print(user)
