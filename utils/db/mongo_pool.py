# --*-- conding:utf-8 --*--
# todo：mongodb的连接池
#
# @Time : 2024/1/26 14:11
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

"""
mongodb的连接池
"""
import threading

from pymongo import MongoClient

from conf.load_config import Config


class MongoPool(object):
    """
    mongodb连接池创建
    """
    _pool = None
    _instance = None
    _lock = threading.Lock()  # 线程锁，是属于同步锁

    def __new__(cls):
        if not cls._instance:  # 在并发进来的时候，只会创建一次，如果已经创建了，就不用排队再去等待上锁了再判断实例是否为空，这样子可以提高性能。
            with cls._lock:  # 类似JAVA的synchronized,自动获取和释放锁
                if not cls._instance:
                    cls._instance = cls.get_pool()
        return cls._instance

    @classmethod
    def get_pool(cls):
        """
        连接池初始化
        """
        user = Config['mongodb']['user']
        host = Config['mongodb']['host']
        port = Config['mongodb']['port']
        password = Config['mongodb']['password']
        maxPoolSize = Config['mongodb']['maxPoolSize']
        connectTimeoutMS = Config['mongodb']['connectTimeoutMS']

        # 连接mongodb的URI
        uri = f"mongodb://{user}:{password}@{host}:{port}/?maxPoolSize={maxPoolSize}"
        client = MongoClient(uri, connectTimeoutMS=connectTimeoutMS)
        return client
