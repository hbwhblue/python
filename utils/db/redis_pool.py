# --*-- conding:utf-8 --*--
# todo：使用Redis的连接池创建redis连接
#
# @Time : 2024/1/25 19:17
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm

"""
redis连接池的单例实现
"""
import threading

import redis

from conf.load_config import Config


class RedisPool(object):
    # 单例对象
    _instance = None
    # 连接池对象
    _pool = None
    # 线程锁
    _lock = threading.Lock()

    def __new__(cls):
        """
        单例实现
        """
        if not cls._instance:  # 为了防止多线程下，不停的去加锁判断是否创建对象，在最外面层加一层判断
            with cls._lock:  # 类似JAVA的synchronized,自动获取和释放锁
                if not cls._instance:
                    cls._pool = cls.get_pool()
                    cls._instance = redis.StrictRedis(connection_pool=cls._pool)  # 获取edis连接对象

        return cls._instance

    @classmethod
    def get_pool(cls):
        """
        连接池初始化
        """
        redis_conf = dict(
            host=Config['redis']['host'],
            port=Config['redis']['port'],
            db=Config['redis']['db'],
            password=Config['redis']['password'],
            socket_timeout=Config['redis']['socket_timeout'],
        )
        return redis.ConnectionPool(decode_responses=True, **redis_conf)
