# --*-- conding:utf-8 --*--
# todo：加载配置文件
#
# @Time : 2024/1/26 01:19
# @Author : allen.huang
# @Email : hjc_042042@sina.cn
# @Software : PyCharm
# 加载配置文件
import os
from os.path import abspath, dirname

import yaml

# 获取各个环境对应的配置文件
CONFIG_FILES = {
    'dev': 'config-dev.yaml',
    'test': 'config-test.yaml',
    'prod': 'config-prod.yaml'
}

# 获取配置
Config = {}


def load_config():
    global Config

    # 获取当前运行环境
    env = os.getenv('CHATGPT_PYTHON_ENV', 'dev')
    if env not in CONFIG_FILES:
        raise ValueError('没有该环境的配置文件')

    with open(abspath(dirname(__file__)) + '/' + CONFIG_FILES[env], 'r', encoding='utf-8') as f:
        try:
            Config = yaml.safe_load(f)
        except yaml.YAMLError as e:
            raise e


# 读取当前项目的运行环境,如果没有则默认是dev环境
try:
    load_config()
except Exception as e:
    print(e)
